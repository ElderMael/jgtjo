package mx.gob.jgtjo.apps.schedule.service;

import java.util.Date;
import java.util.List;

import mx.gob.jgtjo.apps.schedule.model.AudienciaOral;
import mx.gob.jgtjo.apps.schedule.model.CausaPenal;

/**
 * 
 * 
 * @author ElderMael
 * 
 */
public interface AudienciaOralService {

	/**
	 * Persiste en base de datos una audiencia oral.
	 * 
	 * @param audienciaOral
	 *            - Audiencia Oral a persistir en base de datos
	 */
	public void save(AudienciaOral audienciaOral);

	/**
	 * Actualiza la audiencia oral proporcionada
	 * 
	 * @param audienciaOral
	 *            - La audiencia oral previamente persistida que se va a
	 *            actualizar.
	 */
	public void update(AudienciaOral audienciaOral);

	/**
	 * Obtiene todas las audiencias orales agendadas entre las fechas
	 * especificadas.
	 * 
	 * @param startDate
	 *            - Fecha desde la cual comenzara la busqueda.
	 * @param endDate
	 *            - Fecha hasta la cual se buscara.
	 * @return Todas las audiencias orales agendadas entre las fechas
	 *         especificadas. Nunca {@code null}
	 */
	public List<AudienciaOral> loadAudienciasInTimeFrame(Date startDate, Date endDate);

	/**
	 * Obtiene todas las audiencias orales agendadas entre las fechas
	 * especificadas para la sala cuyo id se proporciona.
	 * 
	 * @param startDate
	 *            - Fecha desde la cual comenzara la busqueda.
	 * @param endDate
	 *            - Fecha hasta la cual se buscara.
	 * @param salaId
	 *            - id de la sala para en la cual se desea obtener las
	 *            audiencias agendadas
	 * @return Todas las audiencias orales agendadas entre las fechas
	 *         especificadas para la sala especificada.
	 */
	public List<AudienciaOral> loadAudienciasInTimeFrame(Date startDate, Date endDate,
			Integer salaId);

	/**
	 * Obtiene el numero de Audiencias realizadas dentro de una causa penal
	 * 
	 * @param causaPenal
	 *            - La causa penal
	 * @return El numero de audiencias realizadas dentro de la causa penal.
	 */
	public Integer getNumberOfAudienciasInCausa(CausaPenal causaPenal);

	/**
	 * Busca todas las audiencias cuya causa penal tiene como numero y año los
	 * proporcionados.
	 * 
	 * @param causaNumber
	 *            - Numero de la causa
	 * @param causaYear
	 *            - Año de la causa
	 * @return - Todas las Audiencias Penales de la causa penal identificada por
	 *         el numero y año especificado.
	 */

	public List<AudienciaOral> findByCausaNumberAndYear(Integer causaNumber, Integer causaYear);

}
