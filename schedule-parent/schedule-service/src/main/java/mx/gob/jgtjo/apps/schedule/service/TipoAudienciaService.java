package mx.gob.jgtjo.apps.schedule.service;

import java.util.List;

import mx.gob.jgtjo.apps.schedule.model.TipoAudiencia;

/**
 * 
 * @author ElderMael
 * 
 */
public interface TipoAudienciaService {

	/**
	 * Obtiene todos los tipos de audiencia existentes.
	 * 
	 * @return Todos los tipos de audiencia en base de datos.
	 */
	public List<TipoAudiencia> loadAll();

	/**
	 * Persiste un tipo de audiencia en base de datos.
	 * 
	 * @param tipoAudiencia
	 *            El tipo de audiencia a persistir.
	 */
	public void save(TipoAudiencia tipoAudiencia);

	/**
	 * Obtiene un tipo de audiencia por el id proporcionado.
	 * 
	 * @param id
	 *            - El id del tipo de audiencia.
	 * @return El tipo de audiencia asociado al id proporcionado.
	 */
	public TipoAudiencia loadById(Integer id);

}
