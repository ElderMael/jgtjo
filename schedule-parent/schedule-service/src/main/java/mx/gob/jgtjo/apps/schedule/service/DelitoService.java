package mx.gob.jgtjo.apps.schedule.service;

import java.util.List;

import mx.gob.jgtjo.apps.schedule.model.Delito;

/**
 * 
 * @author necromancer
 * 
 */
public interface DelitoService {

	/**
	 * Persite un delito en base de datos.
	 * 
	 * @param delito
	 *            - El delito a persistir.
	 */
	public void save(Delito delito);

	/**
	 * Carga todos los delitos existentes en la base de datos.
	 * 
	 * @return El catalogo de delitos en base de datos.
	 */
	public List<Delito> loadAll();

	/**
	 * Busca un delito en base de datos por medio de su id.
	 * 
	 * @param id
	 *            - El id del delito a traer de la base de datos.
	 * @return El delito cuyo id se proporciono, si no se encuentra regresa
	 *         {@code null}
	 */
	public Delito loadById(Integer id);

	/**
	 * Persiste o actualiza un delito en base de datos.
	 * 
	 * @param delito
	 *            - Un delito persistido o nuevo.
	 */
	public void saveOrUpdate(Delito delito);

	/**
	 * Persiste varios delitos en base de datos.
	 * 
	 * @param delitos
	 *            - Los delitos a persistir.
	 */
	public void saveAll(List<Delito> delitos);

	/**
	 * Borra todos los delitos existentes en base de datos.
	 * 
	 * Usar con cuidado.
	 */
	public void deleteAll();

	/**
	 * Borra un delito de la base de datos.
	 * 
	 * @param delito
	 */
	public void delete(Delito delito);

}
