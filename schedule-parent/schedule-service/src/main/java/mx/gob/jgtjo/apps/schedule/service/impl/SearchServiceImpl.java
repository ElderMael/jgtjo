package mx.gob.jgtjo.apps.schedule.service.impl;

import java.io.Serializable;
import java.util.List;
import java.util.regex.Pattern;

import org.apache.lucene.search.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.search.FullTextSession;
import org.hibernate.search.Search;
import org.hibernate.search.query.dsl.QueryBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import mx.gob.jgtjo.apps.schedule.model.AudienciaOral;
import mx.gob.jgtjo.apps.schedule.model.CausaPenal;
import mx.gob.jgtjo.apps.schedule.model.MinisterioPublico;
import mx.gob.jgtjo.apps.schedule.service.AudienciaOralService;
import mx.gob.jgtjo.apps.schedule.service.SearchService;

/**
 * 
 * Hibernate Search Implementation of {@link SearchService}.
 * 
 * @author necromancer
 * 
 */
@Service("searchService")
public class SearchServiceImpl implements SearchService, Serializable {

	private static final long serialVersionUID = 1L;

	protected static final Logger log = LoggerFactory.getLogger(SearchServiceImpl.class);

	@Autowired
	private SessionFactory sessionFactory;

	@Autowired
	private AudienciaOralService audienciaOralService;

	public SearchServiceImpl() {

	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	@Transactional(readOnly = true)
	public List<CausaPenal> searchCausasPenales(String keywords) {

		log.debug("Buscando Causa Penal por palabras clave '{}'", keywords);

		final String[] fields = { "numeroCausa", "annioCausa", "victimas.nombres",
				"imputados.nombres", "victimas.apellidoPaterno", "victimas.apellidoMaterno",
				"imputados.apellidoPaterno", "imputados.apellidoMaterno" };

		Session session = this.sessionFactory.getCurrentSession();

		FullTextSession fullTextSession = Search.getFullTextSession(session);

		QueryBuilder queryBuilder = fullTextSession.getSearchFactory().buildQueryBuilder()
				.forEntity(CausaPenal.class).get();

		Query luceneQuery = queryBuilder.keyword().onFields(fields).matching(keywords)
				.createQuery();

		org.hibernate.Query hibernateQuery = fullTextSession.createFullTextQuery(luceneQuery,
				CausaPenal.class);

		List results = hibernateQuery.list();

		log.debug("Encontradas {} causa(s) penal(es)", results.size());

		return results;
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	@Transactional(readOnly = true)
	public List<AudienciaOral> searchAudienciasOrales(String keywords) {

		final String[] fields = { "causaPenal.imputados.nombres",
				"causaPenal.imputados.apellidoPaterno", "causaPenal.imputados.apellidoMaterno",
				"causaPenal.victimas.nombres", "causaPenal.victimas.apellidoPaterno",
				"causaPenal.victimas.apellidoMaterno" };

		keywords = keywords.trim();

		log.debug("Buscando Audiencias Orales por palabras clave '{}'", keywords);

		final Pattern causaAndYearRegex = Pattern.compile("\\d{4}/\\d{4}"); // e.g.
																			// 0001/2012
		if (causaAndYearRegex.matcher(keywords).matches()) {
			String[] causaNumberAndYear = keywords.split("/");

			this.sessionFactory.getCurrentSession().clear();

			return this.audienciaOralService.findByCausaNumberAndYear(
					Integer.parseInt(causaNumberAndYear[0]),
					Integer.parseInt(causaNumberAndYear[1]));

		} else {
			// Buscar con Hibernate Search

			Session session = this.sessionFactory.getCurrentSession();

			FullTextSession fullTextSession = Search.getFullTextSession(session);

			QueryBuilder queryBuilder = fullTextSession.getSearchFactory().buildQueryBuilder()
					.forEntity(AudienciaOral.class).get();

			Query luceneQuery = queryBuilder.keyword().onFields(fields).matching(keywords)
					.createQuery();

			org.hibernate.Query hibernateQuery = fullTextSession.createFullTextQuery(luceneQuery,
					AudienciaOral.class);

			List results = hibernateQuery.list();

			log.debug("Encontradas {} Audiencia(s) Oral(es)", results.size());

			return results;
		}

	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	@Transactional(readOnly = true)
	public List<MinisterioPublico> searchMinisteriosPublicos(String keywords) {

		final String[] fields = { "apellidoPaterno", "apellidoMaterno", "nombres" };

		Session session = this.sessionFactory.getCurrentSession();

		FullTextSession fullTextSession = Search.getFullTextSession(session);

		QueryBuilder queryBuilder = fullTextSession.getSearchFactory().buildQueryBuilder()
				.forEntity(MinisterioPublico.class).get();

		Query luceneQuery = queryBuilder.keyword().onFields(fields).matching(keywords)
				.createQuery();

		org.hibernate.Query hibernateQuery = fullTextSession.createFullTextQuery(luceneQuery,
				MinisterioPublico.class);

		List results = hibernateQuery.list();

		log.debug("Encontrados {} Ministerio(s) Publico(s)", results.size());

		return results;
	}

	public SessionFactory getSessionFactory() {
		return sessionFactory;
	}

	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}

	public AudienciaOralService getAudienciaOralService() {
		return audienciaOralService;
	}

	public void setAudienciaOralService(AudienciaOralService audienciaOralService) {
		this.audienciaOralService = audienciaOralService;
	}

}
