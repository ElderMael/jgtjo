package mx.gob.jgtjo.apps.schedule.service;

import java.util.Date;
import java.util.List;

import mx.gob.jgtjo.apps.schedule.model.CausaPenal;
import mx.gob.jgtjo.apps.schedule.model.Juez;
import mx.gob.jgtjo.apps.schedule.model.TipoAudiencia;

/**
 * 
 * @author ElderMael
 * 
 */
public interface JuezService {

	/**
	 * Obtiene todos los jueces en base de datos.
	 * 
	 * @return Los jueces guardados en base de datos.
	 */
	public List<Juez> loadAll();

	/**
	 * Persite un juez en base de datos.
	 * 
	 * @param juez
	 *            - El juez que se persistira.
	 */
	public void save(Juez juez);

	/**
	 * Busca un juez en base de datos cuyos nombres coincidan exactamente con la
	 * String proporcionada.
	 * 
	 * @param names
	 *            - Los nombres del juez a buscar.
	 * @return El juez que coincide con el nombre o nombres de pila
	 *         proporcionados.
	 */
	public Juez findByNames(String names);

	/**
	 * Obtiene un juez mediante el siguiente algoritmo.
	 * 
	 * <ul>
	 * <li>No hay juez existente que pueda atender dos audiencias al mismo
	 * tiempo (fact of physics)</li>
	 * 
	 * <li>De vez en cuando hay jueces que no pueden ser agendados por no estar
	 * activados</li>
	 * 
	 * <li>Si conoce formulación, debe conocer vinculación</li>
	 * 
	 * <li>Si conoce vinculación e imposición de medidas, debe conocer revisión
	 * de medidas cautelares</li>
	 * 
	 * <li>si conoce conciliación, debe conocer revisión de condiciones de
	 * conciliación</li>
	 * 
	 * <li>Si conoce suspensión del proceso a prueba, debe conocer revisión de
	 * condiciones de suspensión</li>
	 * 
	 * <li>Si conoce orden de aprehensión, no debe conocer ningún otra audiencia
	 * </li>
	 * 
	 * <li>Si conoce vinculación, no debe conocer intermedia ni juicio oral</li>
	 * 
	 * <li>Si conoce vinculación, no debe conocer salidas alternas (siempre y
	 * cuando sean audiencias diferentes)</li>
	 * 
	 * <li>Si conoce intermedia, no debe conocer juicio oral</li>
	 * 
	 * <li>Si conoce control de detención, no debe conocer ninguna otra
	 * audiencia, salvo que hayan sido impuestas medidas cautelares y deban
	 * revisarse</li>
	 * </ul>
	 * 
	 * 
	 * @param causaPenal
	 *            - Causa penal para la audiencia a efectuar.
	 * @param tipoAudiencia
	 *            - Tipo de audiencia que se efectuara.
	 * @param horaAudiencia
	 *            - Hora en que se agendo la audiencia.
	 * @param razones
	 *            - Una lista que sera modificada dentro de el metodo agregando
	 *            las razones por las cuales se recomienda el juez regresado por
	 *            este metodo.
	 * @return Un juez recomendado por el algoritmo antes mencionado.
	 */
	public Juez suggestJuezForAudiencia(CausaPenal causaPenal, TipoAudiencia tipoAudiencia,
			Date horaAudiencia, List<String> razones);

}
