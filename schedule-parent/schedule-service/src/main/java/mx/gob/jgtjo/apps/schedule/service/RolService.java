package mx.gob.jgtjo.apps.schedule.service;

import java.util.List;

import org.hibernate.HibernateException;

import mx.gob.jgtjo.apps.schedule.model.Rol;

/**
 * 
 * @author ElderMael
 * 
 */
public interface RolService {

	/**
	 * Persiste un rol en base de datos.
	 * 
	 * @param rol
	 *            - El rol que se persistira
	 */
	public void save(Rol rol);

	/**
	 * Persite un rol en base de datos y dependiendo del parametro flushAndClear
	 * se efectuara una transaccion y se hara commit de la misma.
	 * 
	 * @param rol
	 *            - El rol a persistir.
	 * @param flushAndClear
	 *            - Si se debe realizar el commit al efectuar la transaccion.
	 */
	public void save(Rol rol, Boolean flushAndClear);

	/**
	 * Carga un rol con el id suministrado.
	 * 
	 * @param id
	 *            - El id del rol.
	 * @return El rol cuyo id se proporciono como argumento.
	 */
	public Rol load(Integer id);

	/**
	 * Obtiene todos los roles desde base de datos.
	 * 
	 * @return Los roles en base de datos. Todos ellos.
	 */
	public List<Rol> loadAll();

	/**
	 * Busca un rol especifico por su campo authority.
	 * 
	 * Supuestamente solo debe haber un rol con dicho authority.
	 * 
	 * 
	 * @param authorityString
	 *            - El authority del rol a obtener.
	 * 
	 * @return El unico rol para el authority proporcionado.
	 * 
	 * @throws HibernateException
	 *             Si hay mas de un Rol con dicho authority.
	 */
	public Rol findByAuthorityString(String authorityString);

}
