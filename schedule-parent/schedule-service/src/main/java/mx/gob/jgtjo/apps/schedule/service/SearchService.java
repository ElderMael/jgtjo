package mx.gob.jgtjo.apps.schedule.service;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;

import org.hibernate.Session;
import org.hibernate.SessionFactory;

import mx.gob.jgtjo.apps.schedule.model.AudienciaOral;
import mx.gob.jgtjo.apps.schedule.model.CausaPenal;
import mx.gob.jgtjo.apps.schedule.model.MinisterioPublico;

/**
 * Busca diferentes entidades en base de datos por medio de palabras clave.
 * Estas busquedas dependen de la implementacion del motor de busqueda. <br />
 * <br />
 * La mayoria de los metodos estan sobrecargados con un manejador de
 * peresistencia (e.g. {@link Session}, {@link EntityManager} con el cual se
 * haran las busquedas. De otra manera se usara el manejador proporcionado por
 * el contexto actual (e.g. {@link SessionFactory#getCurrentSession()}) <br />
 * <br />
 * Si la implementacion no usa un manejador de persistencia es razonable que se
 * lance un {@link UnsupportedOperationException} con los metodos antes
 * mencionados.
 * 
 * @author necromancer
 * 
 */
public interface SearchService {

	List<CausaPenal> searchCausasPenales(String keywords);

	List<AudienciaOral> searchAudienciasOrales(String keywords);

	List<MinisterioPublico> searchMinisteriosPublicos(String keywords);

}
