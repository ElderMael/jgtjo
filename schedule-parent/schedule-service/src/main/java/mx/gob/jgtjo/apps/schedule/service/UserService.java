package mx.gob.jgtjo.apps.schedule.service;

import java.util.Collection;
import java.util.List;

import mx.gob.jgtjo.apps.schedule.model.User;

/**
 * 
 * @author ElderMael
 * 
 */
public interface UserService {

	/**
	 * Persiste un usuario en base de datos.
	 * 
	 * @param user
	 *            - El usuario a persistir
	 */
	public void save(User user);

	/**
	 * Persiste un usuario en base de datos y termina la transaccion actual al
	 * terminar dependiendo del parametro flush;
	 * 
	 * @param user
	 *            - El usuario a persistir.
	 * @param flush
	 *            - Si se debe terminar la transaccion.
	 */
	public void saveAndFlushSession(User user, Boolean flush);

	/**
	 * Persite varios usuarios en la misma transaccion.
	 * 
	 * @param users
	 *            - Los usuarios a persistir.
	 */
	public void saveMany(Collection<User> users);

	/**
	 * Obtiene un usuario desde base de datos por medio de su id.
	 * 
	 * @param id
	 *            - El id del usuario.
	 * 
	 * @return El usuario cuyo id se proporciono como argumento.
	 */
	public User load(Integer id);

	/**
	 * Actualiza los cambios hechos en el usuario en base de datos.
	 * 
	 * @param user
	 *            - El usuario existente en base de datos con cambios en sus
	 *            campos.
	 */
	public void update(User user);

	/**
	 * Obtiene todos los usuarios de la base de datos.
	 * 
	 * @return Todos los usuarios en base de datos.
	 */
	public List<User> loadAll();

}
