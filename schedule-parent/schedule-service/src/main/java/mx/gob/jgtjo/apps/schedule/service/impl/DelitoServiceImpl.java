package mx.gob.jgtjo.apps.schedule.service.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import mx.gob.jgtjo.apps.schedule.dao.DelitoDao;
import mx.gob.jgtjo.apps.schedule.model.Delito;
import mx.gob.jgtjo.apps.schedule.service.DelitoService;

@Service("delitoService")
public class DelitoServiceImpl implements DelitoService {

	protected static final Logger log = LoggerFactory.getLogger(DelitoServiceImpl.class);

	public static final String DELETE_DELITO_BY_ID_HQL = "DELETE FROM Delito WHERE id = :id";

	public static final String ID_PARAMETER_NAME = "id";

	public static final String DELETE_ALL_DELITOS_HQL = "DELETE FROM Delito";

	public static final String GET_ALL_DELITOS_HQL = "FROM Delito";

	@Autowired
	private DelitoDao delitoDao;

	public DelitoServiceImpl() {

	}

	@Override
	@Transactional(readOnly = true)
	public Delito loadById(Integer id) {
		return this.delitoDao.load(id);
	}

	@Override
	@Transactional
	public void save(Delito delito) {
		this.delitoDao.save(delito);
	}

	@Override
	@Transactional(readOnly = true)
	public List<Delito> loadAll() {
		return this.delitoDao.createQuery(GET_ALL_DELITOS_HQL);
	}

	@Override
	@Transactional
	public void saveOrUpdate(Delito delito) {
		this.delitoDao.saveOrUpdate(delito);
	}

	@Override
	@Transactional
	public void saveAll(List<Delito> delitos) {

		for (Delito delito : delitos) {
			this.delitoDao.saveOrUpdate(delito);
		}

	}

	@Override
	@Transactional
	public void deleteAll() {
		this.delitoDao.createQuery(DELETE_ALL_DELITOS_HQL);
	}

	@Override
	public void delete(Delito delito) {
		Map<String, Object> parameterNameAndValues = new HashMap<String, Object>();
		parameterNameAndValues.put(ID_PARAMETER_NAME, delito.getId());

		int results = this.delitoDao.createDeleteQuery(DELETE_DELITO_BY_ID_HQL,
				parameterNameAndValues);

		log.debug("Deleted {} delitos", results);

	}

	public DelitoDao getDelitoDao() {
		return this.delitoDao;
	}

	public void setDelitoDao(DelitoDao delitoDao) {
		this.delitoDao = delitoDao;
	}

}
