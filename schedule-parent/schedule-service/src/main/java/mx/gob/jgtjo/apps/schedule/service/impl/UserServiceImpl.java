package mx.gob.jgtjo.apps.schedule.service.impl;

import java.util.Collection;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import mx.gob.jgtjo.apps.schedule.dao.UserDao;
import mx.gob.jgtjo.apps.schedule.dao.hibernate.HibernateBaseDao;
import mx.gob.jgtjo.apps.schedule.model.User;
import mx.gob.jgtjo.apps.schedule.service.UserService;

@Service("userService")
public class UserServiceImpl implements UserService {

	private static final String GET_ALL_USERS_HQL = "FROM User";
	@Autowired
	private UserDao userDao;

	@Override
	@Transactional
	public void save(User user) {
		this.userDao.save(user);
	}

	@Override
	@Transactional
	public void saveAndFlushSession(User user, Boolean flush) {
		if (flush) {
			this.userDao.save(user);
			this.castDaoToBase().getSessionFactory().getCurrentSession().flush();
			this.castDaoToBase().getSessionFactory().getCurrentSession().clear();
		}

	}

	@Override
	@Transactional
	public void saveMany(Collection<User> users) {
		for (User user : users) {
			this.save(user);
		}
	}

	public UserDao getUserDao() {
		return this.userDao;
	}

	@Override
	@Transactional
	public User load(Integer id) {
		return this.userDao.load(id);
	}

	public void setUserDao(UserDao userDao) {
		this.userDao = userDao;
	}

	@Override
	@Transactional
	public void update(User user) {
		this.userDao.update(user);
	}

	@Override
	@Transactional(readOnly = true)
	public List<User> loadAll() {
		return this.userDao.createQuery(GET_ALL_USERS_HQL);
	}

	@SuppressWarnings("unchecked")
	private HibernateBaseDao<User, Integer> castDaoToBase() {
		return (HibernateBaseDao<User, Integer>) this.userDao;
	}

}
