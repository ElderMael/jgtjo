package mx.gob.jgtjo.apps.schedule.service;

import java.util.List;

import mx.gob.jgtjo.apps.schedule.model.Sala;

public interface SalaService {

	/**
	 * Obtiene todas las salas existentes.
	 * 
	 * @return Todas las salas en base de datos.
	 */
	List<Sala> loadAll();

	/**
	 * Persiste una sala en base de datos.
	 * 
	 * @param sala
	 *            - La sala a persistir.
	 */
	void save(Sala sala);

	/**
	 * Obtiene una sala por su Id.
	 * 
	 * @param id
	 *            - El id de la sala.
	 * @return La sala cuyo id se proporciono como argumento.
	 */
	Sala loadById(Integer id);
}
