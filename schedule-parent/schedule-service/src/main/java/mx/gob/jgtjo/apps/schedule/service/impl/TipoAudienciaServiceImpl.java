package mx.gob.jgtjo.apps.schedule.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import mx.gob.jgtjo.apps.schedule.dao.TipoAudienciaDao;
import mx.gob.jgtjo.apps.schedule.model.TipoAudiencia;
import mx.gob.jgtjo.apps.schedule.service.TipoAudienciaService;

@Service("tipoAudienciaService")
public class TipoAudienciaServiceImpl implements TipoAudienciaService {

	public static String LOAD_ALL_TIPO_AUDIENCIA_HQL = "FROM TipoAudiencia";

	@Autowired
	private TipoAudienciaDao tipoAudienciaDao;

	public TipoAudienciaServiceImpl() {

	}

	@Override
	@Transactional(readOnly = true)
	public List<TipoAudiencia> loadAll() {
		return this.tipoAudienciaDao.createQuery(LOAD_ALL_TIPO_AUDIENCIA_HQL);
	}

	@Override
	@Transactional
	public void save(TipoAudiencia tipoAudiencia) {
		this.tipoAudienciaDao.save(tipoAudiencia);
	}

	@Override
	@Transactional(readOnly = true)
	public TipoAudiencia loadById(Integer id) {
		return this.tipoAudienciaDao.load(id);
	}

	public TipoAudienciaDao getTipoAudienciaDao() {
		return tipoAudienciaDao;
	}

	public void setTipoAudienciaDao(TipoAudienciaDao tipoAudienciaDao) {
		this.tipoAudienciaDao = tipoAudienciaDao;
	}

}
