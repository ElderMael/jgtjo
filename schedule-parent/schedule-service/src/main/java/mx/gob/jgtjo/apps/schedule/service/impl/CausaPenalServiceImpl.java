package mx.gob.jgtjo.apps.schedule.service.impl;

import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import mx.gob.jgtjo.apps.schedule.dao.CausaPenalDao;
import mx.gob.jgtjo.apps.schedule.dao.ParteMaterialDao;
import mx.gob.jgtjo.apps.schedule.model.CausaPenal;
import mx.gob.jgtjo.apps.schedule.service.CausaPenalService;

@Service("causaPenalService")
public class CausaPenalServiceImpl implements CausaPenalService {

	public static final String ANNIO_PARAMETER_NAME = "annio";
	public static final String GET_MAX_NUMERO_CAUSA_PER_IN_YEAR_HQL = "SELECT new MAP( count(numeroCausa) as counter ) FROM CausaPenal WHERE annioCausa = :annio";
	public static final String MAX_NUMERO_CAUSA_RESULT_ALIAS = "counter";
	public static final String GET_ALL_CAUSAS_HQL = "FROM CausaPenal";

	public static final String GET_CAUSA_BY_NUMBER_AND_YEAR = "FROM CausaPenal WHERE annioCausa = :annioCausa AND numeroCausa = :numeroCausa";
	public static final String ANNIO_CAUSA_PARAMETER_NAME = "annioCausa";
	public static final String NUMERO_CAUSA_PARAMETER_NAME = "numeroCausa";

	@Autowired
	private CausaPenalDao causaPenalDao;

	@Autowired
	private ParteMaterialDao parteMaterialDao;

	private CausaPenalServiceImpl() {

	}

	@Override
	@Transactional
	public void save(CausaPenal nuevaCausa) {

		// Guardar Causa
		this.causaPenalDao.save(nuevaCausa);

	}

	@Override
	public Integer generateNewCausaPenalNumber() {

		return this.generateNewCausaPenalNumber(Calendar.getInstance().get(Calendar.YEAR));
	}

	@Override
	@Transactional
	public Integer generateNewCausaPenalNumber(Integer annio) {

		Map<String, Long> result = this.causaPenalDao.createQueryAndReturnFirstMap(
				GET_MAX_NUMERO_CAUSA_PER_IN_YEAR_HQL, ANNIO_PARAMETER_NAME, annio, Long.class);

		return result.get(MAX_NUMERO_CAUSA_RESULT_ALIAS).intValue() + 1;
	}

	@Override
	public List<CausaPenal> loadAllCausas() {
		return this.causaPenalDao.createQuery(GET_ALL_CAUSAS_HQL);
	}

	@Override
	public CausaPenal findByNumberAndYear(Integer causaNumber, Integer causaYear) {
		Map<String, Object> parametersNameAndValues = new HashMap<String, Object>();

		parametersNameAndValues.put(ANNIO_CAUSA_PARAMETER_NAME, causaYear);
		parametersNameAndValues.put(NUMERO_CAUSA_PARAMETER_NAME, causaNumber);

		return this.causaPenalDao
				.createQuery(GET_CAUSA_BY_NUMBER_AND_YEAR, parametersNameAndValues).get(0);
	}

	@Override
	public Integer getNumberOfAudiencias(Integer id) {

		Map<String, Integer> counts = this.causaPenalDao.createQueryAndReturnFirstMap(
				"SELECT COUNT(id) AS count FROM CausaPenal WHERE id = :idCausa", Integer.class);

		return counts.get("count");
	}

	public CausaPenalDao getCausaPenalDao() {
		return this.causaPenalDao;
	}

	public ParteMaterialDao getParteMaterialDao() {
		return this.parteMaterialDao;
	}

	public void setCausaPenalDao(CausaPenalDao causaPenalDao) {
		this.causaPenalDao = causaPenalDao;
	}

	public void setParteMaterialDao(ParteMaterialDao parteMaterialDao) {
		this.parteMaterialDao = parteMaterialDao;
	}

}
