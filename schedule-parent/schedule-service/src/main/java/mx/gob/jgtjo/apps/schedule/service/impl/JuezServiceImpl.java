package mx.gob.jgtjo.apps.schedule.service.impl;

import java.text.DateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.TimeZone;

import mx.gob.jgtjo.apps.schedule.dao.AudienciaOralDao;
import mx.gob.jgtjo.apps.schedule.dao.JuezDao;
import mx.gob.jgtjo.apps.schedule.model.AudienciaOral;
import mx.gob.jgtjo.apps.schedule.model.CausaPenal;
import mx.gob.jgtjo.apps.schedule.model.Juez;
import mx.gob.jgtjo.apps.schedule.model.TipoAudiencia;
import mx.gob.jgtjo.apps.schedule.service.JuezService;

import org.apache.commons.collections.ListUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service("juezService")
public class JuezServiceImpl implements JuezService {

     public static final String GET_JUEZ_FROM_VINCULATION = "SELECT juez FROM Juez AS juez JOIN juez.audienciasOrales AS audiencia WHERE (audiencia.causaPenal.numeroCausa = :numeroCausa AND audiencia.causaPenal.annioCausa = :annioCausa) AND audiencia.seVinculoAlProceso = true";
     public static final String GET_BY_NAMES_HQL = "FROM Juez WHERE nombres = :nombres ";
     public static final String GET_JUECES_NO_OCUPADOS_EN_FECHA = "SELECT juez FROM Juez as juez JOIN juez.audienciasOrales as audiencia WHERE audiencia.fechaInicio != :horaAudiencia AND juez.activo = true";
     public static final String GET_LAST_AUDIENCIA_DE_FORMULACION = "FROM AudienciaOral audiencia WHERE tipoAudiencia.formulacion = true AND causaPenal.numeroCausa = :numeroCausa AND causaPenal.annioCausa = :annioCausa ORDER BY fechaInicio DESC";
     public static final String GET_JUEZ_QUE_CONCILIO_HQL = "SELECT juez FROM Juez AS juez JOIN juez.audienciasOrales AS audiencia WHERE (audiencia.causaPenal.numeroCausa = :numeroCausa AND audiencia.causaPenal.annioCausa = :annioCausa) AND audiencia.seConcilio = true";
     public static final String GET_JUEZ_QUE_SUSPENDIO_HQL = "SELECT juez FROM Juez AS juez JOIN juez.audienciasOrales AS audiencia WHERE (audiencia.causaPenal.numeroCausa = :numeroCausa AND audiencia.causaPenal.annioCausa = :annioCausa) AND audiencia.seSuspendio = true";
     public static final String GET_JUEZ_DE_VINCULACION_Y_MEDIDAS_HQL = "SELECT juez FROM Juez AS juez JOIN juez.audienciasOrales AS audiencia WHERE (audiencia.causaPenal.numeroCausa = :numeroCausa AND audiencia.causaPenal.annioCausa = :annioCausa) AND (audiencia.seVinculoAlProceso = true AND audiencia.seImpusoMedidasCautelares = true)";
     public static final String GET_ALL_JUECES_HQL = "FROM Juez ORDER BY apellidoPaterno";

     public static final String ANNIO_CAUSA_PARAMETER_NAME = "annioCausa";
     public static final String NUMERO_CAUSA_PARAMETER_NAME = "numeroCausa";
     public static final String HORA_AUDIENCIA_PARAMETER_NAME = "horaAudiencia";
     public static final String JUEZ_NAMES_PARAMETER_NAME = "nombres";

     @Autowired
     private JuezDao juezDao;

     @Autowired
     private AudienciaOralDao audienciaOralDao;

     public JuezServiceImpl() {

     }

     @Override
     @Transactional(readOnly = true)
     public List<Juez> loadAll() {
          return this.juezDao.createQuery(GET_ALL_JUECES_HQL);
     }

     @Override
     @Transactional
     public void save(Juez juez) {
          this.juezDao.save(juez);
     }

     @Override
     @Transactional(readOnly = true)
     public Juez findByNames(String names) {
          Map<String, Object> parameterNameAndValues = new HashMap<String, Object>();

          parameterNameAndValues.put(JUEZ_NAMES_PARAMETER_NAME, names);

          return (Juez) this.juezDao.createQuery(GET_BY_NAMES_HQL,
                    parameterNameAndValues).get(0);
     }

     // Algoritmo de Sugerencia de Juez
     @Override
     @Transactional(readOnly = true)
     public Juez suggestJuezForAudiencia(CausaPenal causaPenal,
               TipoAudiencia tipoAudiencia, Date horaAudiencia,
               List<String> razones) {

          List<Juez> juecesSinAudiencia = this
                    .obtenerJuecesNoAgendadosParaFecha(horaAudiencia);

          Juez juezSugerido = null;

          if ( tipoAudiencia.getVinculacion() ) {

               // si conoce formulación, debe conocer vinculación
               juezSugerido = this.traerJuezDeUltimaFormulacion(causaPenal);

               if ( !juecesSinAudiencia.contains(juezSugerido) ) {
                    // no hay juez existente que pueda atender dos audiencias al
                    // mismo tiempo (fact of physics)
                    razones.add("El juez de formulacion ya tiene audiencia a la misma fecha/hora");
                    juezSugerido = null;
               }

               if ( juezSugerido != null ) {
                    razones.add("Juez de formulacion de imputacion.");
               }

          }

          // si conoce vinculación e imposición de medidas, debe conocer
          // revisión
          // de medidas cautelares
          if ( tipoAudiencia.getMedidaCautelar() ) {
               juezSugerido = traerJuezDeVinculacionAndMedidas(causaPenal);

               if ( !juecesSinAudiencia.contains(juezSugerido) ) {
                    // no hay juez existente que pueda atender dos audiencias al
                    // mismo tiempo (fact of physics)
                    razones.add("El juez de vinculacion y medidas ya tiene audiencia a la misma fecha/hora");
                    juezSugerido = null;
               }

          }

          // si conoce conciliación, debe conocer revisión de condiciones de
          // conciliación
          if ( tipoAudiencia.getRevisionDeCondiciones() ) {

               juezSugerido = traerJuezQueConcilio(causaPenal);

               if ( !juecesSinAudiencia.contains(juezSugerido) ) {
                    // no hay juez existente que pueda atender dos audiencias al
                    // mismo tiempo (fact of physics)
                    razones.add("El juez que concilio ya tiene audiencia a la misma fecha/hora");
                    juezSugerido = null;
               }

          }

          // si conoce suspensión del proceso a prueba, debe conocer revisión de
          // condiciones de suspensión
          if ( tipoAudiencia.getRevisionProcesoPrueba() ) {

               juezSugerido = traerJuezQueSuspendio(causaPenal);

               // no hay juez existente que pueda atender dos audiencias al
               // mismo tiempo (fact of physics)
               razones.add("El juez que suspendio ya tiene audiencia a la misma fecha/hora");
               juezSugerido = null;

          }

          // si conoce vinculacion, no debe conocer intermedia ni juicio oral
          if ( tipoAudiencia.getIntermedia() || tipoAudiencia.getJuicioOral() ) {

               juezSugerido = traerJuezDeVinculacion(causaPenal);

               if ( juecesSinAudiencia.contains(juezSugerido) ) {
                    razones.add("El juez de vinculacion fue"
                              + juezSugerido.getNombres()
                              + " se sugiere tomar un juez distinto");
               }

               juezSugerido = null;

          }

          // si conoce vinculación, no debe conocer salidas alternas (siempre y
          // cuando sean audiencias diferentes)
          if ( tipoAudiencia.getSalidaAlterna() ) {

               juezSugerido = traerJuezDeVinculacion(causaPenal);

               if ( juecesSinAudiencia.contains(juezSugerido) ) {
                    razones.add("El juez de vinculacion fue"
                              + juezSugerido.getNombres()
                              + " se sugiere tomar un juez distinto");
               }

               juezSugerido = null;

          }

          // Si va a conocer juicio oral, no debe conocer anteriores
          if ( tipoAudiencia.getJuicioOral() ) {

               List<Juez> juecesQueNoConocen = traerJuecesQueNoConocen(causaPenal);

               @SuppressWarnings("unchecked")
               List<Juez> recomendados = ListUtils.subtract(juecesSinAudiencia,
                         juecesQueNoConocen);

               StringBuilder sb = new StringBuilder();
               sb.append("Se recomienda un juez de los siguientes: ");
               for (Juez juez : recomendados) {
                    sb.append(juez.getNombres()).append(", ");
               }

          }

          // Default: Siguiente Juez por orden alfabetico, pero cual es el
          // siguiente?
          if ( juezSugerido == null ) {
               razones.add("No se encontro ningun juez que cumpliera con las reglas, "
                         + "se sugiere el siguiente juez en la agenda");

               final DateFormat fullDateFormat = DateFormat
                         .getDateTimeInstance(DateFormat.FULL, DateFormat.FULL,
                                   new Locale("es", "mx"));

               fullDateFormat.setTimeZone(TimeZone.getTimeZone("GMT-6:00"));

               if ( causaPenal.getAudienciasOrales() != null
                         && !causaPenal.getAudienciasOrales().isEmpty() ) {

                    for (AudienciaOral audiencia : causaPenal
                              .getAudienciasOrales()) {

                         for (Juez juez : audiencia.getJueces()) {

                              razones.add("El juez "
                                        + juez.getNombres()
                                        + " ha estado en "
                                        + "audiencia de "
                                        + audiencia.getTipoAudiencia()
                                                  .getNombre()
                                        + " "
                                        + " en fecha "
                                        + fullDateFormat.format(audiencia
                                                  .getFechaInicio()));
                         }

                    }

               }

          }

          return juezSugerido;
     }

     private List<Juez> traerJuecesQueNoConocen(CausaPenal causaPenal) {
          Map<String, Object> parameterNameAndValues = new HashMap<String, Object>();
          parameterNameAndValues.put(NUMERO_CAUSA_PARAMETER_NAME,
                    causaPenal.getNumeroCausa());
          parameterNameAndValues.put(ANNIO_CAUSA_PARAMETER_NAME,
                    causaPenal.getAnnioCausa());

          return this.juezDao
                    .createQuery(
                              "SELECT juez FROM Juez AS juez JOIN juez.audienciasOrales AS audiencia"
                                        + "WHERE audiencia.causaPenal.numeroCausa != :numeroCausa"
                                        + "AND audiencia.causaPenal.annioCausa = :annioCausa",
                              parameterNameAndValues);
     }

     private Juez traerJuezDeVinculacion(CausaPenal causaPenal) {
          Map<String, Object> parameterNameAndValues = new HashMap<String, Object>();
          parameterNameAndValues.put(NUMERO_CAUSA_PARAMETER_NAME,
                    causaPenal.getNumeroCausa());
          parameterNameAndValues.put(ANNIO_CAUSA_PARAMETER_NAME,
                    causaPenal.getAnnioCausa());

          return this.juezDao.createQueryAndReturnUniqueResult(
                    GET_JUEZ_FROM_VINCULATION, parameterNameAndValues);

     }

     private Juez traerJuezQueSuspendio(CausaPenal causaPenal) {
          Map<String, Object> parameterNameAndValues = new HashMap<String, Object>();
          parameterNameAndValues.put(NUMERO_CAUSA_PARAMETER_NAME,
                    causaPenal.getNumeroCausa());
          parameterNameAndValues.put(ANNIO_CAUSA_PARAMETER_NAME,
                    causaPenal.getAnnioCausa());

          return juezDao.createQueryAndReturnUniqueResult(
                    GET_JUEZ_QUE_SUSPENDIO_HQL, parameterNameAndValues);
     }

     private Juez traerJuezQueConcilio(CausaPenal causaPenal) {
          Map<String, Object> parameterNameAndValues = new HashMap<String, Object>();
          parameterNameAndValues.put(NUMERO_CAUSA_PARAMETER_NAME,
                    causaPenal.getNumeroCausa());
          parameterNameAndValues.put(ANNIO_CAUSA_PARAMETER_NAME,
                    causaPenal.getAnnioCausa());

          return juezDao.createQueryAndReturnUniqueResult(
                    GET_JUEZ_QUE_CONCILIO_HQL, parameterNameAndValues);
     }

     private Juez traerJuezDeVinculacionAndMedidas(CausaPenal causaPenal) {
          Map<String, Object> parameterNameAndValues = new HashMap<String, Object>();
          parameterNameAndValues.put(NUMERO_CAUSA_PARAMETER_NAME,
                    causaPenal.getNumeroCausa());
          parameterNameAndValues.put(ANNIO_CAUSA_PARAMETER_NAME,
                    causaPenal.getAnnioCausa());

          return this.juezDao.createQueryAndReturnUniqueResult(
                    GET_JUEZ_DE_VINCULACION_Y_MEDIDAS_HQL,
                    parameterNameAndValues);
     }

     private Juez traerJuezDeUltimaFormulacion(CausaPenal causaPenal) {

          Map<String, Object> parameterNameAndValues = new HashMap<String, Object>();
          parameterNameAndValues.put(NUMERO_CAUSA_PARAMETER_NAME,
                    causaPenal.getNumeroCausa());
          parameterNameAndValues.put(ANNIO_CAUSA_PARAMETER_NAME,
                    causaPenal.getAnnioCausa());

          List<AudienciaOral> audiencias = this.audienciaOralDao.createQuery(
                    GET_LAST_AUDIENCIA_DE_FORMULACION, parameterNameAndValues);

          return audiencias.get(audiencias.size() - 1).getJueces().get(0);
     }

     private List<Juez> obtenerJuecesNoAgendadosParaFecha(Date fechaAudiencia) {
          Map<String, Object> parameterNameAndValues = new HashMap<String, Object>();
          parameterNameAndValues.put(HORA_AUDIENCIA_PARAMETER_NAME,
                    fechaAudiencia);

          return this.juezDao.createQuery(GET_JUECES_NO_OCUPADOS_EN_FECHA,
                    parameterNameAndValues);
     }

     public JuezDao getJuezDao() {
          return juezDao;
     }

     public void setJuezDao(JuezDao juezDao) {
          this.juezDao = juezDao;
     }

     public AudienciaOralDao getAudienciaOralDao() {
          return audienciaOralDao;
     }

     public void setAudienciaOralDao(AudienciaOralDao audienciaOralDao) {
          this.audienciaOralDao = audienciaOralDao;
     }

}
