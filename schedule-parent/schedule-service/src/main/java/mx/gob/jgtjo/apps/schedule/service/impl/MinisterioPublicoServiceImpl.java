package mx.gob.jgtjo.apps.schedule.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import mx.gob.jgtjo.apps.schedule.dao.MinisterioPublicoDao;
import mx.gob.jgtjo.apps.schedule.model.MinisterioPublico;
import mx.gob.jgtjo.apps.schedule.service.MinisterioPublicoService;

@Service("ministerioPublicoService")
public class MinisterioPublicoServiceImpl implements MinisterioPublicoService {

	public static final String LOAD_ALL_MPS_HQL = "FROM MinisterioPublico";

	@Autowired
	private MinisterioPublicoDao ministerioPublicoDao;

	public MinisterioPublicoServiceImpl() {

	}

	@Override
	@Transactional(readOnly = true)
	public MinisterioPublico loadById(Integer id) {
		return this.ministerioPublicoDao.load(id);
	}

	@Override
	@Transactional(readOnly = true)
	public List<MinisterioPublico> loadAll() {
		return this.ministerioPublicoDao.createQuery(LOAD_ALL_MPS_HQL);
	}

	@Override
	@Transactional
	public void save(MinisterioPublico ministerioPublico) {
		this.ministerioPublicoDao.save(ministerioPublico);

	}

	public MinisterioPublicoDao getMinisterioPublicoDao() {
		return ministerioPublicoDao;
	}

	public void setMinisterioPublicoDao(MinisterioPublicoDao ministerioPublicoDao) {
		this.ministerioPublicoDao = ministerioPublicoDao;
	}

}
