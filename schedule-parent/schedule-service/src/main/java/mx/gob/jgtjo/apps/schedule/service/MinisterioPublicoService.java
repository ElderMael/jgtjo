package mx.gob.jgtjo.apps.schedule.service;

import java.util.List;

import mx.gob.jgtjo.apps.schedule.model.MinisterioPublico;

public interface MinisterioPublicoService {

	MinisterioPublico loadById(Integer id);
	
	List<MinisterioPublico> loadAll();
	
	void save(MinisterioPublico ministerioPublico);

}
