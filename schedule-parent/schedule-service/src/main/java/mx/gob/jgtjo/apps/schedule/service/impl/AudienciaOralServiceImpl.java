package mx.gob.jgtjo.apps.schedule.service.impl;

import java.io.Serializable;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import mx.gob.jgtjo.apps.schedule.dao.AudienciaOralDao;
import mx.gob.jgtjo.apps.schedule.model.AudienciaOral;
import mx.gob.jgtjo.apps.schedule.model.CausaPenal;
import mx.gob.jgtjo.apps.schedule.service.AudienciaOralService;

@Service("audienciaOralService")
public class AudienciaOralServiceImpl implements AudienciaOralService, Serializable {

	private static final long serialVersionUID = 1L;

	public static final String ID_CAUSA_PARAMETER_NAME = "id";
	public static final String RESULT_MAP_COUNT_KEY = "count";
	public static final String GET_NUMBER_OF_AUDIENCIAS_HQL = "SELECT new Map( COUNT(*) AS count ) FROM AudienciaOral audiencia WHERE audiencia.causaPenal.id = :id AND audiencia.seCancelo = false";
	public static final String LOAD_AUDIENCIAS_IN_TIMEFRAME_HQL = "FROM AudienciaOral WHERE fechaInicio BETWEEN :fechaInicio AND :fechaFin";
	public static final String LOAD_AUDIENCIAS_IN_TIMEFRAME_WITH_SALA_HQL = "FROM AudienciaOral WHERE sala.id = :salaId AND fechaInicio BETWEEN :fechaInicio AND :fechaFin";
	public static final String FECHA_FIN_PARAMETER_NAME = "fechaFin";
	public static final String FECHA_INICIO_PARAMETER_NAME = "fechaInicio";
	public static final String SALA_ID_INICIO_PARAMETER_NAME = "salaId";

	@Autowired
	private AudienciaOralDao audienciaOralDao;

	public AudienciaOralServiceImpl() {

	}

	@Override
	@Transactional
	public void save(AudienciaOral audienciaOral) {
		this.audienciaOralDao.save(audienciaOral);
	}

	@Override
	@Transactional
	public void update(AudienciaOral audienciaOral) {
		this.audienciaOralDao.update(audienciaOral);
	}

	@Override
	@Transactional(readOnly = true)
	public List<AudienciaOral> loadAudienciasInTimeFrame(Date startDate, Date endDate) {
		// OMFG
		Map<String, Object> parameterNameAndValues = new HashMap<String, Object>();

		parameterNameAndValues.put(FECHA_INICIO_PARAMETER_NAME, startDate);
		parameterNameAndValues.put(FECHA_FIN_PARAMETER_NAME, endDate);

		return this.audienciaOralDao.createQuery(LOAD_AUDIENCIAS_IN_TIMEFRAME_HQL,
				parameterNameAndValues);

	}

	@Override
	@Transactional(readOnly = true)
	public List<AudienciaOral> loadAudienciasInTimeFrame(Date startDate, Date endDate,
			Integer salaId) {
		// OMFG
		Map<String, Object> parameterNameAndValues = new HashMap<String, Object>();

		parameterNameAndValues.put(FECHA_INICIO_PARAMETER_NAME, startDate);
		parameterNameAndValues.put(FECHA_FIN_PARAMETER_NAME, endDate);
		parameterNameAndValues.put(SALA_ID_INICIO_PARAMETER_NAME, salaId);

		return this.audienciaOralDao.createQuery(LOAD_AUDIENCIAS_IN_TIMEFRAME_WITH_SALA_HQL,
				parameterNameAndValues);

	}

	@Override
	@Transactional(readOnly = true)
	public Integer getNumberOfAudienciasInCausa(CausaPenal causaPenal) {

		Map<String, Long> result = this.audienciaOralDao.createQueryAndReturnFirstMap(
				GET_NUMBER_OF_AUDIENCIAS_HQL, ID_CAUSA_PARAMETER_NAME, causaPenal.getId(),
				Long.class);

		return result.get(RESULT_MAP_COUNT_KEY).intValue();
	}

	@Override
	@Transactional
	public List<AudienciaOral> findByCausaNumberAndYear(Integer causaNumber, Integer causaYear) {

		Map<String, Object> parameterNameAndValues = new HashMap<String, Object>();

		parameterNameAndValues.put("causaNumber", causaNumber);
		parameterNameAndValues.put("causaYear", causaYear);

		return this.audienciaOralDao.createQuery("FROM AudienciaOral audiencia "
				+ "WHERE audiencia.causaPenal.numeroCausa = :causaNumber" + " AND "
				+ "audiencia.causaPenal.annioCausa = :causaYear", parameterNameAndValues);

	}

	public AudienciaOralDao getAudienciaOralDao() {
		return audienciaOralDao;
	}

	public void setAudienciaOralDao(AudienciaOralDao audienciaOralDao) {
		this.audienciaOralDao = audienciaOralDao;
	}

}
