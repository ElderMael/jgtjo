package mx.gob.jgtjo.apps.schedule.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import mx.gob.jgtjo.apps.schedule.dao.SalaDao;
import mx.gob.jgtjo.apps.schedule.model.Sala;
import mx.gob.jgtjo.apps.schedule.service.SalaService;

@Service("salaService")
public class SalaServiceImpl implements SalaService {

	public static final String LOAD_ALL_SALAS_HQL = "FROM Sala";
	@Autowired
	private SalaDao salaDao;

	public SalaServiceImpl() {

	}

	@Override
	@Transactional(readOnly = true)
	public List<Sala> loadAll() {
		return this.salaDao.createQuery(LOAD_ALL_SALAS_HQL);
	}

	@Override
	@Transactional
	public void save(Sala sala) {
		this.salaDao.save(sala);

	}

	@Override
	@Transactional(readOnly = true)
	public Sala loadById(Integer id) {
		return this.salaDao.load(id);
	}

	public SalaDao getSalaDao() {
		return salaDao;
	}

	public void setSalaDao(SalaDao salaDao) {
		this.salaDao = salaDao;
	}

}
