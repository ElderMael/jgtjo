package mx.gob.jgtjo.apps.schedule.service;

import java.util.Date;
import java.util.List;

import mx.gob.jgtjo.apps.schedule.model.CausaPenal;

/**
 * 
 * @author ElderMael
 * 
 */
public interface CausaPenalService {

	/**
	 * Persite una causa penal en base de datos. El objeto pasado como parametro
	 * sera modificado para incorporarle el identificador proporcionado en basde
	 * de datos.
	 * 
	 * @param nuevaCausa
	 *            - La nueva causa penal a persistir.
	 */
	public void save(CausaPenal nuevaCausa);

	/**
	 * Genera un nuevo numero de causa penal para el año actual (proporcionado
	 * por la creacion de una {@link Date} con el constructor por defecto.
	 * 
	 * Debe aclararse que dicho numero no es generado por la base de datos sino
	 * que se crea una consulta haciendo un conteo de las causas penales para el
	 * año actual y regresando dicho resultado mas uno.
	 * 
	 * Esto con la finalidad de evitar bloquear numeros de causa en caso de que
	 * no se haya guardado una nueva causa penal al haber solicitado el numero
	 * por medio de este metodo.
	 * 
	 * @return El nuevo numero de causa penal para el año actual.
	 */
	public Integer generateNewCausaPenalNumber();

	/**
	 * Genera un nuevo numero de causa penal para el año especificado.
	 * 
	 * @param annio
	 *            - El año para el cual
	 * @return El nuevo numero de causa penal para el año especificado.
	 * 
	 * @see CausaPenalService#generateNewCausaPenalNumber()
	 */
	public Integer generateNewCausaPenalNumber(Integer annio);

	/**
	 * Obtiene todas las causas penales persistidas desde base de datos.
	 * 
	 * @return Todas las causas penales en base de datos.
	 */
	public List<CausaPenal> loadAllCausas();

	/**
	 * Busca en base de datos la causa penal cuyo numero y año son iguales a los
	 * proporcionados en los argumentos.
	 * 
	 * @param causaNumber
	 *            - Numero de causa.
	 * @param causaYear
	 *            - Año de la causa.
	 * @return La causa penal cuyo numero y año coincien con los argumentos.
	 */
	public CausaPenal findByNumberAndYear(Integer causaNumber, Integer causaYear);

	/**
	 * Obtiene el numero de audiencias orales celebradas dentro de una causa
	 * penal.
	 * 
	 * @param id
	 *            - El id de la Causa Penal
	 * @return - El numero de audiencias celebradas en la causa cuyo id se
	 *         proporciona.
	 */
	public Integer getNumberOfAudiencias(Integer id);

}
