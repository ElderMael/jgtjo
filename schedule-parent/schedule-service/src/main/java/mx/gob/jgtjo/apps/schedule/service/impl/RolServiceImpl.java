package mx.gob.jgtjo.apps.schedule.service.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import mx.gob.jgtjo.apps.schedule.dao.RolDao;
import mx.gob.jgtjo.apps.schedule.dao.hibernate.HibernateBaseDao;
import mx.gob.jgtjo.apps.schedule.model.Rol;
import mx.gob.jgtjo.apps.schedule.service.RolService;

@Service("rolService")
public class RolServiceImpl implements RolService {

	private static final String AUTHORITY_PARAMETER_NAME = "authority";

	public static final String FIND_ROLE_BY_AUTHORITY_HQL = "FROM Rol WHERE authority = :authority";

	public static final String GET_ALL_ROLES_HQL = "FROM Rol";

	@Autowired
	private RolDao rolDao;

	@Override
	@Transactional
	public void save(Rol rol) {
		this.rolDao.save(rol);
	}

	@Override
	@Transactional
	public void save(Rol rol, Boolean flushAndClear) {
		if (flushAndClear) {
			this.rolDao.save(rol);
			this.castDaoToBase().getSessionFactory().getCurrentSession().flush();
			this.castDaoToBase().getSessionFactory().getCurrentSession().clear();
		}

	}

	public RolDao getRolDao() {
		return this.rolDao;
	}

	@Override
	@Transactional(readOnly = true)
	public Rol load(Integer id) {
		return this.rolDao.load(id);
	}

	@Override
	@Transactional(readOnly = true)
	public List<Rol> loadAll() {
		return this.rolDao.createQuery(GET_ALL_ROLES_HQL);
	}

	@Override
	@Transactional(readOnly = true)
	public Rol findByAuthorityString(String authorityString) {
		Map<String, Object> parameterNameAndValues = new HashMap<String, Object>();
		parameterNameAndValues.put(AUTHORITY_PARAMETER_NAME, authorityString);

		return this.rolDao.createQueryAndReturnUniqueResult(FIND_ROLE_BY_AUTHORITY_HQL,
				parameterNameAndValues);
	}

	public void setRolDao(RolDao rolDao) {
		this.rolDao = rolDao;
	}

	@SuppressWarnings("unchecked")
	private HibernateBaseDao<Rol, Integer> castDaoToBase() {
		return (HibernateBaseDao<Rol, Integer>) this.rolDao;
	}

}
