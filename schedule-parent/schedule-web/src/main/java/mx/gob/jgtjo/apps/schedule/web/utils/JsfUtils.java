package mx.gob.jgtjo.apps.schedule.web.utils;

import java.text.MessageFormat;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.ResourceBundle;

import javax.faces.application.FacesMessage;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

/**
 * Metodos de utileria para JSF 2
 * 
 * @author ElderMael
 * 
 */
public class JsfUtils {

	public static ServletContext getCurrentServletContext() {
		return (ServletContext) FacesContext.getCurrentInstance().getExternalContext().getContext();
	}

	public static HttpServletResponse getCurrentHttpResponse() {
		return (HttpServletResponse) FacesContext.getCurrentInstance().getExternalContext()
				.getResponse();
	}

	public static <T> T getBeanFromWebApplicationContext(Class<T> requiredType) {
		return JsfUtils.getRequiredWebApplicationContext(JsfUtils.getCurrentHttpRequest()).getBean(
				requiredType);
	}

	public static Object getCurrentSessionAttribute(String key) {
		return JsfUtils.getCurrentHttpRequest().getSession().getAttribute(key);
	}

	public static ExternalContext getCurrentExternalContext() {
		return JsfUtils.getCurrentFacesContext().getExternalContext();
	}

	public static FacesContext getCurrentFacesContext() {
		return FacesContext.getCurrentInstance();
	}

	public static HttpServletRequest getCurrentHttpRequest() {
		return JsfUtils.getCurrentHttpRequest(JsfUtils.getCurrentFacesContext());
	}

	public static HttpServletRequest getCurrentHttpRequest(FacesContext facesContext) {
		return (HttpServletRequest) facesContext.getExternalContext().getRequest();
	}

	public static WebApplicationContext getRequiredWebApplicationContext(HttpServletRequest request) {

		return WebApplicationContextUtils.getRequiredWebApplicationContext(request
				.getServletContext());
	}

	public static void addNewFacesMessage(FacesContext facesContext, String clientId,
			FacesMessage.Severity severity, String summary, String detail) {

		FacesMessage message = new FacesMessage(severity, summary, detail);

		facesContext.addMessage(clientId, message);

	}

	public static void addNewFacesMessage(String clientId, FacesMessage.Severity severity,
			String summary, String detail) {

		JsfUtils.addNewFacesMessage(JsfUtils.getCurrentFacesContext(), clientId, severity, summary,
				detail);

	}

	public static void addNewFacesMessageFromBundle(FacesContext facesContext,
			FacesMessage.Severity severity, String clientId, String key, Object[] summaryArgs,
			Object[] detailArgs) {

		String bundleName = facesContext.getApplication().getMessageBundle();
		Locale locale = facesContext.getViewRoot().getLocale();

		if (bundleName == null) {
			bundleName = Constantes.DEFAULT_CUSTOM_FACES_MESSAGE_BUNDLE_NAME;
		}

		ResourceBundle bundle = ResourceBundle.getBundle(bundleName, locale);

		String summary = bundle.getString(key);
		String detail = bundle.getString(key + "_detail");

		summary = MessageFormat.format(summary, summaryArgs);
		detail = MessageFormat.format(detail, detailArgs);

		JsfUtils.addNewFacesMessage(facesContext, clientId, severity, summary, detail);

	}

	public static Map<String, String> getMessagesFromDefaultBundle(FacesContext context, String key) {
		String bundleName = context.getApplication().getMessageBundle();
		Locale locale = context.getViewRoot().getLocale();

		if (bundleName == null) {
			bundleName = Constantes.DEFAULT_CUSTOM_FACES_MESSAGE_BUNDLE_NAME;
		}

		ResourceBundle bundle = ResourceBundle.getBundle(bundleName, locale);

		Map<String, String> retVal = new HashMap<String, String>(2);

		retVal.put("summary", bundle.getString(key));
		retVal.put("detail", bundle.getString(key + "_detail"));

		return retVal;
	}

	public static void addNewFacesMessageFromBundle(FacesMessage.Severity severity,
			String clientId, String key, Object[] summaryArgs, Object[] detailArgs) {
		FacesContext facesContext = JsfUtils.getCurrentFacesContext();
		JsfUtils.addNewFacesMessageFromBundle(facesContext, severity, clientId, key, summaryArgs,
				detailArgs);
	}

	public static void addNewFacesMessageFromBundle(FacesMessage.Severity severity,
			String clientId, String key) {

		FacesContext facesContext = JsfUtils.getCurrentFacesContext();

		String bundleName = facesContext.getApplication().getMessageBundle();
		Locale locale = facesContext.getViewRoot().getLocale();

		if (bundleName == null) {
			bundleName = Constantes.DEFAULT_CUSTOM_FACES_MESSAGE_BUNDLE_NAME;
		}

		ResourceBundle bundle = ResourceBundle.getBundle(bundleName, locale);

		String summary = bundle.getString(key);
		String detail = bundle.getString(key + "_detail");

		JsfUtils.addNewFacesMessage(facesContext, clientId, severity, summary, detail);

	}

	public static Locale getViewLocale(FacesContext context) {
		return context.getViewRoot().getLocale();
	}

	public static Locale getViewLocale() {
		return JsfUtils.getViewLocale(FacesContext.getCurrentInstance());
	}

}
