package mx.gob.jgtjo.apps.schedule.web.view.beans;

import java.io.Serializable;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import mx.gob.jgtjo.apps.schedule.model.Delito;
import mx.gob.jgtjo.apps.schedule.web.services.ScheduleServiceLocator;
import mx.gob.jgtjo.apps.schedule.web.utils.Constantes;
import mx.gob.jgtjo.apps.schedule.web.utils.JsfUtils;
import mx.gob.jgtjo.apps.schedule.web.view.beans.model.DelitoBaseDataModel;

@ManagedBean(name = "delitosBean")
@ViewScoped
public class DelitosBean implements Serializable {

     private static final long serialVersionUID = 1L;

     private static final Logger log = LoggerFactory
               .getLogger(DelitosBean.class);

     @ManagedProperty(value = "#{serviceLocator}")
     private ScheduleServiceLocator serviceLocator;

     private Delito delito = new Delito();

     private DelitoBaseDataModel dataModel;

     private Delito selectedDelito;

     public DelitosBean() {

     }

     @PostConstruct
     public void loadDelitos() {
          this.dataModel = new DelitoBaseDataModel(this.serviceLocator
                    .getDelitoService().loadAll());

     }

     @SuppressWarnings("unchecked")
     public void save() {

          this.serviceLocator.getDelitoService().save(this.delito);
          
          ((List<Delito>) this.dataModel.getWrappedData()).add(this.delito);

          this.delito = new Delito();

          JsfUtils.addNewFacesMessageFromBundle(FacesMessage.SEVERITY_INFO,
                    null, Constantes.DELITOS_ADDED_DELITO_KEY, new Object[] {},
                    new Object[] {});
     }

     @SuppressWarnings("unchecked")
     public String saveAll() {

          return null;
     }

     @SuppressWarnings("unchecked")
     public void delete() {
          log.debug("Deleting {}", this.selectedDelito);

          if ( ((List<Delito>) this.dataModel.getWrappedData())
                    .remove(selectedDelito) ) {

               if ( selectedDelito.getId() != null ) {
                    this.serviceLocator.getDelitoService().delete(
                              selectedDelito);
               }

               JsfUtils.addNewFacesMessageFromBundle(
                         FacesMessage.SEVERITY_INFO, null,
                         Constantes.DELITOS_REMOVED_DELITO_KEY,
                         new Object[] { selectedDelito.getNombre() },
                         new Object[] {});

          }

     }

     public ScheduleServiceLocator getServiceLocator() {
          return serviceLocator;
     }

     public void setServiceLocator(ScheduleServiceLocator serviceLocator) {
          this.serviceLocator = serviceLocator;
     }

     public String getNombre() {
          return delito.getNombre();
     }

     public void setNombre(String nombre) {
          delito.setNombre(nombre);
     }

     public Delito getDelito() {
          return delito;
     }

     public void setDelito(Delito delito) {
          this.delito = delito;
     }

     public Delito getSelectedDelito() {
          return selectedDelito;
     }

     public void setSelectedDelito(Delito selectedDelito) {
          this.selectedDelito = selectedDelito;
     }

     public DelitoBaseDataModel getDataModel() {
          return dataModel;
     }

     public void setDataModel(DelitoBaseDataModel dataModel) {
          this.dataModel = dataModel;
     }

}
