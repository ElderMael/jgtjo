package mx.gob.jgtjo.apps.schedule.web.conversation;

import java.io.IOException;
import java.util.UUID;

import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.logout.LogoutSuccessHandler;

import mx.gob.jgtjo.apps.schedule.web.utils.Constantes;

public class ConversationTerminatorLogoutSuccessHandler implements LogoutSuccessHandler {

	protected static final Logger log = LoggerFactory
			.getLogger(ConversationTerminatorLogoutSuccessHandler.class);

	public ConversationTerminatorLogoutSuccessHandler() {

	}

	@Override
	public void onLogoutSuccess(HttpServletRequest request, HttpServletResponse response,
			Authentication authentication) throws IOException, ServletException {

		Cookie conversationIdCookie = lookupConversationCookie(request);

		if (conversationIdCookie != null) {
			if (log.isDebugEnabled()) {
				log.debug("Ending conversation with id '{}' after user logout",
						conversationIdCookie.getValue());
			}

			ConversationManager.endConversation(UUID.fromString(conversationIdCookie.getValue()));

			conversationIdCookie.setMaxAge(0); // Delete cookie from browser

			response.addCookie(conversationIdCookie);
		}

		response.sendRedirect(request.getContextPath() + "/");

	}

	private Cookie lookupConversationCookie(HttpServletRequest request) {

		if (request.getCookies() == null)
			return null;

		for (Cookie cookie : request.getCookies()) {

			if (cookie.getName().equals(Constantes.ACTIVE_CONVERSATION_COOKIE_NAME)) {
				return cookie;
			}

		}

		return null;
	}

}
