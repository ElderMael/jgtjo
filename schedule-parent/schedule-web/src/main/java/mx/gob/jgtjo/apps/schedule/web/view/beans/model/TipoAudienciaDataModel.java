package mx.gob.jgtjo.apps.schedule.web.view.beans.model;

import java.io.Serializable;
import java.util.List;

import javax.faces.model.ListDataModel;

import org.primefaces.model.SelectableDataModel;

import mx.gob.jgtjo.apps.schedule.model.TipoAudiencia;

public class TipoAudienciaDataModel extends ListDataModel<TipoAudiencia> implements
		SelectableDataModel<TipoAudiencia>, Serializable {

	private static final long serialVersionUID = 1L;

	public TipoAudienciaDataModel() {

	}

	public TipoAudienciaDataModel(List<TipoAudiencia> data) {
		super(data);
	}

	@Override
	public Object getRowKey(TipoAudiencia object) {
		return object.getNombre();
	}

	@Override
	@SuppressWarnings("unchecked")
	public TipoAudiencia getRowData(String rowKey) {

		List<TipoAudiencia> tiposAudiencias = (List<TipoAudiencia>) this.getWrappedData();

		for (TipoAudiencia tipoAudiencia : tiposAudiencias) {
			if (tipoAudiencia.getNombre().equals(rowKey)) {
				return tipoAudiencia;
			}
		}

		return null;
	}

}
