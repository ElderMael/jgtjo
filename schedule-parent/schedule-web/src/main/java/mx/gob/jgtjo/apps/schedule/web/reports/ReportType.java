package mx.gob.jgtjo.apps.schedule.web.reports;

public enum ReportType {

	CONTROL_EVENTOS_AUDIENCIA,

	DATOS_PARA_JUEZ,

	PROTOCOLO_DE_AUDIENCIA

}
