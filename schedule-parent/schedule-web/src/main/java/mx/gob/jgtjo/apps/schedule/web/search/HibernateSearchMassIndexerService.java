package mx.gob.jgtjo.apps.schedule.web.search;

import org.hibernate.Session;

public interface HibernateSearchMassIndexerService {

	void buildSearchIndex(Session session, Class<?> classToIndex);

}
