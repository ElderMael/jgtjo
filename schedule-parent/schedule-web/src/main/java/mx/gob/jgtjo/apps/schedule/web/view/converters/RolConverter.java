package mx.gob.jgtjo.apps.schedule.web.view.converters;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;

import mx.gob.jgtjo.apps.schedule.model.Rol;
import mx.gob.jgtjo.apps.schedule.service.RolService;
import mx.gob.jgtjo.apps.schedule.web.utils.JsfUtils;

public class RolConverter implements Converter {

	private RolService rolService;

	public RolConverter() {
		this.rolService = JsfUtils.getBeanFromWebApplicationContext(RolService.class);
	}

	@Override
	public Object getAsObject(FacesContext context, UIComponent component, String value) {
		return this.getRolService().findByAuthorityString(value);
	}

	@Override
	public String getAsString(FacesContext context, UIComponent component, Object value) {

		String returnVal = ((Rol) value).getAuthority();

		return returnVal;
	}

	public RolService getRolService() {
		return rolService;
	}

	public void setRolService(RolService rolService) {
		this.rolService = rolService;
	}

}
