package mx.gob.jgtjo.apps.schedule.web.view.beans;

import java.io.Serializable;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.event.ValueChangeEvent;
import javax.faces.model.SelectItem;

import org.primefaces.event.DateSelectEvent;
import org.primefaces.model.DualListModel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import mx.gob.jgtjo.apps.schedule.model.AudienciaOral;
import mx.gob.jgtjo.apps.schedule.model.CausaPenal;
import mx.gob.jgtjo.apps.schedule.model.Juez;
import mx.gob.jgtjo.apps.schedule.model.Sala;
import mx.gob.jgtjo.apps.schedule.model.TipoAudiencia;
import mx.gob.jgtjo.apps.schedule.web.services.ScheduleServiceLocator;
import mx.gob.jgtjo.apps.schedule.web.utils.Constantes;
import mx.gob.jgtjo.apps.schedule.web.utils.JsfUtils;
import mx.gob.jgtjo.apps.schedule.web.utils.ScheduleUtils;
import mx.gob.jgtjo.apps.schedule.web.view.beans.model.CausaPenalSelectableDataModel;

@ManagedBean(name = "agendarCausaBean")
@ViewScoped
public class AgendarCausaBean implements Serializable {

	private static final long serialVersionUID = 1L;

	private static final Logger log = LoggerFactory.getLogger(AgendarCausaBean.class);

	@ManagedProperty(value = "#{serviceLocator}")
	private ScheduleServiceLocator serviceLocator;

	private String causaNumberAndYear;

	private Date fechaInicio;

	private Date fechaFin;

	private Integer tipoAudiencia;

	private DualListModel<Juez> juezPickModel;

	private Integer idSala;

	private String keywords;

	private Juez juezSugerido;

	private CausaPenal causaPenalSeleccionada;

	private List<String> sugerencias = new LinkedList<String>();

	private CausaPenalSelectableDataModel causasEncontradasDataModel = new CausaPenalSelectableDataModel(
			new LinkedList<CausaPenal>());

	public void searchCausaByKeywords() {

		this.causasEncontradasDataModel.setWrappedData(this.getServiceLocator().getSearchService()
				.searchCausasPenales(this.keywords));

	}

	public void setCausaPenalFromSearch() {

		if (this.causaPenalSeleccionada == null) {
			JsfUtils.addNewFacesMessageFromBundle(FacesMessage.SEVERITY_WARN, null,
					Constantes.MISSING_CAUSA_SELECTED);

			return;
		}
		this.causaNumberAndYear = ScheduleUtils.formatCausaNumberAndYear(
				this.causaPenalSeleccionada.getNumeroCausa(),
				this.causaPenalSeleccionada.getAnnioCausa());
	}

	public AgendarCausaBean() {

		Object numeroCausaAtt = JsfUtils
				.getCurrentSessionAttribute(Constantes.NEW_CAUSA_PENAL_ID_ATTRIBUTE_NAME);

		Object annioCausaAtt = JsfUtils
				.getCurrentSessionAttribute(Constantes.NEW_CAUSA_PENAL_YEAR_ATTRIBUTE_NAME);

		log.debug("Checking for new causa penal attribute '{}/{}'", numeroCausaAtt, annioCausaAtt);

		if (numeroCausaAtt == null || annioCausaAtt == null) {
			return;
		}

		Integer numeroCausa = (Integer) numeroCausaAtt;
		Integer annioCausa = (Integer) annioCausaAtt;

		this.causaNumberAndYear = ScheduleUtils.formatCausaNumberAndYear(numeroCausa, annioCausa);

	}

	public void handleDateSelect(DateSelectEvent event) {
		this.fechaFin = event.getDate();
	}

	public List<SelectItem> getTiposDeAudiencia() {

		List<TipoAudiencia> tipos = this.serviceLocator.getTipoAudienciaService().loadAll();
		List<SelectItem> items = new LinkedList<SelectItem>();

		for (TipoAudiencia tipoAudiencia : tipos) {

			items.add(new SelectItem(tipoAudiencia.getId(), tipoAudiencia.getNombre()));

		}

		return items;
	}

	public String agendarAudienciaOral() {

		String[] causaNumberAndYearFields = this.causaNumberAndYear.split("/");

		Integer causaNumber = Integer.parseInt(causaNumberAndYearFields[0]);
		Integer causaYear = Integer.parseInt(causaNumberAndYearFields[1]);

		CausaPenal causaPenal = this.serviceLocator.getCausaPenalService().findByNumberAndYear(
				causaNumber, causaYear);

		TipoAudiencia tipoAudienciaObject = this.serviceLocator.getTipoAudienciaService().loadById(
				this.tipoAudiencia);

		Sala sala = this.serviceLocator.getSalaService().loadById(this.idSala);

		AudienciaOral audienciaOral = new AudienciaOral(null, null, causaPenal, null,
				this.fechaInicio, this.fechaFin, sala, tipoAudienciaObject,
				this.juezPickModel.getTarget(), null, null);

		Integer numeroAudiencia = this.getServiceLocator().getAudienciaOralService()
				.getNumberOfAudienciasInCausa(causaPenal);

		audienciaOral.setNumeroAudiencia(numeroAudiencia + 1);

		this.serviceLocator.getAudienciaOralService().save(audienciaOral);

		log.debug("Agendanda audiencia:{}", audienciaOral);

		return "pretty:agendarSuccess";
	}

	public List<SelectItem> getAllSalas() {

		List<Sala> salas = this.serviceLocator.getSalaService().loadAll();
		List<SelectItem> items = new LinkedList<SelectItem>();

		for (Sala sala : salas) {

			items.add(new SelectItem(sala.getId(), sala.getNombre()));

		}

		return items;

	}

	public void pickListChangeListener(ValueChangeEvent event) {

		Object source = event.getSource();
		log.debug("Clase de la fuente {}", source.getClass().getName());

	}

	public String buscarJuez() {

		String[] causaNumberAndYearFields = this.causaNumberAndYear.split("/");

		Integer causaNumber = Integer.parseInt(causaNumberAndYearFields[0]);
		Integer causaYear = Integer.parseInt(causaNumberAndYearFields[1]);

		CausaPenal causaPenal = this.serviceLocator.getCausaPenalService().findByNumberAndYear(
				causaNumber, causaYear);

		TipoAudiencia tipoAudiencia = this.serviceLocator.getTipoAudienciaService().loadById(
				this.tipoAudiencia);

		List<String> razones = new LinkedList<String>();

		this.juezSugerido = this.serviceLocator.getJuezService().suggestJuezForAudiencia(
				causaPenal, tipoAudiencia, this.fechaInicio, razones);

		this.sugerencias = razones;

		return null;
	}

	public String getCausaNumberAndYear() {
		return causaNumberAndYear;
	}

	public void setCausaNumberAndYear(String causaNumberAndYear) {
		this.causaNumberAndYear = causaNumberAndYear;
	}

	public Date getFechaInicio() {
		return fechaInicio;
	}

	public void setFechaInicio(Date fechaInicio) {
		this.fechaInicio = fechaInicio;
	}

	public Date getFechaFin() {
		return fechaFin;
	}

	public void setFechaFin(Date fechaFin) {
		this.fechaFin = fechaFin;
	}

	public Integer getTipoAudiencia() {
		return tipoAudiencia;
	}

	public void setTipoAudiencia(Integer tipoAudiencia) {
		this.tipoAudiencia = tipoAudiencia;
	}

	public DualListModel<Juez> getJuezPickModel() {

		if (juezPickModel == null) {
			this.juezPickModel = new DualListModel<Juez>(this.serviceLocator.getJuezService()
					.loadAll(), new LinkedList<Juez>());

		}

		return juezPickModel;
	}

	public void setJuezPickModel(DualListModel<Juez> juezPickModel) {
		this.juezPickModel = juezPickModel;
	}

	public Integer getIdSala() {
		return idSala;
	}

	public void setIdSala(Integer idSala) {
		this.idSala = idSala;
	}

	public ScheduleServiceLocator getServiceLocator() {
		return serviceLocator;
	}

	public void setServiceLocator(ScheduleServiceLocator serviceLocator) {
		this.serviceLocator = serviceLocator;
	}

	public String getKeywords() {
		return keywords;
	}

	public void setKeywords(String keywords) {
		this.keywords = keywords;
	}

	public Juez getJuezSugerido() {
		return juezSugerido;
	}

	public void setJuezSugerido(Juez juezSugerido) {
		this.juezSugerido = juezSugerido;
	}

	public List<String> getSugerencias() {
		return sugerencias;
	}

	public void setSugerencias(List<String> sugerencias) {
		this.sugerencias = sugerencias;
	}

	public CausaPenal getCausaPenalSeleccionada() {
		return causaPenalSeleccionada;
	}

	public void setCausaPenalSeleccionada(CausaPenal causaPenalSeleccionada) {
		this.causaPenalSeleccionada = causaPenalSeleccionada;
	}

	public CausaPenalSelectableDataModel getCausasEncontradasDataModel() {
		return causasEncontradasDataModel;
	}

	public void setCausasEncontradasDataModel(
			CausaPenalSelectableDataModel causasEncontradasDataModel) {
		this.causasEncontradasDataModel = causasEncontradasDataModel;
	}

}
