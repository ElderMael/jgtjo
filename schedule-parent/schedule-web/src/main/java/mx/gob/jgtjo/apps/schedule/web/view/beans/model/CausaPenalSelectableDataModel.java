package mx.gob.jgtjo.apps.schedule.web.view.beans.model;

import java.io.Serializable;
import java.util.List;

import javax.faces.model.ListDataModel;

import org.primefaces.model.SelectableDataModel;

import mx.gob.jgtjo.apps.schedule.model.CausaPenal;
import mx.gob.jgtjo.apps.schedule.web.utils.ScheduleUtils;

public class CausaPenalSelectableDataModel extends ListDataModel<CausaPenal> implements
		SelectableDataModel<CausaPenal>, Serializable {

	private static final long serialVersionUID = 1L;

	public CausaPenalSelectableDataModel() {

	}

	public CausaPenalSelectableDataModel(List<CausaPenal> data) {
		super(data);
	}

	@Override
	public Object getRowKey(CausaPenal object) {
		return ScheduleUtils.formatCausaNumberAndYear(object.getNumeroCausa(),
				object.getAnnioCausa());
	}

	@SuppressWarnings("unchecked")
	@Override
	public CausaPenal getRowData(String rowKey) {

		List<CausaPenal> data = (List<CausaPenal>) this.getWrappedData();

		for (CausaPenal causa : data) {

			String causaKey = ScheduleUtils.formatCausaNumberAndYear(causa.getNumeroCausa(),
					causa.getAnnioCausa());

			if (causaKey.equals(rowKey))
				return causa;

		}

		return null;
	}
}
