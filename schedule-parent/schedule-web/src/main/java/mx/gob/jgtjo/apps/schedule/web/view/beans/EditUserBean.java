package mx.gob.jgtjo.apps.schedule.web.view.beans;

import java.io.Serializable;
import java.util.Collection;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;

import mx.gob.jgtjo.apps.schedule.model.User;
import mx.gob.jgtjo.apps.schedule.web.services.ScheduleServiceLocator;
import mx.gob.jgtjo.apps.schedule.web.utils.JsfUtils;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetailsService;

@ManagedBean
@ViewScoped
public class EditUserBean implements Serializable {

     private static final long serialVersionUID = 1L;

     @ManagedProperty(value = "#{customUserDetailsService}")
     private UserDetailsService userDetailsService;

     @ManagedProperty(value = "#{serviceLocator}")
     private ScheduleServiceLocator serviceLocator;

     private User user;

     private String passwordCheck;

     public EditUserBean() {

     }

     @PostConstruct
     public void init() {

          String userName = JsfUtils.getCurrentHttpRequest().getParameter(
                    "user");

          this.user = (User) this.userDetailsService
                    .loadUserByUsername(userName);

     }

     public String saveChanges() {

          if ( !this.passwordCheck.equals(this.user.getPassword()) ) {

               JsfUtils.addNewFacesMessageFromBundle(
                         FacesMessage.SEVERITY_ERROR, null,
                         "mx.gob.jgtjo.apps.schedule.root.users.edit.labels.noPasswordCheck");

               return null;
          }
          this.serviceLocator.getUserService().update(this.user);

          return "pretty:users";
     }

     public Collection<GrantedAuthority> getAuthorities() {
          return user.getAuthorities();
     }

     public String getEmail() {
          return user.getEmail();
     }

     public String getNombres() {
          return user.getNombres();
     }

     public String getPassword() {
          return user.getPassword();
     }

     public String getUsername() {
          return user.getUsername();
     }

     public void setAuthorities(List<GrantedAuthority> authorities) {
          user.setAuthorities(authorities);
     }

     public void setEmail(String email) {
          user.setEmail(email);
     }

     public void setNombres(String nombres) {
          user.setNombres(nombres);
     }

     public void setPassword(String password) {
          user.setPassword(password);
     }

     public void setUsername(String username) {
          user.setUsername(username);
     }

     public ScheduleServiceLocator getServiceLocator() {
          return serviceLocator;
     }

     public void setServiceLocator(ScheduleServiceLocator serviceLocator) {
          this.serviceLocator = serviceLocator;
     }

     public UserDetailsService getUserDetailsService() {
          return userDetailsService;
     }

     public void setUserDetailsService(UserDetailsService userDetailsService) {
          this.userDetailsService = userDetailsService;
     }

     public String getPasswordCheck() {
          return passwordCheck;
     }

     public void setPasswordCheck(String passwordCheck) {
          this.passwordCheck = passwordCheck;
     }

}
