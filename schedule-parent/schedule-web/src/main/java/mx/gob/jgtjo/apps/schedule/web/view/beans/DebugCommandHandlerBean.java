package mx.gob.jgtjo.apps.schedule.web.view.beans;

import java.text.DateFormat;
import java.util.Date;
import java.util.List;

import javax.faces.bean.ApplicationScoped;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.primefaces.component.terminal.Terminal;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.core.context.SecurityContextHolder;

import mx.gob.jgtjo.apps.schedule.model.CausaPenal;
import mx.gob.jgtjo.apps.schedule.model.User;
import mx.gob.jgtjo.apps.schedule.web.search.HibernateSearchMassIndexerService;
import mx.gob.jgtjo.apps.schedule.web.services.ScheduleServiceLocator;
import mx.gob.jgtjo.apps.schedule.web.utils.JsfUtils;
import mx.gob.jgtjo.apps.schedule.web.utils.ScheduleUtils;

@ManagedBean(name = "terminalBean")
@ApplicationScoped
public class DebugCommandHandlerBean {

	protected static final Logger log = LoggerFactory.getLogger(DebugCommandHandlerBean.class);

	private Terminal terminal;

	@ManagedProperty(value = "#{serviceLocator}")
	private ScheduleServiceLocator serviceLocator;

	public DebugCommandHandlerBean() {
	}

	public String handleCommand(String command, String[] params) {

		if (command.equalsIgnoreCase("greet"))
			return "Hola " + params[0] + ". ¿Como estas?";

		if (command.equalsIgnoreCase("date"))
			return DateFormat.getInstance().format(new Date());

		if (command.equalsIgnoreCase("login-details"))
			return this.getUserDetails();

		if (command.equalsIgnoreCase("show-causas"))
			return this.getAllCausas();

		if (command.equalsIgnoreCase("show-users"))
			return this.getAllUsers();

		if (command.equalsIgnoreCase("index-database"))
			return this.createHibernateIndexes(params[0]);

		return command + " not found.";

	}

	private String createHibernateIndexes(String classToIndex) {

		log.debug("Opening session for indexing");
		Session session = JsfUtils.getBeanFromWebApplicationContext(SessionFactory.class)
				.openSession();

		HibernateSearchMassIndexerService indexingService = JsfUtils
				.getBeanFromWebApplicationContext(HibernateSearchMassIndexerService.class);

		String className = "mx.gob.jgtjo.apps.schedule.model." + classToIndex;

		try {
			indexingService.buildSearchIndex(session, Class.forName(className));
		} catch (ClassNotFoundException e) {
			return "Clase no encontrada '" + className + "'";
		} finally {
			log.debug("Closing session for indexing");
			session.close();

		}

		return "Hibernate search indexing started in new thread";

	}

	private String getAllUsers() {

		StringBuilder stringBuilder = new StringBuilder();

		for (User user : this.serviceLocator.getUserService().loadAll()) {

			stringBuilder.append(user.getUsername()).append(" - ").append(user.getNombres())
					.append("<br />");

		}

		return stringBuilder.toString();
	}

	private String getUserDetails() {
		return SecurityContextHolder.getContext().getAuthentication().toString()
				.replace(";", ";\n<br />");
	}

	private String getAllCausas() {

		List<CausaPenal> causas = this.serviceLocator.getCausaPenalService().loadAllCausas();

		StringBuilder stringBuilder = new StringBuilder();

		for (CausaPenal causaPenal : causas) {
			stringBuilder.append(
					ScheduleUtils.formatCausaNumberAndYear(causaPenal.getNumeroCausa(),
							causaPenal.getAnnioCausa())).append("<br />");
		}

		return stringBuilder.toString();
	}

	public void setTerminal(Terminal terminal) {
		this.terminal = terminal;
	}

	public Terminal getTerminal() {
		return this.terminal;
	}

	public ScheduleServiceLocator getServiceLocator() {
		return serviceLocator;
	}

	public void setServiceLocator(ScheduleServiceLocator serviceLocator) {
		this.serviceLocator = serviceLocator;
	}

}
