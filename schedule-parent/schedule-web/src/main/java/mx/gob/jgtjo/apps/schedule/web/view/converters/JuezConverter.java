package mx.gob.jgtjo.apps.schedule.web.view.converters;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import mx.gob.jgtjo.apps.schedule.model.Juez;
import mx.gob.jgtjo.apps.schedule.service.JuezService;
import mx.gob.jgtjo.apps.schedule.web.utils.JsfUtils;

public class JuezConverter implements Converter {

	public static final Logger log = LoggerFactory.getLogger(JuezConverter.class);

	private JuezService juezService;

	public JuezConverter() {
		this.juezService = JsfUtils.getBeanFromWebApplicationContext(JuezService.class);
	}

	@Override
	public Object getAsObject(FacesContext context, UIComponent component, String value) {

		log.debug("Converting {} to Object", value);

		Juez returnVal = this.getJuezService().findByNames(value);

		log.debug("Returned {} after search", returnVal);
		return returnVal;
	}

	@Override
	public String getAsString(FacesContext context, UIComponent component, Object value) {

		log.debug("Converting {} to String", value);

		if (value instanceof String) {
			log.debug("String found {} after search", value);
			return (String) value;
		}

		String returnVal = ((Juez) value).getNombres();

		log.debug("Returned {} after search", returnVal);

		return returnVal;

	}

	public JuezService getJuezService() {
		return juezService;
	}

	public void setJuezService(JuezService juezService) {
		this.juezService = juezService;
	}

}
