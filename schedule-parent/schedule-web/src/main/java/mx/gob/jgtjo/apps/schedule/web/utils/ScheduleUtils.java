package mx.gob.jgtjo.apps.schedule.web.utils;

import java.text.MessageFormat;

import org.apache.commons.lang.StringUtils;

import mx.gob.jgtjo.apps.schedule.model.CausaPenal;

public class ScheduleUtils {

	private static final String CAUSA_NUMBER_AND_YEAR_FORMAT_PATTERN = "{0}/{1}";

	public static String formatCausaNumberAndYear(Integer numeroCausa, Integer annioCausa) {

		String causaNumberAndYear = MessageFormat.format(CAUSA_NUMBER_AND_YEAR_FORMAT_PATTERN,
				numeroCausa.toString(), annioCausa.toString());

		causaNumberAndYear = StringUtils.leftPad(causaNumberAndYear, 9, "0");

		return causaNumberAndYear;
	}

	public static String formatCausaNumberAndYear(CausaPenal causa) {
		return ScheduleUtils
				.formatCausaNumberAndYear(causa.getNumeroCausa(), causa.getAnnioCausa());
	}

}
