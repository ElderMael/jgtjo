package mx.gob.jgtjo.apps.schedule.web.view.beans;

import java.io.IOException;
import java.io.Serializable;
import java.util.TimeZone;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;

import mx.gob.jgtjo.apps.schedule.web.utils.Constantes;
import mx.gob.jgtjo.apps.schedule.web.utils.JsfUtils;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;

/**
 * 
 * Backing bean utilizado para forwarding hacia la url de autenticacion de
 * spring security y mantener los datos del usuario accesibles.
 * 
 * @author ElderMael
 * 
 */

@ManagedBean(name = "loginBean", eager = true)
@SessionScoped
public class LoginBean implements Serializable {

     private static final long serialVersionUID = 1L;

     private static final Logger log = LoggerFactory.getLogger(LoginBean.class);

     private String userName = "";
     private String userPassword = "";
     private TimeZone userTimeZone = TimeZone.getTimeZone("GMT-6:00"); // TODO -
                                                                       // Preferences?

     public LoginBean() {

     }

     public String doLogin() throws ServletException, IOException {

          String springCheckUrl = this.buildSpringSecurityCheckUrl();

          log.debug("Forwarding to spring security url '{}'.", springCheckUrl);

          RequestDispatcher dispatcher = JsfUtils.getCurrentHttpRequest()
                    .getRequestDispatcher(springCheckUrl);

          dispatcher.forward((ServletRequest) JsfUtils.getCurrentHttpRequest(),
                    (ServletResponse) JsfUtils.getCurrentExternalContext()
                              .getResponse());

          JsfUtils.getCurrentFacesContext().responseComplete();

          return null;
     }

     public UserDetails getUserDetails() {

          return (UserDetails) SecurityContextHolder.getContext()
                    .getAuthentication().getPrincipal();

     }

     public Boolean getUserLogedIn() {
          return JsfUtils.getCurrentExternalContext().isUserInRole(
                    Constantes.REGISTERED_USER_ROLE_STRING);
     }

     public String getUserName() {
          return this.userName;
     }

     public Boolean getUserOficialDePartes() {
          return JsfUtils.getCurrentHttpRequest().isUserInRole(
                    Constantes.OFICIAL_DE_PARTES_ROLE_STRING);
     }

     public String getUserPassword() {
          return this.userPassword;
     }

     public Boolean getUserRoot() {
          return JsfUtils.getCurrentHttpRequest().isUserInRole(
                    Constantes.ROOT_ROLE_STRING);
     }

     public Boolean getUserAudioAndVideo() {
          return JsfUtils.getCurrentHttpRequest().isUserInRole(
                    Constantes.AUDIO_AND_VIDEO_ROLE_STRING);
     }

     public void setUserName(String userName) {
          this.userName = userName;
     }

     public void setUserPassword(String userPassword) {
          this.userPassword = userPassword;
     }

     private String buildSpringSecurityCheckUrl() {
          StringBuilder springCheckUrl = new StringBuilder(
                    Constantes.SPRING_SECURITY_LOGIN_URL).append("?")
                    .append(Constantes.SPRING_SECURITY_USERNAME_PARAMETER)
                    .append("=").append(this.userName.trim()).append("&")
                    .append(Constantes.SPRING_SECURITY_PASSWORD_PARAMETER)
                    .append("=").append(this.userPassword.trim());
          return springCheckUrl.toString();
     }

     public TimeZone getUserTimeZone() {
          return userTimeZone;
     }

     public void setUserTimeZone(TimeZone userTimeZone) {
          this.userTimeZone = userTimeZone;
     }

}
