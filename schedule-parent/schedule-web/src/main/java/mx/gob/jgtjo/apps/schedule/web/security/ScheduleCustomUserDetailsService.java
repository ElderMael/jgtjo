package mx.gob.jgtjo.apps.schedule.web.security;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import mx.gob.jgtjo.apps.schedule.dao.UserDao;
import mx.gob.jgtjo.apps.schedule.model.User;

/**
 * 
 * @author ElderMael
 * 
 */
@Service("customUserDetailsService")
public class ScheduleCustomUserDetailsService implements UserDetailsService {

	public static String LOAD_USER_BY_USERNAME_HQL_QUERY = "FROM User WHERE Username = :username";
	public static Logger log = LoggerFactory.getLogger(ScheduleCustomUserDetailsService.class);
	public static String USERNAME_PARAMETER = "username";

	@Autowired
	private UserDao userDao;

	public UserDao getUserDao() {
		return this.userDao;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	@Transactional(propagation = Propagation.REQUIRED)
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException,
			DataAccessException {

		log.debug("Loading user by username '{}'", username);

		List<User> users;

		users = this.userDao.createQuery(LOAD_USER_BY_USERNAME_HQL_QUERY, USERNAME_PARAMETER,
				username);

		if (users.isEmpty()) {
			throw new UsernameNotFoundException("User name '" + username
					+ "' not found in database.");
		}

		return users.get(0);
	}

	public void setUserDao(UserDao userDao) {
		this.userDao = userDao;
	}

}
