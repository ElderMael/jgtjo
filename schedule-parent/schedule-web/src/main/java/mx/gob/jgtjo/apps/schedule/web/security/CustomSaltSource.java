package mx.gob.jgtjo.apps.schedule.web.security;

import org.springframework.security.authentication.dao.SaltSource;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Component;

/**
 * 
 * @author ElderMael
 * 
 */
@Component("scheduleSaltSource")
public class CustomSaltSource implements SaltSource {

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Object getSalt(UserDetails user) {
		return "";
	}

}
