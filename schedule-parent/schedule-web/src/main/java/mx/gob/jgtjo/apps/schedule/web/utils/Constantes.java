package mx.gob.jgtjo.apps.schedule.web.utils;

/**
 * Interface donde se guardan las constantes utilizadas dentro del modulo web.
 * 
 * @author ElderMael
 * 
 */
public interface Constantes {

     // Conversation
     String ACTIVE_CONVERSATION_ATTRIBUTE_NAME = "mx.gob.jtjo.conversation.active";
     String ACTIVE_CONVERSATION_COOKIE_NAME = "mx.gob.jgtjo.mana.cookie";

     // Attribute
     String NEW_CAUSA_PENAL_ID_ATTRIBUTE_NAME = "mx.gob.jgtjo.schedule.web.oficialia.newCausaId";
     String NEW_CAUSA_PENAL_YEAR_ATTRIBUTE_NAME = "mx.gob.jgtjo.schedule.web.oficialia.newCausaYear";

     // Roles
     String REGISTERED_USER_ROLE_STRING = "ROLE_USER";
     String ROOT_ROLE_STRING = "ROLE_ROOT";
     String OFICIAL_DE_PARTES_ROLE_STRING = "ROLE_OFICIAL_DE_PARTES";
     String JUEZ_ROLE_STRING = "ROLE_JUEZ";
     String AUDIO_AND_VIDEO_ROLE_STRING = "ROLE_AUDIO_Y_VIDEO";

     // Spring security
     String SPRING_SECURITY_LOGIN_URL = "/j_spring_security_check";
     String SPRING_SECURITY_PASSWORD_PARAMETER = "j_password";
     String SPRING_SECURITY_USERNAME_PARAMETER = "j_username";

     // Faces Messages
     String PASSWORD_VERIFICATION_MISMATCH = "mx.gob.jgtjo.apps.schedule.root.users.labels.passwordMismatch";
     String LOGIN_FAIL_KEY = "mx.gob.jgtjo.apps.schedule.login.fail";
     String DEFAULT_CUSTOM_FACES_MESSAGE_BUNDLE_NAME = "mx.gob.jgtjo.apps.schedule.web.appmsgs";
     String ADDCAUSA_NEW_VICTIMA_KEY = "mx.gob.jgtjo.apps.schedule.facesMessages.addCausa.addedVictima";
     String ADDCAUSA_NEW_IMPUTADO_KEY = "mx.gob.jgtjo.apps.schedule.facesMessages.addCausa.addedImputado";
     String USERS_ADD_USER_KEY = "mx.gob.jgtjo.apps.schedule.facesMessages.users.addUser";
     String DELITOS_ADDED_DELITO_KEY = "mx.gob.jgtjo.apps.schedule.facesMessages.delitos.added";
     String DELITOS_REMOVED_DELITO_KEY = "mx.gob.jgtjo.apps.schedule.facesMessages.delitos.removed";
     String SALAS_ADD_SALA_KEY = "mx.gob.jgtjo.apps.schedule.facesMessages.salas.added";
     String TIPO_AUDIENCIA_ADDED_KEY = "mx.gob.jgtjo.apps.schedule.facesMessages.tipoAudiencia.add";
     String MISSING_CAUSA_SELECTED = "mx.gob.jgtjo.apps.schedule.facesMessages.missingSelection";
     String CANCELED_AUDIENCIA_KEY = "mx.gob.jgtjo.apps.schedule.facesMessages.cancel.audiencia";
     String ERROR_CONVERTING_MP_KEY = "mx.gob.jgtjo.apps.schedule.facesMessages.converters.mp.error";

     // Validation
     String CAUSA_MINIMAL_YEAR_VALIDATION_MESSAGE_KEY = "{mx.gob.jgtjo.apps.schedule.validation.minimalYearCause}";

     // Reports
     String HOJA_DE_DATOS_REPORT_PATH = "mx/gob/jgtjo/apps/schedule/web/reports/EventosAudiencia.xml";
     String DATOS_JUEZ_REPORT_PATH = "mx/gob/jgtjo/apps/schedule/web/reports/DatosJuez.xml";
     String DATOS_JUEZ_IMPUGNACION_REPORT_PATH = "mx/gob/jgtjo/apps/schedule/web/reports/DatosJuezImpugnacion.xml";
     String PROTOCOLO_AUDIENCIA_REPORT_PATH = "mx/gob/jgtjo/apps/schedule/web/reports/ProtocoloAudiencia.xml";
     String PROTOCOLO_AUDIENCIA_IMPUGNACION_REPORT_PATH = "mx/gob/jgtjo/apps/schedule/web/reports/ProtocoloAudienciaImpugnacion.xml";
     String HOJA_DE_DATOS_IMPUGNACION_REPORT_PATH = "mx/gob/jgtjo/apps/schedule/web/reports/EventosAudienciaImpugnacion.xml";
     String EVENTOS_AUDIENCIA_REPORT_STRIPES_IMAGE_PARAMETER_NAME = "StripesImage";
     String REPORTS_RESOURCE_BUNDLE = "mx.gob.jgtjo.apps.schedule.web.reports.Reportes_Bundle";
     String EVENTOS_AUDIENCIA_REPORT_FECHA_AUDIENCIA_PARAMETER_NAME = "FechaAudiencia";
     String EVENTOS_AUDIENCIA_REPORT_CAUSA_NUMBER_AND_YEAR_PARAMETER_NAME = "CausaNumberAndYear";
     String EVENTOS_AUDIENCIA_REPORT_JUEZ_PARAMETER_NAME = "Juez";
     String EVENTOS_AUDIENCIA_REPORT_SALA_PARAMETER_NAME = "sala";
     String EVENTOS_AUDIENCIA_REPORT_NUMERO_AUDIENCIA_PARAMETER_NAME = "numeroAudiencia";
     String EVENTOS_AUDIENCIA_REPORT_FECHA_INICIO_PARAMETER_NAME = "fechaInicio";
     String EVENTOS_AUDIENCIA_REPORT_TIPO_AUDIENCIA_NAME_PARAMETER_NAME = "tipoAudienciaName";
     String EVENTOS_AUDIENCIA_REPORT_DELITOS_BY_NAME_PARAMETER_NAME = "DelitosByName";
     String EVENTOS_AUDIENCIA_REPORT_IMPUTADOS_PARAMETER_NAME = "Imputados";
     String EVENTOS_AUDIENCIA_REPORT_VICTIMAS_BY_NAME_PARAMETER_NAME = "VictimasByName";
     String DATOS_JUEZ_TIPO_AUDIENCIA_PARAMETER_NAME = "TipoAudiencia";
     String DATOS_JUEZ_CAUSA_NUMBER_AND_YEAR_PARAMETER_NAME = "Causa";
     String DATOS_JUEZ_IMPUTADOS_PARAMETER_NAME = "Imputados";
     String DATOS_JUEZ_DELITOS_PARAMETER_NAME = "Delitos";
     String DATOS_JUEZ_VICTIMAS_PARAMETER_NAME = "Victimas";

}
