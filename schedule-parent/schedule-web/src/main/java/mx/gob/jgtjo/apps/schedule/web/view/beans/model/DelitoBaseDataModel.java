package mx.gob.jgtjo.apps.schedule.web.view.beans.model;

import java.io.Serializable;
import java.util.List;

import javax.faces.model.ListDataModel;

import org.primefaces.model.SelectableDataModel;

import mx.gob.jgtjo.apps.schedule.model.Delito;

public class DelitoBaseDataModel extends ListDataModel<Delito> implements
		SelectableDataModel<Delito>, Serializable {

	private static final long serialVersionUID = 1L;

	public DelitoBaseDataModel() {

	}

	public DelitoBaseDataModel(List<Delito> data) {
		super(data);
	}

	@Override
	public Object getRowKey(Delito object) {
		return object.getNombre();
	}

	@Override
	public Delito getRowData(String rowKey) {

		@SuppressWarnings("unchecked")
		List<Delito> delitos = (List<Delito>) this.getWrappedData();

		for (Delito delito : delitos) {
			if (delito.getNombre().equals(rowKey)) {
				return delito;
			}
		}

		return null;
	}

}
