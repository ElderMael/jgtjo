package mx.gob.jgtjo.apps.schedule.web.view.beans.model;

import java.util.Date;

import org.primefaces.model.LazyScheduleModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import mx.gob.jgtjo.apps.schedule.model.Sala;
import mx.gob.jgtjo.apps.schedule.web.services.ScheduleServiceLocator;

@Component("customLazyModelBean")
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public class CustomLazyScheduleModel extends LazyScheduleModel {

	private static final long serialVersionUID = 1L;

	@Autowired
	private ScheduleServiceLocator serviceLocator;

	private Sala sala;

	public CustomLazyScheduleModel() {

	}

	@Override
	public void loadEvents(Date start, Date end) {

	}

	public ScheduleServiceLocator getServiceLocator() {
		return serviceLocator;
	}

	public void setServiceLocator(ScheduleServiceLocator serviceLocator) {
		this.serviceLocator = serviceLocator;
	}

	public Sala getSala() {
		return sala;
	}

	public void setSala(Sala sala) {
		this.sala = sala;
	}

}
