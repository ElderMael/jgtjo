package mx.gob.jgtjo.apps.schedule.web.view.beans.model;

import java.util.List;

import javax.faces.model.ListDataModel;

import org.primefaces.model.SelectableDataModel;

import mx.gob.jgtjo.apps.schedule.model.DelitoConfigurado;

public class DelitoDataModel extends ListDataModel<DelitoConfigurado> implements
		SelectableDataModel<DelitoConfigurado> {

	public DelitoDataModel() {

	}

	public DelitoDataModel(List<DelitoConfigurado> data) {
		super(data);
	}

	@SuppressWarnings("unchecked")
	@Override
	public DelitoConfigurado getRowData(String rowKey) {

		List<DelitoConfigurado> delitos = (List<DelitoConfigurado>) this.getWrappedData();

		for (DelitoConfigurado delito : delitos) {
			if (delito.getDelito().getNombre().equalsIgnoreCase(rowKey)) {
				return delito;
			}
		}

		return null;
	}

	@Override
	public Object getRowKey(DelitoConfigurado object) {
		return object.getDelito().getNombre();
	}

}
