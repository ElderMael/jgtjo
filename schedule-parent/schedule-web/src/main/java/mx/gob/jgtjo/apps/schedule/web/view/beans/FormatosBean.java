package mx.gob.jgtjo.apps.schedule.web.view.beans;

import java.io.Serializable;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;

import org.hibernate.SessionFactory;
import org.primefaces.model.StreamedContent;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import mx.gob.jgtjo.apps.schedule.model.AudienciaOral;
import mx.gob.jgtjo.apps.schedule.model.CausaPenal;
import mx.gob.jgtjo.apps.schedule.model.ParteMaterial;
import mx.gob.jgtjo.apps.schedule.web.reports.AudienciaOralReportFactory;
import mx.gob.jgtjo.apps.schedule.web.reports.ReportType;
import mx.gob.jgtjo.apps.schedule.web.services.ScheduleServiceLocator;
import mx.gob.jgtjo.apps.schedule.web.utils.JsfUtils;
import mx.gob.jgtjo.apps.schedule.web.view.beans.model.AudienciaOralSelectableDataModel;

@ManagedBean(name = "formatosBean")
@ViewScoped
public class FormatosBean implements Serializable {

	private static final long serialVersionUID = 1L;

	protected static final Logger log = LoggerFactory.getLogger(FormatosBean.class);

	@ManagedProperty(value = "#{serviceLocator}")
	private ScheduleServiceLocator serviceLocator;

	@ManagedProperty(value = "#{reportFactory}")
	private AudienciaOralReportFactory reportFactory;

	@ManagedProperty(value = "#{loginBean.userTimeZone}")
	private TimeZone userTimeZone;

	@ManagedProperty(value = "#{sessionFactory}")
	private SessionFactory sessionFactory;

	private String keywords;

	private AudienciaOral selectedAudienciaOral;

	private AudienciaOralSelectableDataModel dataModel = new AudienciaOralSelectableDataModel();

	private List<ParteMaterial> exclusiones = new LinkedList<ParteMaterial>();

	public FormatosBean() {

	}

	public void searchByKeywords() {

		this.selectedAudienciaOral = null;
		this.exclusiones = new LinkedList<ParteMaterial>();

		if (this.keywords == null || this.keywords.isEmpty())
			return;

		List<AudienciaOral> results = this.getServiceLocator().getSearchService()
				.searchAudienciasOrales(keywords);

		this.dataModel.setWrappedData(results);

	}

	public List<ParteMaterial> getImputadosFromSelectedAudienciaOral() {

		if (this.selectedAudienciaOral == null)
			return Collections.emptyList();

		return this.selectedAudienciaOral.getCausaPenal().getImputados();

	}

	public List<ParteMaterial> getVictimasFromSelectedAudienciaOral() {

		if (this.selectedAudienciaOral == null)
			return Collections.emptyList();

		return this.selectedAudienciaOral.getCausaPenal().getVictimas();

	}

	public StreamedContent getAudienciaProtocoloReport() {

		Locale locale = JsfUtils.getViewLocale();

		StreamedContent report = this.reportFactory.generateReport(
				ReportType.PROTOCOLO_DE_AUDIENCIA, this.selectedAudienciaOral, locale,
				userTimeZone, this.exclusiones);

		return report;

	}

	public StreamedContent getControlEventosReport() {

		Locale locale = JsfUtils.getViewLocale();

		StreamedContent report = reportFactory.generateReport(ReportType.CONTROL_EVENTOS_AUDIENCIA,
				this.selectedAudienciaOral, locale, userTimeZone, this.exclusiones);

		return report;

	}

	public StreamedContent getDatosJuezReport() {

		Locale locale = JsfUtils.getViewLocale();

		StreamedContent report = this.reportFactory.generateReport(ReportType.DATOS_PARA_JUEZ,
				this.selectedAudienciaOral, locale, userTimeZone, this.exclusiones);

		return report;
	}

	public String getKeywords() {
		return keywords;
	}

	public void setKeywords(String keywords) {
		this.keywords = keywords;
	}

	public ScheduleServiceLocator getServiceLocator() {
		return serviceLocator;
	}

	public void setServiceLocator(ScheduleServiceLocator serviceLocator) {
		this.serviceLocator = serviceLocator;
	}

	public AudienciaOralSelectableDataModel getDataModel() {
		return dataModel;
	}

	public void setDataModel(AudienciaOralSelectableDataModel dataModel) {
		this.dataModel = dataModel;
	}

	public AudienciaOral getSelectedAudienciaOral() {

		if (this.selectedAudienciaOral == null) {
			AudienciaOral temporal = new AudienciaOral();

			temporal.setCausaPenal(new CausaPenal());

			List<ParteMaterial> emptyList = Collections.emptyList();

			temporal.getCausaPenal().setImputados(emptyList);
			temporal.getCausaPenal().setVictimas(emptyList);

			return temporal;
		}
		return selectedAudienciaOral;
	}

	public void setSelectedAudienciaOral(AudienciaOral selectedAudienciaOral) {
		this.selectedAudienciaOral = selectedAudienciaOral;
	}

	public AudienciaOralReportFactory getReportFactory() {
		return reportFactory;
	}

	public void setReportFactory(AudienciaOralReportFactory reportFactory) {
		this.reportFactory = reportFactory;
	}

	public TimeZone getUserTimeZone() {
		return userTimeZone;
	}

	public void setUserTimeZone(TimeZone userTimeZone) {
		this.userTimeZone = userTimeZone;
	}

	public SessionFactory getSessionFactory() {
		return sessionFactory;
	}

	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}

	public List<ParteMaterial> getExclusiones() {
		return exclusiones;
	}

	public void setExclusiones(List<ParteMaterial> exclusiones) {
		this.exclusiones = exclusiones;
	}

}
