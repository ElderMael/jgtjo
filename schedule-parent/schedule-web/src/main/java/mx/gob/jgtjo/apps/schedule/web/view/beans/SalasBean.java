package mx.gob.jgtjo.apps.schedule.web.view.beans;

import java.io.Serializable;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import mx.gob.jgtjo.apps.schedule.model.Sala;
import mx.gob.jgtjo.apps.schedule.web.services.ScheduleServiceLocator;
import mx.gob.jgtjo.apps.schedule.web.utils.Constantes;
import mx.gob.jgtjo.apps.schedule.web.utils.JsfUtils;
import mx.gob.jgtjo.apps.schedule.web.view.beans.model.SalasDataModel;

@ManagedBean(name = "salasBean")
@ViewScoped
public class SalasBean implements Serializable {

	private static final long serialVersionUID = 1L;

	protected static final Logger log = LoggerFactory.getLogger(SalasBean.class);

	@ManagedProperty(value = "#{serviceLocator}")
	private ScheduleServiceLocator serviceLocator;

	private SalasDataModel dataModel;

	private Sala sala;

	public SalasBean() {

	}

	@PostConstruct
	public void init() {
		List<Sala> data = this.serviceLocator.getSalaService().loadAll();
		this.dataModel = new SalasDataModel(data);
		this.sala = new Sala();

	}

	@SuppressWarnings("unchecked")
	public void addSala() {
		log.debug("Agregando sala {}", this.sala);

		this.serviceLocator.getSalaService().save(sala);

		((List<Sala>) this.dataModel.getWrappedData()).add(sala);

		JsfUtils.addNewFacesMessageFromBundle(FacesMessage.SEVERITY_INFO, null,
				Constantes.SALAS_ADD_SALA_KEY);

		this.sala = new Sala();

	}

	public ScheduleServiceLocator getServiceLocator() {
		return serviceLocator;
	}

	public void setServiceLocator(ScheduleServiceLocator serviceLocator) {
		this.serviceLocator = serviceLocator;
	}

	public SalasDataModel getDataModel() {
		return dataModel;
	}

	public void setDataModel(SalasDataModel dataModel) {
		this.dataModel = dataModel;
	}

	public String getLocacion() {
		return sala.getLocacion();
	}

	public String getNombre() {
		return sala.getNombre();
	}

	public void setLocacion(String locacion) {
		sala.setLocacion(locacion);
	}

	public void setNombre(String nombre) {
		sala.setNombre(nombre);
	}

}
