package mx.gob.jgtjo.apps.schedule.web.conversation;

import java.io.IOException;
import java.util.UUID;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.filter.OncePerRequestFilter;

import mx.gob.jgtjo.apps.schedule.web.utils.Constantes;

/**
 * 
 * Filter that looks up on request cookies for a conversation id.
 * 
 * If no id is found then the filter creates a new one and stores the id in a
 * new cookie.
 * 
 * @author ElderMael
 * 
 */
public class OpenSessionInViewInsideConversationFilter extends
          OncePerRequestFilter {

     protected static final Logger log = LoggerFactory
               .getLogger(OpenSessionInViewInsideConversationFilter.class);

     public OpenSessionInViewInsideConversationFilter() {

     }

     @Override
     protected void doFilterInternal(HttpServletRequest request,
               HttpServletResponse response, FilterChain filterChain)
               throws ServletException, IOException {

          UUID conversationId = lookupConversationOrCreateIfNecessary(request,
                    response);

          if ( !ConversationManager.isConversationOpen(conversationId) ) {
               log.debug("Conversation for this request already expired, restarting.");
               ConversationManager.restartConversation(conversationId);
          }

          ConversationManager
                    .bindConversationToRequest(conversationId, request);
          log.debug("Binding conversation '{}' to Request", conversationId);

          try {

               filterChain.doFilter(request, response);

          } finally {

               log.debug("Unbinding conversation from request '{}'", request);
               ConversationManager.unbindConversationToRequest(request);

          }

     }

     private UUID lookupConversationOrCreateIfNecessary(
               HttpServletRequest request, HttpServletResponse response) {
          UUID conversationId = null;

          conversationId = lookupConversationIdOnCookies(request);

          if ( conversationId == null ) {
               conversationId = startNewConversationAndStoreCookie(request,
                         response);
          }
          return conversationId;
     }

     private UUID startNewConversationAndStoreCookie(
               HttpServletRequest request, HttpServletResponse response) {
          UUID conversationId;
          log.debug("No conversation cookie found in request {}, creating new conversation.",
                    request);

          conversationId = ConversationManager.createNewConversation();

          Cookie cookie = new Cookie(
                    Constantes.ACTIVE_CONVERSATION_COOKIE_NAME,
                    conversationId.toString());

          cookie.setMaxAge(-1); // It will expire after browser shut-down

          response.addCookie(cookie);
          return conversationId;
     }

     private UUID lookupConversationIdOnCookies(HttpServletRequest request) {

          if ( request.getCookies() == null || request.getCookies().length == 0 )
               return null;

          UUID conversationId = null;

          for (Cookie cookie : request.getCookies()) {
               if ( cookie.getName().equals(
                         Constantes.ACTIVE_CONVERSATION_COOKIE_NAME) ) {
                    log.debug("Conversation cookie found in request {}.",
                              request);
                    conversationId = UUID.fromString(cookie.getValue());
                    break;
               }
          }

          return conversationId;
     }
}
