package mx.gob.jgtjo.apps.schedule.web.view.beans;

import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.model.SelectItem;

import org.primefaces.event.ScheduleEntrySelectEvent;
import org.primefaces.model.DefaultScheduleEvent;
import org.primefaces.model.LazyScheduleModel;
import org.primefaces.model.StreamedContent;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import mx.gob.jgtjo.apps.schedule.model.AudienciaOral;
import mx.gob.jgtjo.apps.schedule.model.ParteMaterial;
import mx.gob.jgtjo.apps.schedule.model.Sala;
import mx.gob.jgtjo.apps.schedule.web.reports.AudienciaOralReportFactory;
import mx.gob.jgtjo.apps.schedule.web.reports.ReportType;
import mx.gob.jgtjo.apps.schedule.web.services.ScheduleServiceLocator;
import mx.gob.jgtjo.apps.schedule.web.utils.JsfUtils;
import mx.gob.jgtjo.apps.schedule.web.utils.ScheduleUtils;

@ManagedBean(name = "scheduleBean", eager = true)
@ViewScoped
public class ScheduleBackingBean extends LazyScheduleModel {

	public static final long serialVersionUID = 1L;

	public static final Logger log = LoggerFactory.getLogger(ScheduleBackingBean.class);

	@ManagedProperty(value = "#{serviceLocator}")
	private ScheduleServiceLocator serviceLocator;

	@ManagedProperty(value = "#{loginBean.userTimeZone}")
	private TimeZone userTimeZone;

	@ManagedProperty(value = "#{reportFactory}")
	private AudienciaOralReportFactory reportFactory;

	private Integer sala = -1;

	private AudienciaOral audienciaOralSeleccionada;

	public ScheduleBackingBean() {

	}

	@Override
	public void loadEvents(Date start, Date end) {

		List<AudienciaOral> audiencias = null;

		if (this.sala == null || this.sala == -1) {
			audiencias = this.serviceLocator.getAudienciaOralService().loadAudienciasInTimeFrame(
					start, end);
		} else {
			audiencias = this.serviceLocator.getAudienciaOralService().loadAudienciasInTimeFrame(
					start, end, this.sala);
		}

		for (AudienciaOral audienciaOral : audiencias) {

			DefaultScheduleEvent event = buildEvent(audienciaOral);

			this.addEvent(event);
		}
	}

	private DefaultScheduleEvent buildEvent(AudienciaOral audienciaOral) {
		DefaultScheduleEvent event = new DefaultScheduleEvent();

		String tituloAudiencia = buildAudienciaTitle(audienciaOral);

		event.setAllDay(false);
		event.setData(audienciaOral);
		event.setEditable(true);
		event.setEndDate(audienciaOral.getFechaFinAprox());
		event.setStartDate(audienciaOral.getFechaInicio());
		event.setTitle(tituloAudiencia);
		return event;
	}

	private String buildAudienciaTitle(AudienciaOral audienciaOral) {
		StringBuilder tituloAudiencia = new StringBuilder();
		tituloAudiencia
				.append(audienciaOral.getTipoAudiencia().getNombre())
				.append(" (")
				.append(ScheduleUtils.formatCausaNumberAndYear(audienciaOral.getCausaPenal()
						.getNumeroCausa(), audienciaOral.getCausaPenal().getAnnioCausa()))
				.append(") ").append(audienciaOral.getSeCancelo() ? " - CANCELADA" : "");
		return tituloAudiencia.toString();
	}

	public List<SelectItem> getAllSalas() {
		List<SelectItem> items = new LinkedList<SelectItem>();

		List<Sala> salas = this.serviceLocator.getSalaService().loadAll();

		for (Sala sala : salas) {
			items.add(new SelectItem(sala.getId(), sala.getNombre()));
		}

		return items;
	}

	public void scheduleEventSelectListener(ScheduleEntrySelectEvent event) {

		log.debug("Seleccionado un evento: {}", event);

		this.audienciaOralSeleccionada = (AudienciaOral) event.getScheduleEvent().getData();

	}

	public String getCausaNumberAndYear() {

		if (this.audienciaOralSeleccionada == null)
			return "";

		return ScheduleUtils.formatCausaNumberAndYear(this.audienciaOralSeleccionada
				.getCausaPenal().getNumeroCausa(), this.audienciaOralSeleccionada.getCausaPenal()
				.getAnnioCausa());

	}

	public StreamedContent getAudienciaProtocoloReport() {

		Locale locale = JsfUtils.getViewLocale();

		return this.reportFactory.generateReport(ReportType.PROTOCOLO_DE_AUDIENCIA,
				audienciaOralSeleccionada, locale, userTimeZone, new LinkedList<ParteMaterial>());
	}

	public StreamedContent getControlEventosReport() {

		Locale locale = JsfUtils.getViewLocale();

		return reportFactory.generateReport(ReportType.CONTROL_EVENTOS_AUDIENCIA,
				audienciaOralSeleccionada, locale, userTimeZone, new LinkedList<ParteMaterial>());

	}

	public StreamedContent getDatosJuezReport() {

		Locale locale = JsfUtils.getViewLocale();

		return this.reportFactory.generateReport(ReportType.DATOS_PARA_JUEZ,
				audienciaOralSeleccionada, locale, userTimeZone, new LinkedList<ParteMaterial>());
	}

	public Integer getSala() {
		return this.sala;
	}

	public void setSala(Integer sala) {
		this.sala = sala;
	}

	public ScheduleServiceLocator getServiceLocator() {
		return serviceLocator;
	}

	public void setServiceLocator(ScheduleServiceLocator serviceLocator) {
		this.serviceLocator = serviceLocator;
	}

	public AudienciaOral getAudienciaOralSeleccionada() {
		return audienciaOralSeleccionada;
	}

	public void setAudienciaOralSeleccionada(AudienciaOral audienciaOralSeleccionada) {
		this.audienciaOralSeleccionada = audienciaOralSeleccionada;
	}

	public TimeZone getUserTimeZone() {
		return userTimeZone;
	}

	public void setUserTimeZone(TimeZone userTimeZone) {
		this.userTimeZone = userTimeZone;
	}

	public AudienciaOralReportFactory getReportFactory() {
		return reportFactory;
	}

	public void setReportFactory(AudienciaOralReportFactory reportFactory) {
		this.reportFactory = reportFactory;
	}

}
