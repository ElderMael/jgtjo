package mx.gob.jgtjo.apps.schedule.web.view.beans;

import java.io.Serializable;
import java.util.LinkedList;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.event.ActionEvent;

import mx.gob.jgtjo.apps.schedule.model.AudienciaOral;
import mx.gob.jgtjo.apps.schedule.service.SearchService;
import mx.gob.jgtjo.apps.schedule.web.services.ScheduleServiceLocator;
import mx.gob.jgtjo.apps.schedule.web.utils.Constantes;
import mx.gob.jgtjo.apps.schedule.web.utils.JsfUtils;
import mx.gob.jgtjo.apps.schedule.web.view.beans.model.AudienciaOralSelectableDataModel;

@ManagedBean(name = "cancelAudienciaBean")
@ViewScoped
public class CancelAudienciaBean implements Serializable {

     private static final long serialVersionUID = 1L;

     @ManagedProperty(value = "#{serviceLocator}")
     private ScheduleServiceLocator serviceLocator;

     @ManagedProperty(value = "#{searchService}")
     private SearchService searchService;

     private String keywords;

     private AudienciaOral audienciaOralSeleccionada;

     private AudienciaOralSelectableDataModel dataModel = new AudienciaOralSelectableDataModel(
               new LinkedList<AudienciaOral>());

     public CancelAudienciaBean() {

     }

     public void searchByKeywords() {
          this.dataModel.setWrappedData(this.searchService
                    .searchAudienciasOrales(this.keywords));
     }

     public void cancelAudiencia() {

          if ( this.audienciaOralSeleccionada == null )
               return;

          this.audienciaOralSeleccionada.setSeCancelo(true);

          this.serviceLocator.getAudienciaOralService().update(
                    audienciaOralSeleccionada);

          JsfUtils.addNewFacesMessageFromBundle(FacesMessage.SEVERITY_INFO,
                    null, Constantes.CANCELED_AUDIENCIA_KEY);

     }

     public String getKeywords() {
          return keywords;
     }

     public void setKeywords(String keywords) {
          this.keywords = keywords;
     }

     public ScheduleServiceLocator getServiceLocator() {
          return serviceLocator;
     }

     public void setServiceLocator(ScheduleServiceLocator serviceLocator) {
          this.serviceLocator = serviceLocator;
     }

     public SearchService getSearchService() {
          return searchService;
     }

     public void setSearchService(SearchService searchService) {
          this.searchService = searchService;
     }

     public AudienciaOralSelectableDataModel getDataModel() {
          return dataModel;
     }

     public void setDataModel(AudienciaOralSelectableDataModel dataModel) {
          this.dataModel = dataModel;
     }

     public AudienciaOral getAudienciaOralSeleccionada() {
          return audienciaOralSeleccionada;
     }

     public void setAudienciaOralSeleccionada(
               AudienciaOral audienciaOralSeleccionada) {
          this.audienciaOralSeleccionada = audienciaOralSeleccionada;
     }
}
