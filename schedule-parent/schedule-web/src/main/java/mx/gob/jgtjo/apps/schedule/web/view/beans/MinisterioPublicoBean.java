package mx.gob.jgtjo.apps.schedule.web.view.beans;

import java.io.Serializable;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;

import mx.gob.jgtjo.apps.schedule.model.MinisterioPublico;
import mx.gob.jgtjo.apps.schedule.web.services.ScheduleServiceLocator;

@ManagedBean(name = "mpsBean")
@ViewScoped
public class MinisterioPublicoBean implements Serializable {

	private static final long serialVersionUID = 1L;

	@ManagedProperty(value = "#{serviceLocator}")
	private ScheduleServiceLocator serviceLocator;

	private List<MinisterioPublico> ministeriosPublicos;

	private MinisterioPublico ministerioPublico = new MinisterioPublico();

	public MinisterioPublicoBean() {

	}

	@PostConstruct
	public void init() {
		this.ministeriosPublicos = this.getServiceLocator().getMinisterioPublicoService().loadAll();
	}

	public void saveMp() {
		this.serviceLocator.getMinisterioPublicoService().save(this.ministerioPublico);
		this.ministeriosPublicos.add(ministerioPublico);

		this.ministerioPublico = new MinisterioPublico();

	}

	public ScheduleServiceLocator getServiceLocator() {
		return serviceLocator;
	}

	public void setServiceLocator(ScheduleServiceLocator serviceLocator) {
		this.serviceLocator = serviceLocator;
	}

	public List<MinisterioPublico> getMinisteriosPublicos() {
		return ministeriosPublicos;
	}

	public void setMinisteriosPublicos(List<MinisterioPublico> ministeriosPublicos) {
		this.ministeriosPublicos = ministeriosPublicos;
	}

	public String getApellidoMaterno() {
		return ministerioPublico.getApellidoMaterno();
	}

	public String getApellidoPaterno() {
		return ministerioPublico.getApellidoPaterno();
	}

	public String getNombres() {
		return ministerioPublico.getNombres();
	}

	public void setApellidoMaterno(String apellidoMaterno) {
		ministerioPublico.setApellidoMaterno(apellidoMaterno);
	}

	public void setApellidoPaterno(String apellidoPaterno) {
		ministerioPublico.setApellidoPaterno(apellidoPaterno);
	}

	public void setNombres(String nombres) {
		ministerioPublico.setNombres(nombres);
	}

	public String getCedulaProfesional() {
		return ministerioPublico.getCedulaProfesional();
	}

	public void setCedulaProfesional(String cedulaProfesional) {
		ministerioPublico.setCedulaProfesional(cedulaProfesional);
	}
}
