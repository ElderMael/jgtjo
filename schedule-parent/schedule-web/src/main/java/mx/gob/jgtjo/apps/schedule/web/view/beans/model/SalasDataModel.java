package mx.gob.jgtjo.apps.schedule.web.view.beans.model;

import java.io.Serializable;
import java.util.List;

import javax.faces.model.ListDataModel;

import org.primefaces.model.SelectableDataModel;

import mx.gob.jgtjo.apps.schedule.model.Sala;

public class SalasDataModel extends ListDataModel<Sala> implements SelectableDataModel<Sala>,
		Serializable {

	private static final long serialVersionUID = 1L;

	public SalasDataModel() {

	}

	public SalasDataModel(List<Sala> data) {
		super(data);
	}

	@Override
	public Object getRowKey(Sala object) {
		return object.getNombre();
	}

	@Override
	@SuppressWarnings("unchecked")
	public Sala getRowData(String rowKey) {

		List<Sala> salas = (List<Sala>) this.getWrappedData();

		for (Sala sala : salas) {
			if (sala.getNombre().equals(rowKey)) {
				return sala;
			}
		}

		return null;
	}
}
