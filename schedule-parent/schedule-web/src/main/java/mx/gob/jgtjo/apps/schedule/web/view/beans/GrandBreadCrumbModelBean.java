package mx.gob.jgtjo.apps.schedule.web.view.beans;

import java.io.Serializable;
import java.util.LinkedList;
import java.util.List;

import javax.faces.bean.ApplicationScoped;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.component.UIComponent;

import org.primefaces.component.menuitem.MenuItem;
import org.primefaces.component.submenu.Submenu;
import org.primefaces.model.MenuModel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ocpsoft.pretty.PrettyContext;

import mx.gob.jgtjo.apps.schedule.web.utils.BreadCrumbMenuItemFactory;

@ManagedBean(name = "breadCrumbBean")
@ApplicationScoped
public class GrandBreadCrumbModelBean implements Serializable, MenuModel {

	private static final long serialVersionUID = 1L;

	protected static final Logger log = LoggerFactory.getLogger(GrandBreadCrumbModelBean.class);

	@ManagedProperty(value = "#{breadCrumbMenuFactory}")
	private BreadCrumbMenuItemFactory menuFactory;

	public GrandBreadCrumbModelBean() {

	}

	@Override
	public List<UIComponent> getContents() {

		List<UIComponent> items = new LinkedList<UIComponent>();

		String uri = PrettyContext.getCurrentInstance().getRequestURL().toURL();

		log.debug("URI from PrettyContext {}", uri);

		this.grandioseResolveOfMenusToAdd(uri, items);

		return items;
	}

	private void grandioseResolveOfMenusToAdd(String uri, List<UIComponent> items) {

		items.add(menuFactory.buildMenuItem("null"));

		String[] parts = uri.split("/");

		for (String part : parts) {
			if (part.isEmpty())
				continue;
			items.add(menuFactory.buildMenuItem(part));
		}

	}

	@Override
	public void addSubmenu(Submenu submenu) {

	}

	@Override
	public void addMenuItem(MenuItem menuItem) {

	}

	public BreadCrumbMenuItemFactory getMenuFactory() {
		return menuFactory;
	}

	public void setMenuFactory(BreadCrumbMenuItemFactory menuFactory) {
		this.menuFactory = menuFactory;
	}

}
