package mx.gob.jgtjo.apps.schedule.web.utils;

import java.util.Properties;

import org.primefaces.component.menuitem.MenuItem;

/**
 * Fabrica para generar {@link MenuItem}s con una seccion de la URI con la cual
 * se invoco una pagina web.
 * 
 * Obtiene las etiquetas y la url desde un objeto {@link Properties} de java
 * proporcionado.
 * 
 * 
 * @author ElderMael
 * 
 */
public class BreadCrumbMenuItemFactory {

	private Properties urlAndLabelMap;

	private BreadCrumbMenuItemFactory() {

	}

	/**
	 * Obtiene desde las propiedades dentro del urlAndLabelMap
	 * concatenando la seccion del uri para generar la llave para la propiedad.
	 * 
	 * @param section
	 *            - La seccion de la URI para la cual se generara el menu.
	 * @return Un menu item que posee una url y label de acuerdo a la URI
	 *         proporcionada.
	 */
	public MenuItem buildMenuItem(String section) {

		String url = urlAndLabelMap.getProperty(section + "_url");
		String label = urlAndLabelMap.getProperty(section + "_label");

		MenuItem menuItem = new MenuItem();

		menuItem.setUrl(url);
		menuItem.setValue(label);

		return menuItem;

	}

	public Properties getUrlAndLabelMap() {
		return urlAndLabelMap;
	}

	public void setUrlAndLabelMap(Properties urlAndLabelMap) {
		this.urlAndLabelMap = urlAndLabelMap;
	}

}
