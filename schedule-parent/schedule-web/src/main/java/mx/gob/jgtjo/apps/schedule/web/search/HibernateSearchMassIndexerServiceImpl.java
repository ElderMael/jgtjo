package mx.gob.jgtjo.apps.schedule.web.search;

import org.hibernate.CacheMode;
import org.hibernate.Session;
import org.hibernate.search.FullTextSession;
import org.hibernate.search.Search;
import org.hibernate.search.impl.SimpleIndexingProgressMonitor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

@Component("hibernateSearchMassIndexerService")
public class HibernateSearchMassIndexerServiceImpl implements HibernateSearchMassIndexerService {

	public static final Logger log = LoggerFactory
			.getLogger(HibernateSearchMassIndexerServiceImpl.class);

	@Override
	public void buildSearchIndex(Session session, Class<?> classToIndex) {
		log.debug("Indexing entities");

		FullTextSession fullTextSession = Search.getFullTextSession(session);

		try {
			fullTextSession.createIndexer(classToIndex).batchSizeToLoadObjects(5)
					.batchSizeToLoadObjects(5).cacheMode(CacheMode.NORMAL).threadsToLoadObjects(2)
					.threadsForIndexWriter(2).threadsForSubsequentFetching(5)
					.progressMonitor(new SimpleIndexingProgressMonitor()).startAndWait();

		} catch (InterruptedException e) {
			log.debug("Interrupted indexing process");
		}

		log.debug("Ended indexing of entities");

	}

}
