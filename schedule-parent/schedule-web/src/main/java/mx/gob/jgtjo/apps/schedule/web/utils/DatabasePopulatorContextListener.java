package mx.gob.jgtjo.apps.schedule.web.utils;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.orm.hibernate3.SessionFactoryUtils;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

import mx.gob.jgtjo.apps.schedule.model.Rol;
import mx.gob.jgtjo.apps.schedule.model.User;
import mx.gob.jgtjo.apps.schedule.service.RolService;
import mx.gob.jgtjo.apps.schedule.service.UserService;
import mx.gob.jgtjo.apps.schedule.web.conversation.ConversationManager;

/**
 * Llena la base de datos con registros de prueba, usa los servicios de la
 * aplicacion..
 * 
 * @author ElderMael
 * 
 */

public class DatabasePopulatorContextListener implements ServletContextListener {

	private static final Logger log = LoggerFactory
			.getLogger(DatabasePopulatorContextListener.class);

	public DatabasePopulatorContextListener() {

	}

	@Override
	public void contextInitialized(ServletContextEvent sce) {
		log.debug("Populating database with test records");

		populateDatabase(sce);

		log.debug("Database populated with test records successfully");

	}

	private void populateDatabase(ServletContextEvent sce) {

		WebApplicationContext context = WebApplicationContextUtils
				.getRequiredWebApplicationContext(sce.getServletContext());

		SessionFactory sessionFactory = context.getBean(SessionFactory.class);

		Session session = SessionFactoryUtils.getSession(sessionFactory, true);

		ConversationManager.startDefaultConversation(session);

		RolService rolService = context.getBean(RolService.class);

		if (!rolService.loadAll().isEmpty())
			return;

		Rol user = new Rol(null, null, "ROLE_USER", "Any user inside the app");

		Rol root = new Rol(null, null, "ROLE_ROOT", "The Almighty Lord of this App");

		Rol juez = new Rol(null, null, "ROLE_JUEZ", "Juez de garantias y tribunal de juicio oral");

		Rol oficialDePartes = new Rol(null, null, "ROLE_OFICIAL_DE_PARTES", "Oficial de Partes");

		Rol audioAndVideo = new Rol(null, null, "ROLE_AUDIO_Y_VIDEO", "Encargado de Audio y Video");

		rolService.save(user);
		rolService.save(root);
		rolService.save(oficialDePartes);
		rolService.save(juez);
		rolService.save(audioAndVideo);

		List<GrantedAuthority> mickyAuthority = new ArrayList<GrantedAuthority>(2);
		mickyAuthority.add(user);
		mickyAuthority.add(root);

		User micky = new User(null, null, "micky", "micky", true, true, true, true, mickyAuthority,
				"Miguel", "Enriquez", "Lopez", "anti_deluk@gmail.com");

		UserService userService = context.getBean(UserService.class);

		userService.save(micky);

		ConversationManager.endDefaultConversation();

	}

	@Override
	public void contextDestroyed(ServletContextEvent sce) {

	}

}
