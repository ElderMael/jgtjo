package mx.gob.jgtjo.apps.schedule.web.view.beans;

import java.io.Serializable;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.model.SelectItem;
import javax.validation.constraints.Min;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import mx.gob.jgtjo.apps.schedule.model.CausaPenal;
import mx.gob.jgtjo.apps.schedule.model.Delito;
import mx.gob.jgtjo.apps.schedule.model.DelitoConfigurado;
import mx.gob.jgtjo.apps.schedule.model.Domicilio;
import mx.gob.jgtjo.apps.schedule.model.EstadoCivil;
import mx.gob.jgtjo.apps.schedule.model.Genero;
import mx.gob.jgtjo.apps.schedule.model.MinisterioPublico;
import mx.gob.jgtjo.apps.schedule.model.ParteMaterial;
import mx.gob.jgtjo.apps.schedule.service.SearchService;
import mx.gob.jgtjo.apps.schedule.web.services.ScheduleServiceLocator;
import mx.gob.jgtjo.apps.schedule.web.utils.Constantes;
import mx.gob.jgtjo.apps.schedule.web.utils.JsfUtils;
import mx.gob.jgtjo.apps.schedule.web.view.beans.model.DelitoDataModel;

@ManagedBean(name = "addCausaBean")
@ViewScoped
public class AddCausaBean implements Serializable {

     private static final long serialVersionUID = 1L;

     private static final Logger log = LoggerFactory
               .getLogger(AddCausaBean.class);

     @ManagedProperty(value = "#{serviceLocator}")
     private ScheduleServiceLocator serviceLocator;

     @ManagedProperty(value = "#{searchService}")
     private SearchService searchService;

     private List<DelitoConfigurado> delitosConfigurados = new LinkedList<DelitoConfigurado>();

     private Integer delitoActual;

     private Boolean calificado;

     private Boolean culposo;

     private Boolean tentativa;

     private String aliasImputado;

     private String aliasVictima;

     private List<MinisterioPublico> selectedMps;

     @Min(value = 2009, message = Constantes.CAUSA_MINIMAL_YEAR_VALIDATION_MESSAGE_KEY)
     private Integer annioCausa = Calendar.getInstance().get(Calendar.YEAR);

     private String apellidoMaternoImputado;

     private String apellidoMaternoVictima;

     private String apellidoPaternoImputado;

     private String apellidoPaternoVictima;

     private String calleImputado;

     private String calleVictima;

     private Integer codigoPostalImputado;

     private Integer codigoPostalVictima;

     private String coloniaImputado;

     private String coloniaVictima;

     private DelitoConfigurado[] delitosSeleccionados;

     private List<Domicilio> domiciliosImputado;

     private List<Domicilio> domiciliosVictima;

     private Integer edadImputado;

     private Integer edadVictima;

     private EstadoCivil estadoCivilImputado;

     private EstadoCivil estadoCivilVictima;

     private String estadoImputado;

     private String estadoVictima;

     private Date fechaNacimientoImputado;

     private Date fechaNacimientoVictima;

     private Genero generoImputado;

     private Genero generoVictima;

     private String identificacionImputado;

     private String identificacionVictima;

     private String idiomaImputado;

     private String idiomaVictima;

     private List<ParteMaterial> imputados = new LinkedList<ParteMaterial>();

     private String lugarNacimientoImputado;

     private String lugarNacimientoVictima;

     private String municipioImputado;

     private String municipioVictima;

     private String nombresImputado;

     private String nombresVictima;

     private Integer numeroCausa;

     private String paisImputado;

     private String paisVictima;

     private String profesionOrOficioImputado;

     private String profesionOrOficioVictima;

     private String numeroCasaVictima;

     private String numeroCasaImputado;

     private List<ParteMaterial> victimas = new LinkedList<ParteMaterial>();

     public AddCausaBean() {

     }

     @SuppressWarnings("unchecked")
     public List<MinisterioPublico> completeMps(String query) {

          if ( query.isEmpty() )
               return Collections.EMPTY_LIST;

          return this.searchService.searchMinisteriosPublicos(query);
     }

     public String addDomicilioImputado() {
          if ( this.domiciliosImputado == null ) {
               this.domiciliosImputado = new LinkedList<Domicilio>();
          }

          Domicilio domicilio = new Domicilio(this.calleImputado,
                    this.codigoPostalImputado, this.coloniaImputado,
                    this.estadoImputado, this.municipioImputado,
                    this.paisImputado);

          domicilio.setNumero(this.numeroCasaImputado);

          this.domiciliosImputado.add(domicilio);

          this.resetImputadoDomicilioFields();

          return null;
     }

     public String addDomicilioVictima() {
          if ( this.domiciliosVictima == null ) {
               this.domiciliosVictima = new LinkedList<Domicilio>();
          }

          Domicilio domicilio = new Domicilio(this.calleVictima,
                    this.codigoPostalVictima, this.coloniaVictima,
                    this.estadoVictima, this.municipioVictima, this.paisVictima);

          domicilio.setNumero(this.numeroCasaVictima);

          this.domiciliosVictima.add(domicilio);

          this.resetVictimaDomicilioFields();

          return null;
     }

     public String addImputado() {

          this.imputados.add(

          new ParteMaterial(this.aliasImputado, this.apellidoMaternoImputado,
                    this.apellidoPaternoImputado, null,
                    this.domiciliosImputado, this.edadImputado,
                    this.estadoCivilImputado, this.fechaNacimientoImputado,
                    this.generoImputado, this.codigoPostalImputado,
                    this.identificacionImputado, this.idiomaImputado,
                    this.lugarNacimientoImputado, this.nombresImputado,
                    this.profesionOrOficioImputado, null)

          );

          JsfUtils.addNewFacesMessageFromBundle(FacesMessage.SEVERITY_INFO,
                    null, Constantes.ADDCAUSA_NEW_IMPUTADO_KEY,
                    new Object[] { this.nombresImputado + " "
                              + this.apellidoPaternoImputado + " "
                              + this.apellidoMaternoImputado }, new Object[] {});

          this.resetImputadoFields();
          this.resetImputadoDomicilioFields();

          return null;
     }

     public String addVictima() {

          this.victimas.add(

          new ParteMaterial(this.aliasVictima, this.apellidoMaternoVictima,
                    this.apellidoPaternoVictima, null, this.domiciliosVictima,
                    this.edadVictima, this.estadoCivilVictima,
                    this.fechaNacimientoVictima, this.generoVictima, null,
                    this.identificacionVictima, this.idiomaVictima,
                    this.lugarNacimientoVictima, this.nombresVictima,
                    this.profesionOrOficioVictima, null)

          );

          JsfUtils.addNewFacesMessageFromBundle(FacesMessage.SEVERITY_INFO,
                    null, Constantes.ADDCAUSA_NEW_VICTIMA_KEY,
                    new Object[] { this.nombresVictima + " "
                              + this.apellidoPaternoVictima + " "
                              + this.apellidoMaternoVictima }, new Object[] {});

          this.resetVictimaFields();
          this.resetVictimaDomicilioFields();

          return null;
     }

     public String agregarCausaPenal() {

          CausaPenal causaPenal = new CausaPenal(null, null, this.numeroCausa,
                    this.annioCausa, this.delitosConfigurados, this.victimas,
                    this.imputados, this.selectedMps, null, null);

          log.debug("Guardando causa penal {}", causaPenal);

          this.serviceLocator.getCausaPenalService().save(causaPenal);

          // Guardar el Id en sesion
          JsfUtils.getCurrentHttpRequest()
                    .getSession(false)
                    .setAttribute(Constantes.NEW_CAUSA_PENAL_ID_ATTRIBUTE_NAME,
                              causaPenal.getNumeroCausa());

          JsfUtils.getCurrentHttpRequest()
                    .getSession(false)
                    .setAttribute(
                              Constantes.NEW_CAUSA_PENAL_YEAR_ATTRIBUTE_NAME,
                              causaPenal.getAnnioCausa());

          return "pretty:addCausaSuccess";
     }

     public String generarNumeroCausa() {

          this.numeroCausa = this.serviceLocator.getCausaPenalService()
                    .generateNewCausaPenalNumber(this.annioCausa);

          return null;
     }

     public String getAliasImputado() {
          return this.aliasImputado;
     }

     public String getAliasVictima() {
          return this.aliasVictima;
     }

     public Integer getAnnioCausa() {
          return this.annioCausa;
     }

     // Agregar causa

     public String getApellidoMaternoImputado() {
          return this.apellidoMaternoImputado;
     }

     public String getApellidoMaternoVictima() {
          return this.apellidoMaternoVictima;
     }

     public String getApellidoPaternoImputado() {
          return this.apellidoPaternoImputado;
     }

     public String getApellidoPaternoVictima() {
          return this.apellidoPaternoVictima;
     }

     public String getCalleImputado() {
          return this.calleImputado;
     }

     public String getCalleVictima() {
          return this.calleVictima;
     }

     public Integer getCodigoPostalImputado() {
          return this.codigoPostalImputado;
     }

     public Integer getCodigoPostalVictima() {
          return this.codigoPostalVictima;
     }

     public String getColoniaImputado() {
          return this.coloniaImputado;
     }

     public String getColoniaVictima() {
          return this.coloniaVictima;
     }

     // Delitos
     public DelitoDataModel getDelitos() {

          List<Delito> delitos = this.serviceLocator.getDelitoService()
                    .loadAll();
          List<DelitoConfigurado> delitosConfigurados = new LinkedList<DelitoConfigurado>();

          for (Delito delito : delitos) {
               delitosConfigurados.add(new DelitoConfigurado(null, null,
                         delito, false, false));
          }

          DelitoDataModel dataModel = new DelitoDataModel(delitosConfigurados);

          return dataModel;
     }

     public List<SelectItem> getAllDelitos() {

          List<Delito> delitos = this.serviceLocator.getDelitoService()
                    .loadAll();
          List<SelectItem> items = new LinkedList<SelectItem>();

          for (Delito delito : delitos) {

               items.add(new SelectItem(delito.getId(), delito.getNombre()));
          }

          return items;

     }

     public String addDelito() {

          Delito delito = this.serviceLocator.getDelitoService().loadById(
                    this.delitoActual);

          DelitoConfigurado delitoConfigurado = new DelitoConfigurado(null,
                    null, delito, this.calificado, this.culposo);

          delitoConfigurado.setTentativa(this.tentativa);

          this.delitosConfigurados.add(delitoConfigurado);

          this.delitoActual = null;
          this.calificado = false;
          this.culposo = false;
          this.tentativa = false;

          return null;

     }

     public DelitoConfigurado[] getDelitosSeleccionados() {
          return this.delitosSeleccionados;
     }

     public List<Domicilio> getDomiciliosImputado() {
          return this.domiciliosImputado;
     }

     public List<Domicilio> getDomiciliosVictima() {
          return this.domiciliosVictima;
     }

     public Integer getEdadImputado() {
          return this.edadImputado;
     }

     public Integer getEdadVictima() {
          return this.edadVictima;
     }

     public EstadoCivil getEstadoCivilImputado() {
          return this.estadoCivilImputado;
     }

     public EstadoCivil getEstadoCivilVictima() {
          return this.estadoCivilVictima;
     }

     public String getEstadoImputado() {
          return this.estadoImputado;
     }

     public String getEstadoVictima() {
          return this.estadoVictima;
     }

     public Date getFechaNacimientoImputado() {
          return this.fechaNacimientoImputado;
     }

     public Date getFechaNacimientoVictima() {
          return this.fechaNacimientoVictima;
     }

     public Genero getGeneroImputado() {
          return this.generoImputado;
     }

     public Genero getGeneroVictima() {
          return this.generoVictima;
     }

     public String getIdentificacionImputado() {
          return this.identificacionImputado;
     }

     public String getIdentificacionVictima() {
          return this.identificacionVictima;
     }

     public String getIdiomaImputado() {
          return this.idiomaImputado;
     }

     public String getIdiomaVictima() {
          return this.idiomaVictima;
     }

     public List<ParteMaterial> getImputados() {
          return this.imputados;
     }

     public String getLugarNacimientoImputado() {
          return this.lugarNacimientoImputado;
     }

     public String getLugarNacimientoVictima() {
          return this.lugarNacimientoVictima;
     }

     public String getMunicipioImputado() {
          return this.municipioImputado;
     }

     public String getMunicipioVictima() {
          return this.municipioVictima;
     }

     public String getNombresImputado() {
          return this.nombresImputado;
     }

     public String getNombresVictima() {
          return this.nombresVictima;
     }

     public Integer getNumeroCausa() {
          return this.numeroCausa;
     }

     public String getPaisImputado() {
          return this.paisImputado;
     }

     public String getPaisVictima() {
          return this.paisVictima;
     }

     public String getProfesionOrOficioImputado() {
          return this.profesionOrOficioImputado;
     }

     public String getProfesionOrOficioVictima() {
          return this.profesionOrOficioVictima;
     }

     public List<ParteMaterial> getVictimas() {
          return this.victimas;
     }

     public void setAliasImputado(String aliasImputado) {
          this.aliasImputado = aliasImputado;
     }

     public void setAliasVictima(String aliasVictima) {
          this.aliasVictima = aliasVictima;
     }

     public void setAnnioCausa(Integer annioCausa) {
          this.annioCausa = annioCausa;
     }

     public void setApellidoMaternoImputado(String apellidoMaternoImputado) {
          this.apellidoMaternoImputado = apellidoMaternoImputado;
     }

     public void setApellidoMaternoVictima(String apellidoMaternoVictima) {
          this.apellidoMaternoVictima = apellidoMaternoVictima;
     }

     public void setApellidoPaternoImputado(String apellidoPaternoImputado) {
          this.apellidoPaternoImputado = apellidoPaternoImputado;
     }

     public void setApellidoPaternoVictima(String apellidoPaternoVictima) {
          this.apellidoPaternoVictima = apellidoPaternoVictima;
     }

     public void setCalleImputado(String calleImputado) {
          this.calleImputado = calleImputado;
     }

     public void setCalleVictima(String calleVictima) {
          this.calleVictima = calleVictima;
     }

     public void setCodigoPostalImputado(Integer codigoPostalImputado) {
          this.codigoPostalImputado = codigoPostalImputado;
     }

     public void setCodigoPostalVictima(Integer codigoPostalVictima) {
          this.codigoPostalVictima = codigoPostalVictima;
     }

     public void setColoniaImputado(String coloniaImputado) {
          this.coloniaImputado = coloniaImputado;
     }

     public void setColoniaVictima(String coloniaVictima) {
          this.coloniaVictima = coloniaVictima;
     }

     public void setDelitosSeleccionados(
               DelitoConfigurado[] delitosSeleccionados) {
          this.delitosSeleccionados = delitosSeleccionados;
     }

     public void setDomiciliosImputado(List<Domicilio> domiciliosImputado) {
          this.domiciliosImputado = domiciliosImputado;
     }

     public void setDomiciliosVictima(List<Domicilio> domiciliosVictima) {
          this.domiciliosVictima = domiciliosVictima;
     }

     public void setEdadImputado(Integer edadImputado) {
          this.edadImputado = edadImputado;
     }

     public void setEdadVictima(Integer edadVictima) {
          this.edadVictima = edadVictima;
     }

     public void setEstadoCivilImputado(EstadoCivil estadoCivilImputado) {
          this.estadoCivilImputado = estadoCivilImputado;
     }

     public void setEstadoCivilVictima(EstadoCivil estadoCivilVictima) {
          this.estadoCivilVictima = estadoCivilVictima;
     }

     public void setEstadoImputado(String estadoImputado) {
          this.estadoImputado = estadoImputado;
     }

     public void setEstadoVictima(String estadoVictima) {
          this.estadoVictima = estadoVictima;
     }

     public void setFechaNacimientoImputado(Date fechaNacimientoImputado) {
          this.fechaNacimientoImputado = fechaNacimientoImputado;
     }

     public void setFechaNacimientoVictima(Date fechaNacimientoVictima) {
          this.fechaNacimientoVictima = fechaNacimientoVictima;
     }

     public void setGeneroImputado(Genero generoImputado) {
          this.generoImputado = generoImputado;
     }

     public void setGeneroVictima(Genero generoVictima) {
          this.generoVictima = generoVictima;
     }

     public void setIdentificacionImputado(String identificacionImputado) {
          this.identificacionImputado = identificacionImputado;
     }

     public void setIdentificacionVictima(String identificacionVictima) {
          this.identificacionVictima = identificacionVictima;
     }

     public void setIdiomaImputado(String idiomaImputado) {
          this.idiomaImputado = idiomaImputado;
     }

     public void setIdiomaVictima(String idiomaVictima) {
          this.idiomaVictima = idiomaVictima;
     }

     public void setImputados(List<ParteMaterial> imputados) {
          this.imputados = imputados;
     }

     public void setLugarNacimientoImputado(String lugarNacimientoImputado) {
          this.lugarNacimientoImputado = lugarNacimientoImputado;
     }

     public void setLugarNacimientoVictima(String lugarNacimientoVictima) {
          this.lugarNacimientoVictima = lugarNacimientoVictima;
     }

     public void setMunicipioImputado(String municipioImputado) {
          this.municipioImputado = municipioImputado;
     }

     public void setMunicipioVictima(String municipioVictima) {
          this.municipioVictima = municipioVictima;
     }

     public void setNombresImputado(String nombresImputado) {
          this.nombresImputado = nombresImputado;
     }

     public void setNombresVictima(String nombresVictima) {
          this.nombresVictima = nombresVictima;
     }

     public void setNumeroCausa(Integer numeroCausa) {
          this.numeroCausa = numeroCausa;
     }

     public void setPaisImputado(String paisImputado) {
          this.paisImputado = paisImputado;
     }

     public void setPaisVictima(String paisVictima) {
          this.paisVictima = paisVictima;
     }

     public void setProfesionOrOficioImputado(String profesionOrOficioImputado) {
          this.profesionOrOficioImputado = profesionOrOficioImputado;
     }

     public void setProfesionOrOficioVictima(String profesionOrOficioVictima) {
          this.profesionOrOficioVictima = profesionOrOficioVictima;
     }

     public void setVictimas(List<ParteMaterial> victimas) {
          this.victimas = victimas;
     }

     private void resetImputadoDomicilioFields() {
          this.calleImputado = null;
          this.codigoPostalImputado = null;
          this.coloniaImputado = null;
          this.estadoImputado = null;
          this.municipioImputado = null;
          this.paisImputado = null;
          this.numeroCasaImputado = null;
     }

     private void resetImputadoFields() {
          this.aliasImputado = null;
          this.apellidoMaternoImputado = null;
          this.apellidoPaternoImputado = null;
          this.domiciliosImputado = new LinkedList<Domicilio>();
          this.edadImputado = null;
          this.estadoCivilImputado = null;
          this.fechaNacimientoImputado = null;
          this.generoImputado = null;
          this.identificacionImputado = null;
          this.idiomaImputado = null;
          this.lugarNacimientoImputado = null;
          this.nombresImputado = null;
          this.profesionOrOficioImputado = null;
     }

     private void resetVictimaDomicilioFields() {
          this.calleVictima = null;
          this.codigoPostalVictima = null;
          this.coloniaVictima = null;
          this.estadoVictima = null;
          this.municipioVictima = null;
          this.paisVictima = null;
          this.numeroCasaVictima = null;
     }

     private void resetVictimaFields() {
          this.aliasVictima = null;
          this.apellidoMaternoVictima = null;
          this.apellidoPaternoVictima = null;
          this.domiciliosVictima = new LinkedList<Domicilio>();
          this.edadVictima = null;
          this.estadoCivilVictima = null;
          this.fechaNacimientoVictima = null;
          this.generoVictima = null;
          this.identificacionVictima = null;
          this.idiomaVictima = null;
          this.lugarNacimientoVictima = null;
          this.nombresVictima = null;
          this.profesionOrOficioVictima = null;
     }

     public List<DelitoConfigurado> getDelitosConfigurados() {
          return delitosConfigurados;
     }

     public void setDelitosConfigurados(
               List<DelitoConfigurado> delitosConfigurados) {
          this.delitosConfigurados = delitosConfigurados;
     }

     public Integer getDelitoActual() {
          return delitoActual;
     }

     public void setDelitoActual(Integer delitoActual) {
          this.delitoActual = delitoActual;
     }

     public Boolean getCalificado() {
          return calificado;
     }

     public void setCalificado(Boolean calificado) {
          this.calificado = calificado;
     }

     public ScheduleServiceLocator getServiceLocator() {
          return serviceLocator;
     }

     public void setServiceLocator(ScheduleServiceLocator serviceLocator) {
          this.serviceLocator = serviceLocator;
     }

     public String getNumeroCasaVictima() {
          return numeroCasaVictima;
     }

     public void setNumeroCasaVictima(String numeroCasaVictima) {
          this.numeroCasaVictima = numeroCasaVictima;
     }

     public String getNumeroCasaImputado() {
          return numeroCasaImputado;
     }

     public void setNumeroCasaImputado(String numeroCasaImputado) {
          this.numeroCasaImputado = numeroCasaImputado;
     }

     public Boolean getCulposo() {
          return this.culposo;
     }

     public void setCulposo(Boolean culposo) {
          this.culposo = culposo;
     }

     public List<MinisterioPublico> getSelectedMps() {
          return selectedMps;
     }

     public void setSelectedMps(List<MinisterioPublico> selectedMps) {
          this.selectedMps = selectedMps;
     }

     public SearchService getSearchService() {
          return searchService;
     }

     public void setSearchService(SearchService searchService) {
          this.searchService = searchService;
     }

     public Boolean getTentativa() {
          return tentativa;
     }

     public void setTentativa(Boolean tentativa) {
          this.tentativa = tentativa;
     }

}
