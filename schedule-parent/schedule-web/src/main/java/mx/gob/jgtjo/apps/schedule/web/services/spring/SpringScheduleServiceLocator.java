package mx.gob.jgtjo.apps.schedule.web.services.spring;

import java.io.Serializable;

import javax.faces.bean.ApplicationScoped;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;

import mx.gob.jgtjo.apps.schedule.service.AudienciaOralService;
import mx.gob.jgtjo.apps.schedule.service.CausaPenalService;
import mx.gob.jgtjo.apps.schedule.service.DelitoService;
import mx.gob.jgtjo.apps.schedule.service.JuezService;
import mx.gob.jgtjo.apps.schedule.service.MinisterioPublicoService;
import mx.gob.jgtjo.apps.schedule.service.RolService;
import mx.gob.jgtjo.apps.schedule.service.SalaService;
import mx.gob.jgtjo.apps.schedule.service.SearchService;
import mx.gob.jgtjo.apps.schedule.service.TipoAudienciaService;
import mx.gob.jgtjo.apps.schedule.service.UserService;
import mx.gob.jgtjo.apps.schedule.web.services.ScheduleServiceLocator;

/**
 * Implementacion concreta de {@link ScheduleServiceLocator}.
 * 
 * Obtiene los servicios mediante inyeccion de dependencias de JSF 2. Por la
 * arquitectura de esta apliacion, estos deberian ser obtenidos del
 * WebApplicationContext de Spring.
 * 
 * @author ElderMael
 * 
 */
@ManagedBean(name = "serviceLocator", eager = true)
@ApplicationScoped
public class SpringScheduleServiceLocator implements ScheduleServiceLocator, Serializable {

	private static final long serialVersionUID = -8092272492696461131L;

	@ManagedProperty(value = "#{audienciaOralService}")
	private transient AudienciaOralService audienciaOralService;

	@ManagedProperty(value = "#{causaPenalService}")
	private transient CausaPenalService causaPenalService;

	@ManagedProperty(value = "#{delitoService}")
	private transient DelitoService delitoService;

	@ManagedProperty(value = "#{juezService}")
	private transient JuezService juezService;

	@ManagedProperty(value = "#{rolService}")
	private transient RolService rolService;

	@ManagedProperty(value = "#{salaService}")
	private transient SalaService salaService;

	@ManagedProperty(value = "#{tipoAudienciaService}")
	private transient TipoAudienciaService tipoAudienciaService;

	@ManagedProperty(value = "#{userService}")
	private transient UserService userService;

	@ManagedProperty(value = "#{searchService}")
	private transient SearchService searchService;

	@ManagedProperty(value = "#{ministerioPublicoService}")
	private transient MinisterioPublicoService ministerioPublicoService;

	public SpringScheduleServiceLocator() {

	}

	@Override
	public AudienciaOralService getAudienciaOralService() {
		return this.audienciaOralService;
	}

	public void setAudienciaOralService(AudienciaOralService audienciaOralService) {
		this.audienciaOralService = audienciaOralService;
	}

	@Override
	public CausaPenalService getCausaPenalService() {
		return this.causaPenalService;
	}

	public void setCausaPenalService(CausaPenalService causaPenalService) {
		this.causaPenalService = causaPenalService;
	}

	@Override
	public DelitoService getDelitoService() {
		return this.delitoService;
	}

	public void setDelitoService(DelitoService delitoService) {
		this.delitoService = delitoService;
	}

	@Override
	public JuezService getJuezService() {
		return this.juezService;
	}

	public void setJuezService(JuezService juezService) {
		this.juezService = juezService;
	}

	@Override
	public RolService getRolService() {
		return this.rolService;
	}

	public void setRolService(RolService rolService) {
		this.rolService = rolService;
	}

	@Override
	public SalaService getSalaService() {
		return this.salaService;
	}

	public void setSalaService(SalaService salaService) {
		this.salaService = salaService;
	}

	@Override
	public TipoAudienciaService getTipoAudienciaService() {
		return this.tipoAudienciaService;
	}

	public void setTipoAudienciaService(TipoAudienciaService tipoAudienciaService) {
		this.tipoAudienciaService = tipoAudienciaService;
	}

	@Override
	public UserService getUserService() {
		return this.userService;
	}

	public void setUserService(UserService userService) {
		this.userService = userService;
	}

	public SearchService getSearchService() {
		return searchService;
	}

	public void setSearchService(SearchService searchService) {
		this.searchService = searchService;
	}

	public MinisterioPublicoService getMinisterioPublicoService() {
		return ministerioPublicoService;
	}

	public void setMinisterioPublicoService(MinisterioPublicoService ministerioPublicoService) {
		this.ministerioPublicoService = ministerioPublicoService;
	}

}
