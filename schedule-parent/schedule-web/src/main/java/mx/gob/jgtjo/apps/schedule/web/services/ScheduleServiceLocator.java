package mx.gob.jgtjo.apps.schedule.web.services;

import mx.gob.jgtjo.apps.schedule.service.AudienciaOralService;
import mx.gob.jgtjo.apps.schedule.service.CausaPenalService;
import mx.gob.jgtjo.apps.schedule.service.DelitoService;
import mx.gob.jgtjo.apps.schedule.service.JuezService;
import mx.gob.jgtjo.apps.schedule.service.MinisterioPublicoService;
import mx.gob.jgtjo.apps.schedule.service.RolService;
import mx.gob.jgtjo.apps.schedule.service.SalaService;
import mx.gob.jgtjo.apps.schedule.service.SearchService;
import mx.gob.jgtjo.apps.schedule.service.TipoAudienciaService;
import mx.gob.jgtjo.apps.schedule.service.UserService;

/**
 * Interface para el localizador de servicios del modulo web.
 * 
 * Las implemetaciones de esta interface estan encargadas de buscar/generar
 * todos los servicios declarados.
 * 
 * @author ElderMael
 * 
 */
public interface ScheduleServiceLocator {

	AudienciaOralService getAudienciaOralService();

	CausaPenalService getCausaPenalService();

	DelitoService getDelitoService();

	JuezService getJuezService();

	RolService getRolService();

	SalaService getSalaService();

	TipoAudienciaService getTipoAudienciaService();

	UserService getUserService();

	SearchService getSearchService();

	MinisterioPublicoService getMinisterioPublicoService();

}
