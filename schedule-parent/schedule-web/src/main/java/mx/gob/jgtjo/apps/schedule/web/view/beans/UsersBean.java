package mx.gob.jgtjo.apps.schedule.web.view.beans;

import java.io.Serializable;
import java.util.LinkedList;
import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;

import org.primefaces.model.DualListModel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.core.GrantedAuthority;

import mx.gob.jgtjo.apps.schedule.model.Genero;
import mx.gob.jgtjo.apps.schedule.model.Juez;
import mx.gob.jgtjo.apps.schedule.model.Rol;
import mx.gob.jgtjo.apps.schedule.model.User;
import mx.gob.jgtjo.apps.schedule.web.services.ScheduleServiceLocator;
import mx.gob.jgtjo.apps.schedule.web.utils.Constantes;
import mx.gob.jgtjo.apps.schedule.web.utils.JsfUtils;

@ManagedBean(name = "usersBean")
@ViewScoped
public class UsersBean implements Serializable {

     private static final long serialVersionUID = 1L;

     private static final Logger log = LoggerFactory.getLogger(UsersBean.class);

     @ManagedProperty(value = "#{serviceLocator}")
     private ScheduleServiceLocator serviceLocator;

     private User user = new User();

     private String confirmationPass;

     private DualListModel<Rol> rolDualListModel;

     private Genero genero;

     public UsersBean() {

     }

     public List<User> getAllUsers() {
          return this.serviceLocator.getUserService().loadAll();
     }

     public void addUser() {

          if ( !(user.getPassword().equals(this.confirmationPass)) ) {
               JsfUtils.addNewFacesMessageFromBundle(
                         FacesMessage.SEVERITY_ERROR, null,
                         Constantes.PASSWORD_VERIFICATION_MISMATCH);

               return;
          }

          boolean esJuez = buildUser();

          this.serviceLocator.getUserService().save(this.user);

          log.debug("Added user to app {}", this.user);

          if ( esJuez ) {
               Juez juez = new Juez(null, null, this.user.getNombres(),
                         this.user.getApellidoPaterno(),
                         this.user.getApellidoMaterno(), true, null);

               juez.setGenero(this.genero);

               this.serviceLocator.getJuezService().save(juez);

               log.debug("Guardado juez {}", juez);
          }

          this.user = new User();

          this.rolDualListModel = null;
     }

     private boolean buildUser() {
          boolean esJuez = false;

          this.user.setAccountNonExpired(true);
          this.user.setAccountNonLocked(true);
          this.user.setCredentialsNonExpired(true);
          this.user.setEnabled(true);

          List<GrantedAuthority> authorities = new LinkedList<GrantedAuthority>();
          for (Rol rol : this.rolDualListModel.getTarget()) {
               if ( rol.getAuthority().equals(Constantes.JUEZ_ROLE_STRING) )
                    esJuez = true;
               authorities.add(rol);
          }

          this.user.setAuthorities(authorities);
          return esJuez;
     }

     public ScheduleServiceLocator getServiceLocator() {
          return serviceLocator;
     }

     public void setServiceLocator(ScheduleServiceLocator serviceLocator) {
          this.serviceLocator = serviceLocator;
     }

     public String getApellidoMaterno() {
          return user.getApellidoMaterno();
     }

     public String getApellidoPaterno() {
          return user.getApellidoPaterno();
     }

     public String getEmail() {
          return user.getEmail();
     }

     public String getNombres() {
          return user.getNombres();
     }

     public String getPassword() {
          return user.getPassword();
     }

     public String getUserName() {
          return user.getUsername();
     }

     public void setApellidoMaterno(String apellidoMaterno) {
          user.setApellidoMaterno(apellidoMaterno);
     }

     public void setApellidoPaterno(String apellidoPaterno) {
          user.setApellidoPaterno(apellidoPaterno);
     }

     public void setEmail(String email) {
          user.setEmail(email);
     }

     public void setNombres(String nombres) {
          user.setNombres(nombres);
     }

     public void setPassword(String password) {
          user.setPassword(password);
     }

     public void setUserName(String username) {
          user.setUsername(username);
     }

     public DualListModel<Rol> getRolDualListModel() {
          if ( this.rolDualListModel == null ) {
               List<Rol> roles = this.serviceLocator.getRolService().loadAll();

               this.rolDualListModel = new DualListModel<Rol>(roles,
                         new LinkedList<Rol>());
          }

          return rolDualListModel;
     }

     public void setRolDualListModel(DualListModel<Rol> rolDualListModel) {
          this.rolDualListModel = rolDualListModel;
     }

     public Genero getGenero() {
          return genero;
     }

     public void setGenero(Genero genero) {
          this.genero = genero;
     }

     public String getConfirmationPass() {
          return confirmationPass;
     }

     public void setConfirmationPass(String confirmationPass) {
          this.confirmationPass = confirmationPass;
     }

}
