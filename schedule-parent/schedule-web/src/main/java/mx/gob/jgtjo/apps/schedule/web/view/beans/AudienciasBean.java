package mx.gob.jgtjo.apps.schedule.web.view.beans;

import java.io.Serializable;
import java.util.LinkedList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import mx.gob.jgtjo.apps.schedule.model.TipoAudiencia;
import mx.gob.jgtjo.apps.schedule.web.services.ScheduleServiceLocator;
import mx.gob.jgtjo.apps.schedule.web.utils.Constantes;
import mx.gob.jgtjo.apps.schedule.web.utils.JsfUtils;
import mx.gob.jgtjo.apps.schedule.web.view.beans.model.TipoAudienciaDataModel;

@ManagedBean(name = "audienciasBean")
@ViewScoped
public class AudienciasBean implements Serializable {

	private static final long serialVersionUID = 1L;

	protected static final Logger log = LoggerFactory.getLogger(AudienciasBean.class);

	@ManagedProperty(value = "#{serviceLocator}")
	private ScheduleServiceLocator serviceLocator;

	private TipoAudienciaDataModel dataModel;

	private TipoAudiencia tipoAudiencia;

	private List<String> tiposDeAudiencia;

	public AudienciasBean() {

	}

	@PostConstruct
	public void init() {
		this.dataModel = new TipoAudienciaDataModel(this.serviceLocator.getTipoAudienciaService()
				.loadAll());

		this.tipoAudiencia = new TipoAudiencia();
	}

	@SuppressWarnings("unchecked")
	public void addTipoAudiencia() {

		for (String tipo : this.tiposDeAudiencia) {
			if (tipo.equalsIgnoreCase("orden")) {
				this.tipoAudiencia.setOrdenAprehension(true);
			} else if (tipo.equalsIgnoreCase("formulacion")) {
				this.tipoAudiencia.setFormulacion(true);
			} else if (tipo.equalsIgnoreCase("vinculacion")) {
				this.tipoAudiencia.setVinculacion(true);
			} else if (tipo.equalsIgnoreCase("salidaAlterna")) {
				this.tipoAudiencia.setSalidaAlterna(true);
			} else if (tipo.equalsIgnoreCase("medidaCautelar")) {
				this.tipoAudiencia.setMedidaCautelar(true);
			} else if (tipo.equalsIgnoreCase("imtermedia")) {
				this.tipoAudiencia.setIntermedia(true);
			} else if (tipo.equalsIgnoreCase("juicio")) {
				this.tipoAudiencia.setJuicioOral(true);
			} else if (tipo.equalsIgnoreCase("impugnacion")) {
				this.tipoAudiencia.setImpugnacionMinisterial(true);
			} else if (tipo.equalsIgnoreCase("cateo")) {
				this.tipoAudiencia.setEsCateo(true);
			}
		}

		this.serviceLocator.getTipoAudienciaService().save(this.tipoAudiencia);

		((List<TipoAudiencia>) this.dataModel.getWrappedData()).add(tipoAudiencia);

		log.debug("Saved {}", this.tipoAudiencia);

		JsfUtils.addNewFacesMessageFromBundle(FacesMessage.SEVERITY_INFO, null,
				Constantes.TIPO_AUDIENCIA_ADDED_KEY);

		this.tipoAudiencia = new TipoAudiencia();

		this.tiposDeAudiencia = new LinkedList<String>();

	}

	public ScheduleServiceLocator getServiceLocator() {
		return serviceLocator;
	}

	public void setServiceLocator(ScheduleServiceLocator serviceLocator) {
		this.serviceLocator = serviceLocator;
	}

	public String getDescripcion() {
		return tipoAudiencia.getDescripcion();
	}

	public String getNombre() {
		return tipoAudiencia.getNombre();
	}

	public void setDescripcion(String descripcion) {
		tipoAudiencia.setDescripcion(descripcion);
	}

	public void setNombre(String nombre) {
		tipoAudiencia.setNombre(nombre);
	}

	public TipoAudienciaDataModel getDataModel() {
		return dataModel;
	}

	public void setDataModel(TipoAudienciaDataModel dataModel) {
		this.dataModel = dataModel;
	}

	public List<String> getTiposDeAudiencia() {
		return tiposDeAudiencia;
	}

	public void setTiposDeAudiencia(List<String> tiposDeAudiencia) {
		this.tiposDeAudiencia = tiposDeAudiencia;
	}

}
