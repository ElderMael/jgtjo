package mx.gob.jgtjo.apps.schedule.web.reports;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.ResourceBundle;
import java.util.TimeZone;

import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.StreamedContent;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.io.ClassPathResource;
import org.springframework.stereotype.Component;

import mx.gob.jgtjo.apps.schedule.model.AudienciaOral;
import mx.gob.jgtjo.apps.schedule.model.DelitoConfigurado;
import mx.gob.jgtjo.apps.schedule.model.Juez;
import mx.gob.jgtjo.apps.schedule.model.MinisterioPublico;
import mx.gob.jgtjo.apps.schedule.model.ParteMaterial;
import mx.gob.jgtjo.apps.schedule.web.utils.Constantes;
import mx.gob.jgtjo.apps.schedule.web.utils.ScheduleUtils;

import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.data.JRBeanArrayDataSource;

@Component("reportFactory")
public class AudienciaOralReportFactory implements Serializable {

	protected static final Logger log = LoggerFactory.getLogger(AudienciaOralReportFactory.class);

	private static final long serialVersionUID = 1L;

	private JasperReport controlDeEventosImpugnacion;

	private JasperReport controlDeEventosReport;

	private JasperReport datosParaJuezImpugnacionReport;

	private JasperReport datosParaJuezReport;

	private JasperReport protocoloAudienciaReport;

	private JasperReport protocoloAudienciaImpugnacionReport;

	public AudienciaOralReportFactory() {

		try {
			controlDeEventosReport = JasperCompileManager.compileReport(new ClassPathResource(
					Constantes.HOJA_DE_DATOS_REPORT_PATH).getInputStream());

			datosParaJuezReport = JasperCompileManager.compileReport(new ClassPathResource(
					Constantes.DATOS_JUEZ_REPORT_PATH).getInputStream());

			protocoloAudienciaReport = JasperCompileManager.compileReport(new ClassPathResource(
					Constantes.PROTOCOLO_AUDIENCIA_REPORT_PATH).getInputStream());

			controlDeEventosImpugnacion = JasperCompileManager.compileReport(new ClassPathResource(
					Constantes.HOJA_DE_DATOS_IMPUGNACION_REPORT_PATH).getInputStream());

			datosParaJuezImpugnacionReport = JasperCompileManager
					.compileReport(new ClassPathResource(
							Constantes.DATOS_JUEZ_IMPUGNACION_REPORT_PATH).getInputStream());

			protocoloAudienciaImpugnacionReport = JasperCompileManager
					.compileReport(new ClassPathResource(
							Constantes.PROTOCOLO_AUDIENCIA_IMPUGNACION_REPORT_PATH)
							.getInputStream());

		} catch (Exception e) {
			throw new RuntimeException(e);
		}

	}

	private Map<String, Object> buildParametersForEventosReport(AudienciaOral audiencia,
			String causaNumberAndYear, String numeroAudiencia, Locale locale,
			ResourceBundle resourceBundle, TimeZone timeZone, List<ParteMaterial> exclusiones) {

		String victimas = formatVictimas(audiencia.getCausaPenal().getVictimas(), exclusiones);

		String delitos = formatDelitos(audiencia.getCausaPenal().getDelitosConfigurados(),
				resourceBundle);

		String imputados = formatImputados(audiencia.getCausaPenal().getImputados(), exclusiones);

		String jueces = formatJueces(audiencia.getJueces());

		SimpleDateFormat fechaFormat = new SimpleDateFormat(
				resourceBundle.getString("report.date.format"), locale);

		SimpleDateFormat horaFormat = new SimpleDateFormat(
				resourceBundle.getString("report.time.format"), locale);

		fechaFormat.setTimeZone(timeZone);
		horaFormat.setTimeZone(timeZone);

		String fecha = fechaFormat.format(audiencia.getFechaInicio());
		String hora = horaFormat.format(audiencia.getFechaInicio()) + " Hrs.";

		InputStream stripesImage;

		try {
			stripesImage = new ClassPathResource(
					resourceBundle.getString("report.images.background")).getInputStream();
		} catch (IOException e) {

			throw new RuntimeException(e);
		}

		Map<String, Object> parameters = new HashMap<String, Object>();

		parameters.put(Constantes.EVENTOS_AUDIENCIA_REPORT_NUMERO_AUDIENCIA_PARAMETER_NAME,
				numeroAudiencia);
		parameters.put(Constantes.EVENTOS_AUDIENCIA_REPORT_FECHA_AUDIENCIA_PARAMETER_NAME, fecha);
		parameters.put(Constantes.EVENTOS_AUDIENCIA_REPORT_FECHA_INICIO_PARAMETER_NAME, hora);

		parameters.put(Constantes.EVENTOS_AUDIENCIA_REPORT_TIPO_AUDIENCIA_NAME_PARAMETER_NAME,
				audiencia.getTipoAudiencia().getNombre());

		parameters.put(Constantes.EVENTOS_AUDIENCIA_REPORT_CAUSA_NUMBER_AND_YEAR_PARAMETER_NAME,
				causaNumberAndYear);

		parameters.put(Constantes.EVENTOS_AUDIENCIA_REPORT_JUEZ_PARAMETER_NAME, jueces);

		parameters.put(Constantes.EVENTOS_AUDIENCIA_REPORT_SALA_PARAMETER_NAME, audiencia.getSala()
				.getNombre());

		parameters.put(Constantes.EVENTOS_AUDIENCIA_REPORT_DELITOS_BY_NAME_PARAMETER_NAME,
				delitos.toString());

		parameters.put(Constantes.EVENTOS_AUDIENCIA_REPORT_IMPUTADOS_PARAMETER_NAME, imputados);

		parameters.put(Constantes.EVENTOS_AUDIENCIA_REPORT_VICTIMAS_BY_NAME_PARAMETER_NAME,
				victimas.toString());

		parameters.put(Constantes.EVENTOS_AUDIENCIA_REPORT_STRIPES_IMAGE_PARAMETER_NAME,
				stripesImage);

		parameters.put("REPORT_RESOURCE_BUNDLE", resourceBundle);

		return parameters;
	}

	private Map<String, Object> buildParametersForJuezReport(AudienciaOral audiencia,
			String causaNumberAndYear, String numeroAudiencia, Locale locale,
			ResourceBundle resourceBundle, TimeZone timeZone, List<ParteMaterial> exclusiones) {

		SimpleDateFormat fechaFormat = new SimpleDateFormat(
				resourceBundle.getString("report.date.format"), locale);

		String fecha = fechaFormat.format(audiencia.getFechaInicio());

		String imputados = formatImputados(audiencia.getCausaPenal().getImputados(), exclusiones);

		String delitos = formatDelitos(audiencia.getCausaPenal().getDelitosConfigurados(),
				resourceBundle);

		String victimas = formatVictimas(audiencia.getCausaPenal().getVictimas(), exclusiones);

		Map<String, Object> parameters = new HashMap<String, Object>();

		parameters.put(Constantes.DATOS_JUEZ_CAUSA_NUMBER_AND_YEAR_PARAMETER_NAME,
				causaNumberAndYear);

		parameters.put(Constantes.DATOS_JUEZ_IMPUTADOS_PARAMETER_NAME, imputados);
		parameters.put(Constantes.DATOS_JUEZ_DELITOS_PARAMETER_NAME, delitos);
		parameters.put(Constantes.DATOS_JUEZ_VICTIMAS_PARAMETER_NAME, victimas);

		parameters.put(Constantes.EVENTOS_AUDIENCIA_REPORT_FECHA_AUDIENCIA_PARAMETER_NAME, fecha);

		parameters.put(Constantes.DATOS_JUEZ_TIPO_AUDIENCIA_PARAMETER_NAME, audiencia
				.getTipoAudiencia().getNombre());

		parameters.put("REPORT_RESOURCE_BUNDLE", resourceBundle);

		return parameters;
	}

	private Map<String, Object> buildParametersForProtocoloReport(AudienciaOral audiencia,
			Locale locale, ResourceBundle resourceBundle, TimeZone timeZone,
			List<ParteMaterial> exclusiones) {

		SimpleDateFormat fechaFormat = new SimpleDateFormat(
				resourceBundle.getString("report.date.format.full"), locale);

		SimpleDateFormat horaFormat = new SimpleDateFormat(
				resourceBundle.getString("report.time.format"), locale);

		fechaFormat.setTimeZone(timeZone);
		horaFormat.setTimeZone(timeZone);

		String imputados = this.formatImputados(audiencia.getCausaPenal().getImputados(),
				exclusiones);

		String delitos = this.formatDelitos(audiencia.getCausaPenal().getDelitosConfigurados(),
				resourceBundle);

		String victimas = this.formatVictimas(audiencia.getCausaPenal().getVictimas(), exclusiones);

		String jueces = this.formatJueces(audiencia.getJueces());

		Map<String, Object> parameters = new HashMap<String, Object>();

		parameters.put("REPORT_RESOURCE_BUNDLE", resourceBundle);

		parameters.put("SalaAudiencia", audiencia.getSala().getNombre().toUpperCase(locale));
		parameters.put("Ubicacion", audiencia.getSala().getLocacion().toUpperCase(locale));

		parameters.put("HoraAudiencia",
				horaFormat.format(audiencia.getFechaInicio()).toUpperCase(locale));
		parameters.put("FechaAudiencia", fechaFormat.format(audiencia.getFechaInicio())
				.toUpperCase(locale));

		parameters.put("TipoAudiencia", audiencia.getTipoAudiencia().getNombre()
				.toUpperCase(locale));

		parameters.put("CausaNumeroAndAnnio", ScheduleUtils.formatCausaNumberAndYear(audiencia
				.getCausaPenal().getNumeroCausa(), audiencia.getCausaPenal().getAnnioCausa()));

		parameters.put("Imputados", imputados.toUpperCase(locale));

		parameters.put("Delitos", delitos.toUpperCase(locale));

		parameters.put("Victimas", victimas.toString().toUpperCase(locale));

		parameters.put("JuezAudiencia", jueces.toString().toUpperCase(locale));

		return parameters;

	}

	private String formatCausaNumberAndYear(AudienciaOral audiencia) {

		return ScheduleUtils.formatCausaNumberAndYear(audiencia.getCausaPenal().getNumeroCausa(),
				audiencia.getCausaPenal().getAnnioCausa());

	}

	private String formatDelitos(List<DelitoConfigurado> delitosConfigurados,
			ResourceBundle resourceBundle) {

		if (delitosConfigurados == null || delitosConfigurados.isEmpty())
			return "";

		StringBuilder retVal = new StringBuilder();

		for (DelitoConfigurado delito : delitosConfigurados) {

			retVal.append(delito.getDelito().getNombre())
					.append(" ")
					.append(delito.getCalificado() ? resourceBundle
							.getString("report.delitos.calificado") : "")
					.append(delito.getCulposo() ? resourceBundle
							.getString("report.delitos.culposo") : "").append("<br />");
		}

		return retVal.toString();
	}

	private String formatImputados(List<ParteMaterial> imputados, List<ParteMaterial> exclusiones) {

		if (imputados == null || imputados.isEmpty())
			return "";

		StringBuilder retVal = new StringBuilder();

		for (ParteMaterial imputado : imputados) {

			if (exclusiones.contains(imputado))
				continue;

			retVal.append(imputado.getNombres()).append(" ").append(imputado.getApellidoPaterno())
					.append(" ").append(imputado.getApellidoMaterno()).append(" <br />");
		}

		return retVal.toString();
	}

	private String formatJueces(List<Juez> jueces) {

		if (jueces == null || jueces.isEmpty())
			return "";

		StringBuilder retVal = new StringBuilder();

		for (Juez juez : jueces) {
			retVal.append(juez.getNombres()).append(" ").append(juez.getApellidoPaterno())
					.append(" ").append(juez.getApellidoMaterno()).append(" <br />");
		}
		return retVal.toString();
	}

	private String formatMinisteriosPublicos(List<MinisterioPublico> ministeriosPublicos) {

		if (ministeriosPublicos == null || ministeriosPublicos.isEmpty())
			return "";

		StringBuilder retVal = new StringBuilder();

		for (MinisterioPublico mp : ministeriosPublicos) {
			retVal.append(mp.getNombres()).append(" ").append(mp.getApellidoPaterno()).append(" ")
					.append(mp.getApellidoMaterno()).append("<br />");
		}

		return retVal.toString();
	}

	private String formatVictimas(List<ParteMaterial> victimas, List<ParteMaterial> exclusiones) {

		if (victimas == null || victimas.isEmpty())
			return "";

		StringBuilder retVal = new StringBuilder();

		for (ParteMaterial victima : victimas) {

			if (exclusiones.contains(victima))
				continue;

			retVal.append(victima.getNombres()).append(" ").append(victima.getApellidoPaterno())
					.append(" ").append(victima.getApellidoMaterno()).append(" <br />");

		}
		return retVal.toString();
	}

	private StreamedContent generateControlEventosReport(AudienciaOral audiencia, Locale locale,
			TimeZone timeZone, List<ParteMaterial> exclusiones) {

		String causaNumberAndYear = formatCausaNumberAndYear(audiencia);

		ResourceBundle resourceBundle = ResourceBundle.getBundle(
				Constantes.REPORTS_RESOURCE_BUNDLE, locale);

		String numeroAudiencia = audiencia.getNumeroAudiencia().toString();

		Map<String, Object> parameters = buildParametersForEventosReport(audiencia,
				causaNumberAndYear, numeroAudiencia, locale, resourceBundle, timeZone, exclusiones);

		AudienciaOral[] dataSourceArray = { audiencia };

		JRDataSource dataSource = new JRBeanArrayDataSource(dataSourceArray);

		ByteArrayInputStream inputStream = null;

		try {
			JasperPrint jasperPrint = JasperFillManager.fillReport(this.controlDeEventosReport,
					parameters, dataSource);

			inputStream = new ByteArrayInputStream(
					JasperExportManager.exportReportToPdf(jasperPrint));

		} catch (JRException e) {
			throw new RuntimeException(e);
		}

		return new DefaultStreamedContent(inputStream, "application/pdf",
				causaNumberAndYear.replace("/", "_") + "_A" + numeroAudiencia + "_ControlEventos");
	}

	private StreamedContent generateControlEventosReportForImpugnacion(AudienciaOral audiencia,
			Locale locale, TimeZone timeZone, List<ParteMaterial> exclusiones) {

		String causaNumberAndYear = formatCausaNumberAndYear(audiencia);

		ResourceBundle resourceBundle = ResourceBundle.getBundle(
				Constantes.REPORTS_RESOURCE_BUNDLE, locale);

		String numeroAudiencia = audiencia.getNumeroAudiencia().toString();

		Map<String, Object> parameters = buildParametersForEventosReport(audiencia,
				causaNumberAndYear, numeroAudiencia, locale, resourceBundle, timeZone, exclusiones);

		String ministeriosPublicos = this.formatMinisteriosPublicos(audiencia.getCausaPenal()
				.getMinisteriosPublicos());

		parameters.put("MinisteriosPublicos", ministeriosPublicos);

		AudienciaOral[] dataSourceArray = { audiencia };

		JRDataSource dataSource = new JRBeanArrayDataSource(dataSourceArray);

		ByteArrayInputStream inputStream = null;

		try {
			JasperPrint jasperPrint = JasperFillManager.fillReport(
					this.controlDeEventosImpugnacion, parameters, dataSource);

			inputStream = new ByteArrayInputStream(
					JasperExportManager.exportReportToPdf(jasperPrint));

		} catch (JRException e) {
			throw new RuntimeException(e);
		}

		return new DefaultStreamedContent(inputStream, "application/pdf",
				causaNumberAndYear.replace("/", "_") + "_A" + numeroAudiencia + "_ControlEventos");

	}

	private StreamedContent generateDatosJuezImpugnacionReport(AudienciaOral audiencia,
			Locale locale, TimeZone timeZone, List<ParteMaterial> exclusiones) {

		String causaNumberAndYear = formatCausaNumberAndYear(audiencia);

		ResourceBundle resourceBundle = ResourceBundle.getBundle(
				Constantes.REPORTS_RESOURCE_BUNDLE, locale);

		String numeroAudiencia = audiencia.getNumeroAudiencia().toString();

		Map<String, Object> parameters = buildParametersForJuezReport(audiencia,
				causaNumberAndYear, numeroAudiencia, locale, resourceBundle, timeZone, exclusiones);

		String ministeriosPublicos = formatMinisteriosPublicos(audiencia.getCausaPenal()
				.getMinisteriosPublicos());

		parameters.put("MinisteriosPublicos", ministeriosPublicos);

		AudienciaOral[] dataSourceArray = { audiencia };

		JRDataSource dataSource = new JRBeanArrayDataSource(dataSourceArray);

		ByteArrayInputStream inputStream = null;

		try {
			JasperPrint jasperPrint = JasperFillManager.fillReport(
					this.datosParaJuezImpugnacionReport, parameters, dataSource);

			inputStream = new ByteArrayInputStream(
					JasperExportManager.exportReportToPdf(jasperPrint));
		} catch (JRException e) {
			throw new RuntimeException(e);
		}

		return new DefaultStreamedContent(inputStream, "application/pdf",
				causaNumberAndYear.replace("/", "_") + "_A" + numeroAudiencia + "_DatosJuez");
	}

	private StreamedContent generateDatosJuezReport(AudienciaOral audiencia, Locale locale,
			TimeZone timeZone, List<ParteMaterial> exclusiones) {

		String causaNumberAndYear = formatCausaNumberAndYear(audiencia);

		ResourceBundle resourceBundle = ResourceBundle.getBundle(
				Constantes.REPORTS_RESOURCE_BUNDLE, locale);

		String numeroAudiencia = audiencia.getNumeroAudiencia().toString();

		Map<String, Object> parameters = buildParametersForJuezReport(audiencia,
				causaNumberAndYear, numeroAudiencia, locale, resourceBundle, timeZone, exclusiones);

		AudienciaOral[] dataSourceArray = { audiencia };

		JRDataSource dataSource = new JRBeanArrayDataSource(dataSourceArray);

		ByteArrayInputStream inputStream = null;

		try {
			JasperPrint jasperPrint = JasperFillManager.fillReport(this.datosParaJuezReport,
					parameters, dataSource);

			inputStream = new ByteArrayInputStream(
					JasperExportManager.exportReportToPdf(jasperPrint));
		} catch (JRException e) {
			throw new RuntimeException(e);
		}

		return new DefaultStreamedContent(inputStream, "application/pdf",
				causaNumberAndYear.replace("/", "_") + "_A" + numeroAudiencia + "_DatosJuez");

	}

	private StreamedContent generateProtocoloAudienciaReport(AudienciaOral audiencia,
			Locale locale, TimeZone timeZone, List<ParteMaterial> exclusiones) {

		String causaNumberAndYear = this.formatCausaNumberAndYear(audiencia);

		ResourceBundle resourceBundle = ResourceBundle.getBundle(
				Constantes.REPORTS_RESOURCE_BUNDLE, locale);

		String numeroAudiencia = audiencia.getNumeroAudiencia().toString();

		Map<String, Object> parameters = buildParametersForProtocoloReport(audiencia, locale,
				resourceBundle, timeZone, exclusiones);

		AudienciaOral[] dataSourceArray = { audiencia };

		JRDataSource dataSource = new JRBeanArrayDataSource(dataSourceArray);

		ByteArrayInputStream inputStream = null;

		try {

			JasperPrint jasperPrint = JasperFillManager.fillReport(this.protocoloAudienciaReport,
					parameters, dataSource);

			inputStream = new ByteArrayInputStream(
					JasperExportManager.exportReportToPdf(jasperPrint));

		} catch (JRException e) {
			throw new RuntimeException(e);
		}

		return new DefaultStreamedContent(inputStream, "application/pdf",
				causaNumberAndYear.replace("/", "_") + "_A" + numeroAudiencia + "_Protocolo");

	}

	public StreamedContent generateReport(ReportType type, AudienciaOral audiencia, Locale locale,
			TimeZone timeZone, List<ParteMaterial> exclusiones) {

		switch (type) {
		case CONTROL_EVENTOS_AUDIENCIA:
			if (!audiencia.getTipoAudiencia().getImpugnacionMinisterial()) {
				return generateControlEventosReport(audiencia, locale, timeZone, exclusiones);
			} else {
				return generateControlEventosReportForImpugnacion(audiencia, locale, timeZone,
						exclusiones);
			}
		case DATOS_PARA_JUEZ:
			if (!audiencia.getTipoAudiencia().getImpugnacionMinisterial()) {
				return generateDatosJuezReport(audiencia, locale, timeZone, exclusiones);
			} else {
				return generateDatosJuezImpugnacionReport(audiencia, locale, timeZone, exclusiones);
			}

		case PROTOCOLO_DE_AUDIENCIA:
			if (!audiencia.getTipoAudiencia().getImpugnacionMinisterial()) {
				return generateProtocoloAudienciaReport(audiencia, locale, timeZone, exclusiones);
			} else {
				return generateProtocoloAudienciaImpugnacionReport(audiencia, locale, timeZone,
						exclusiones);
			}

		default:

			throw new IllegalArgumentException("Unsupported report");

		}

	}

	private StreamedContent generateProtocoloAudienciaImpugnacionReport(AudienciaOral audiencia,
			Locale locale, TimeZone timeZone, List<ParteMaterial> exclusiones) {
		String causaNumberAndYear = this.formatCausaNumberAndYear(audiencia);

		ResourceBundle resourceBundle = ResourceBundle.getBundle(
				Constantes.REPORTS_RESOURCE_BUNDLE, locale);

		String numeroAudiencia = audiencia.getNumeroAudiencia().toString();

		Map<String, Object> parameters = buildParametersForProtocoloReport(audiencia, locale,
				resourceBundle, timeZone, exclusiones);

		String ministeriosPublicos = this.formatMinisteriosPublicos(
				audiencia.getCausaPenal().getMinisteriosPublicos()).toUpperCase(locale);

		parameters.put("MinisteriosPublicos", ministeriosPublicos);

		AudienciaOral[] dataSourceArray = { audiencia };

		JRDataSource dataSource = new JRBeanArrayDataSource(dataSourceArray);

		ByteArrayInputStream inputStream = null;

		try {

			JasperPrint jasperPrint = JasperFillManager.fillReport(
					this.protocoloAudienciaImpugnacionReport, parameters, dataSource);

			inputStream = new ByteArrayInputStream(
					JasperExportManager.exportReportToPdf(jasperPrint));

		} catch (JRException e) {
			throw new RuntimeException(e);
		}

		return new DefaultStreamedContent(inputStream, "application/pdf",
				causaNumberAndYear.replace("/", "_") + "_A" + numeroAudiencia + "_Protocolo");

	}
}
