package mx.gob.jgtjo.apps.schedule.web.view.converters;

import java.util.Map;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;

import mx.gob.jgtjo.apps.schedule.model.MinisterioPublico;
import mx.gob.jgtjo.apps.schedule.service.MinisterioPublicoService;
import mx.gob.jgtjo.apps.schedule.web.utils.Constantes;
import mx.gob.jgtjo.apps.schedule.web.utils.JsfUtils;

public class MinisterioPublicoConverter implements Converter {

	private MinisterioPublicoService ministerioPublicoService;

	public MinisterioPublicoConverter() {
		this.ministerioPublicoService = JsfUtils
				.getBeanFromWebApplicationContext(MinisterioPublicoService.class);
	}

	@Override
	public Object getAsObject(FacesContext context, UIComponent component, String value) {

		if (value.trim().isEmpty()) {
			return null;
		} else {
			try {
				int id = Integer.parseInt(value);

				return this.ministerioPublicoService.loadById(id);

			} catch (Exception e) {

				Map<String, String> msg = JsfUtils.getMessagesFromDefaultBundle(context,
						Constantes.ERROR_CONVERTING_MP_KEY);

				JsfUtils.addNewFacesMessage(context, component.getClientId(),
						FacesMessage.SEVERITY_ERROR, msg.get("summary"), msg.get("detail"));
			}
		}
		return null;
	}

	@Override
	public String getAsString(FacesContext context, UIComponent component, Object value) {
		if (value == null || value.equals("")) {
			return "";
		} else {
			return String.valueOf(((MinisterioPublico) value).getId());
		}
	}

	public MinisterioPublicoService getMinisterioPublicoService() {
		return ministerioPublicoService;
	}

	public void setMinisterioPublicoService(MinisterioPublicoService ministerioPublicoService) {
		this.ministerioPublicoService = ministerioPublicoService;
	}

}
