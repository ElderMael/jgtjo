package mx.gob.jgtjo.apps.schedule.web.view.beans.model;

import java.io.Serializable;
import java.util.LinkedList;
import java.util.List;

import javax.faces.model.ListDataModel;

import org.primefaces.model.SelectableDataModel;

import mx.gob.jgtjo.apps.schedule.model.AudienciaOral;

public class AudienciaOralSelectableDataModel extends ListDataModel<AudienciaOral> implements
		SelectableDataModel<AudienciaOral>, Serializable {

	private static final long serialVersionUID = 1L;

	public AudienciaOralSelectableDataModel() {
		this(new LinkedList<AudienciaOral>());
	}

	public AudienciaOralSelectableDataModel(List<AudienciaOral> data) {
		super(data);
	}

	@Override
	public Object getRowKey(AudienciaOral object) {

		return this.createKey(object);

	}

	@Override
	@SuppressWarnings("unchecked")
	public AudienciaOral getRowData(String rowKey) {

		List<AudienciaOral> data = (List<AudienciaOral>) getWrappedData();

		for (AudienciaOral audiencia : data) {

			Object dataKey = this.createKey(audiencia);

			if (dataKey.toString().equals(rowKey)) {
				return audiencia;
			}

		}

		return null;
	}

	private Object createKey(AudienciaOral object) {

		if (object.getCausaPenal().getNumeroCausa() == null)
			return "empty";

		return object.getCausaPenal().getNumeroCausa() + "/"
				+ object.getCausaPenal().getAnnioCausa() + "/" + object.getFechaInicio().toString();
	}

}
