package mx.gob.jgtjo.apps.schedule.web.conversation;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import javax.servlet.http.HttpServletRequest;

import org.hibernate.Session;
import org.hibernate.engine.SessionFactoryImplementor;

public class ConversationManager {

	private static Map<UUID, Session> conversationMap = new HashMap<UUID, Session>();

	private static Map<HttpServletRequest, UUID> requestMap = new HashMap<HttpServletRequest, UUID>();

	private static Session defaultSession = null;

	private static SessionFactoryImplementor factory = null;

	public static UUID createNewConversation() {

		UUID conversationId = UUID.randomUUID();

		Session sessionForConversation = factory.openSession();

		conversationMap.put(conversationId, sessionForConversation);

		return conversationId;
	}

	public static Session getSessionFromConversation(UUID conversationId) {
		return conversationMap.get(conversationId);
	}

	public static void bindConversationToRequest(UUID conversationId, HttpServletRequest request) {
		requestMap.put(request, conversationId);
	}

	public static UUID getConversationForRequest(HttpServletRequest request) {
		return requestMap.get(request);
	}

	public static Session getSessionForRequest(HttpServletRequest request) {
		UUID conversationId = ConversationManager.getConversationForRequest(request);
		return conversationMap.get(conversationId);
	}

	public static UUID unbindConversationToRequest(HttpServletRequest request) {
		UUID conversationId = requestMap.remove(request);
		return conversationId;
	}

	public static void endConversation(UUID conversationId) {
		Session session = conversationMap.remove(conversationId);
		session.close();
	}

	public static void endDefaultConversation() {
		defaultSession.close();
		defaultSession = null;
	}

	public static Session getDefaultConversationSession() {
		return defaultSession;
	}

	public static void startDefaultConversation(Session defaultSession) {
		ConversationManager.defaultSession = defaultSession;
	}

	public static boolean isConversationOpen(UUID conversationID) {
		return conversationMap.containsKey(conversationID);
	}

	public static void restartConversation(UUID conversationId) {
		Session sessionForConversation = factory.openSession();
		conversationMap.put(conversationId, sessionForConversation);
	}

	public static SessionFactoryImplementor getFactory() {
		return factory;
	}

	public static void setFactory(SessionFactoryImplementor factory) {
		ConversationManager.factory = factory;
	}

}
