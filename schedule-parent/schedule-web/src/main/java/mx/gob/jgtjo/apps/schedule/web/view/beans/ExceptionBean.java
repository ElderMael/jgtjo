package mx.gob.jgtjo.apps.schedule.web.view.beans;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;

@ManagedBean(name = "ExceptionBean")
@ViewScoped
public class ExceptionBean {

	private Throwable exception;

	public ExceptionBean() {

		this.exception = FacesContext.getCurrentInstance().getExceptionHandler()
				.getHandledExceptionQueuedEvent().getContext().getException();

	}

	public Throwable getException() {
		return exception;
	}

	public void setException(Throwable exception) {
		this.exception = exception;
	}

}
