package mx.gob.jgtjo.apps.schedule.model;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
import javax.persistence.Version;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ReflectionToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import org.hibernate.search.annotations.DocumentId;
import org.hibernate.search.annotations.Field;
import org.hibernate.search.annotations.Indexed;

//Parte Formal
@Entity
@Table(name = "ministerios_publicos")
@Indexed(index = "ministerio_publico")
public class MinisterioPublico implements Serializable {

	private static final long serialVersionUID = 1L;

	@Basic
	@Column(name = "apellido_materno")
	@Field
	private String apellidoMaterno;

	@Basic
	@Column(name = "apellido_paterno")
	@Field
	private String apellidoPaterno;

	@Basic
	@Column(name = "nombres")
	@Field
	private String nombres;

	@Basic
	@Column(name = "cedula_profesional")
	private String cedulaProfesional;

	@ManyToMany(mappedBy = "ministeriosPublicos")
	private List<AudienciaOral> audienciasOrales;

	@ManyToMany(mappedBy = "ministeriosPublicos")
	private List<CausaPenal> causasPenales;

	@Id
	@GeneratedValue(strategy = GenerationType.TABLE)
	@Column(name = "id")
	@DocumentId
	private Integer id;

	@Version
	@Column(name = "opt_lock")
	private Integer version;

	public MinisterioPublico() {

	}

	@Override
	public boolean equals(Object obj) {

		if (obj == null) {
			return false;
		}
		if (obj == this) {
			return true;
		}
		if (!(obj instanceof MinisterioPublico)) {
			return false;
		}

		MinisterioPublico o = (MinisterioPublico) obj;

		return new EqualsBuilder().append(this.nombres, o.nombres)
				.append(this.apellidoPaterno, o.apellidoPaterno)
				.append(this.apellidoMaterno, o.apellidoMaterno).isEquals();
	}

	public String getApellidoMaterno() {
		return this.apellidoMaterno;
	}

	public String getApellidoPaterno() {
		return this.apellidoPaterno;
	}

	public List<AudienciaOral> getAudienciasOrales() {
		return this.audienciasOrales;
	}

	public List<CausaPenal> getCausasPenales() {
		return this.causasPenales;
	}

	public Integer getId() {
		return this.id;
	}

	public String getNombres() {
		return this.nombres;
	}

	public Integer getVersion() {
		return this.version;
	}

	@Override
	public int hashCode() {
		return new HashCodeBuilder(27, 137).append(this.nombres).append(this.apellidoPaterno)
				.append(this.apellidoMaterno).toHashCode();
	}

	public void setApellidoMaterno(String apellidoMaterno) {
		this.apellidoMaterno = apellidoMaterno;
	}

	public void setApellidoPaterno(String apellidoPaterno) {
		this.apellidoPaterno = apellidoPaterno;
	}

	public void setAudienciasOrales(List<AudienciaOral> audienciasOrales) {
		this.audienciasOrales = audienciasOrales;
	}

	public void setCausasPenales(List<CausaPenal> causasPenales) {
		this.causasPenales = causasPenales;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public void setNombres(String nombres) {
		this.nombres = nombres;
	}

	public void setVersion(Integer version) {
		this.version = version;
	}

	@Override
	public String toString() {
		return ReflectionToStringBuilder.toString(this, ToStringStyle.SHORT_PREFIX_STYLE);
	}

	public String getCedulaProfesional() {
		return cedulaProfesional;
	}

	public void setCedulaProfesional(String cedulaProfesional) {
		this.cedulaProfesional = cedulaProfesional;
	}
}
