package mx.gob.jgtjo.apps.schedule.model;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinTable;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Version;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ReflectionToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import org.hibernate.search.annotations.DocumentId;
import org.hibernate.search.annotations.Field;
import org.hibernate.search.annotations.Indexed;

@Entity
@Indexed(index = "partes_materiales")
@Table(name = "partes_materiales")
public class ParteMaterial implements Serializable {

	private static final long serialVersionUID = 1L;

	@Basic
	@Column(name = "alias")
	private String alias;

	@Basic
	@Column(name = "apellido_materno")
	@Field
	private String apellidoMaterno;

	@Basic
	@Column(name = "apellido_paterno")
	@Field
	private String apellidoPaterno;

	@Basic
	@Column(name = "descripcion_persona_moral")
	private String descripcionPersonaMoral;

	@ElementCollection
	@JoinTable(name = "partes_materiales_domicilios")
	private List<Domicilio> domicilios;

	@Basic
	@Column(name = "edad")
	private Integer edad;

	@Enumerated(EnumType.STRING)
	@Column(name = "estado_civil")
	private EstadoCivil estadoCivil;

	@Temporal(TemporalType.DATE)
	@Column(name = "fecha_nacimiento")
	private Date fechaNacimiento;

	@Enumerated(EnumType.STRING)
	@Column(name = "genero")
	private Genero genero;

	@Id
	@GeneratedValue(strategy = GenerationType.TABLE)
	@Column(name = "id")
	@DocumentId
	private Integer id;

	@Basic
	@Column(name = "identificacion_personal")
	private String identificacion;

	@Basic
	@Column(name = "idioma")
	private String idioma;

	@Basic
	@Column(name = "lugar_nacimiento")
	private String lugarNacimiento;

	@Basic
	@Column(name = "nombres")
	@Field
	private String nombres;

	@Basic
	@Column(name = "profesion_oficio")
	private String profesionOrOficio;

	@Version
	@Column(name = "opt_lock")
	private Integer version;

	public ParteMaterial() {

	}

	public ParteMaterial(String alias, String apellidoMaterno, String apellidoPaterno,
			String descripcionPersonaMoral, List<Domicilio> domicilios, Integer edad,
			EstadoCivil estadoCivil, Date fechaNacimiento, Genero genero, Integer id,
			String identificacion, String idioma, String lugarNacimiento, String nombres,
			String profesionOrOficio, Integer version) {
		super();
		this.alias = alias;
		this.apellidoMaterno = apellidoMaterno;
		this.apellidoPaterno = apellidoPaterno;
		this.descripcionPersonaMoral = descripcionPersonaMoral;
		this.domicilios = domicilios;
		this.edad = edad;
		this.estadoCivil = estadoCivil;
		this.fechaNacimiento = fechaNacimiento;
		this.genero = genero;
		this.id = id;
		this.identificacion = identificacion;
		this.idioma = idioma;
		this.lugarNacimiento = lugarNacimiento;
		this.nombres = nombres;
		this.profesionOrOficio = profesionOrOficio;
		this.version = version;
	}

	@Override
	public boolean equals(Object obj) {

		if (obj == null) {
			return false;
		}
		if (obj == this) {
			return true;
		}
		if (!(obj instanceof ParteMaterial)) {
			return false;
		}

		ParteMaterial o = (ParteMaterial) obj;

		return new EqualsBuilder().append(this.nombres, o.nombres)
				.append(this.apellidoPaterno, o.apellidoPaterno)
				.append(this.apellidoMaterno, o.apellidoMaterno)
				.append(this.descripcionPersonaMoral, o.descripcionPersonaMoral).isEquals();
	}

	public String getAlias() {
		return this.alias;
	}

	public String getApellidoMaterno() {
		return this.apellidoMaterno;
	}

	public String getApellidoPaterno() {
		return this.apellidoPaterno;
	}

	public String getDescripcionPersonaMoral() {
		return this.descripcionPersonaMoral;
	}

	public List<Domicilio> getDomicilios() {
		return this.domicilios;
	}

	public Integer getEdad() {
		return this.edad;
	}

	public EstadoCivil getEstadoCivil() {
		return this.estadoCivil;
	}

	public Date getFechaNacimiento() {
		return this.fechaNacimiento;
	}

	public Genero getGenero() {
		return this.genero;
	}

	public Integer getId() {
		return this.id;
	}

	public String getIdentificacion() {
		return this.identificacion;
	}

	public String getIdioma() {
		return this.idioma;
	}

	public String getLugarNacimiento() {
		return this.lugarNacimiento;
	}

	public String getNombres() {
		return this.nombres;
	}

	public String getProfesionOrOficio() {
		return this.profesionOrOficio;
	}

	public Integer getVersion() {
		return this.version;
	}

	@Override
	public int hashCode() {
		return new HashCodeBuilder(31, 147).append(this.nombres).append(this.apellidoPaterno)
				.append(this.apellidoMaterno).append(this.descripcionPersonaMoral).toHashCode();
	}

	public void setAlias(String alias) {
		this.alias = alias;
	}

	public void setApellidoMaterno(String apellidoMaterno) {
		this.apellidoMaterno = apellidoMaterno;
	}

	public void setApellidoPaterno(String apellidoPaterno) {
		this.apellidoPaterno = apellidoPaterno;
	}

	public void setDescripcionPersonaMoral(String descripcionPersonaMoral) {
		this.descripcionPersonaMoral = descripcionPersonaMoral;
	}

	public void setDomicilios(List<Domicilio> domicilios) {
		this.domicilios = domicilios;
	}

	public void setEdad(Integer edad) {
		this.edad = edad;
	}

	public void setEstadoCivil(EstadoCivil estadoCivil) {
		this.estadoCivil = estadoCivil;
	}

	public void setFechaNacimiento(Date fechaNacimiento) {
		this.fechaNacimiento = fechaNacimiento;
	}

	public void setGenero(Genero genero) {
		this.genero = genero;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public void setIdentificacion(String identificacion) {
		this.identificacion = identificacion;
	}

	public void setIdioma(String idioma) {
		this.idioma = idioma;
	}

	public void setLugarNacimiento(String lugarNacimiento) {
		this.lugarNacimiento = lugarNacimiento;
	}

	public void setNombres(String nombres) {
		this.nombres = nombres;
	}

	public void setProfesionOrOficio(String profesionOrOficio) {
		this.profesionOrOficio = profesionOrOficio;
	}

	public void setVersion(Integer version) {
		this.version = version;
	}

	@Override
	public String toString() {
		return ReflectionToStringBuilder.toString(this, ToStringStyle.SHORT_PREFIX_STYLE);
	}

}
