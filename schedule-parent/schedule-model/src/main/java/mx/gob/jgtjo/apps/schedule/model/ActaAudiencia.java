package mx.gob.jgtjo.apps.schedule.model;

import java.util.List;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinTable;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Version;

import org.hibernate.search.annotations.DocumentId;
import org.hibernate.search.annotations.Indexed;

@Entity
@Table(name = "acta_audiencia")
@Indexed(index = "actas_audiencia")
public class ActaAudiencia {

     @Id
     @GeneratedValue(strategy = GenerationType.TABLE)
     @DocumentId
     @Column(name = "id")
     private Integer id;

     @Version
     @Column(name = "opt_lock")
     private Integer version;

     @OneToOne
     private AudienciaOral audienciaOral;

     @Basic
     @Column(name = "administrativo_acta")
     private AdministrativoActa administrativoActa;

     @ElementCollection
     @JoinTable(name = "acta_audiencia_datos_relevantes")
     private List<String> datosRelevantes;

     @Basic
     @Column(name = "libro_orden")
     private Boolean seLibroOrdenDeAprehension = false;

     @Basic
     @Column(name = "formulo_imputacion")
     private Boolean seFormuloImputacion = false;

     @Basic
     @Column(name = "vinculo_a_proceso")
     private Boolean seVinculoAlProceso = false;

     @Basic
     @Column(name = "impuso_medidas")
     private Boolean seImpusoMedidasCautelares = false;

     @Basic
     @Column(name = "concilio")
     private Boolean seConcilio = false;

     @Basic
     @Column(name = "suspendio")
     Boolean seSuspendio = false;

}
