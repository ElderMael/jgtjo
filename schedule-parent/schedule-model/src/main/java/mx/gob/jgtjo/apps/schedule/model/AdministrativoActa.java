package mx.gob.jgtjo.apps.schedule.model;

import java.io.Serializable;

import javax.persistence.Basic;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.hibernate.search.annotations.DocumentId;
import org.hibernate.search.annotations.Indexed;

@Entity
@Table(name = "administrativos_acta")
@Indexed(index = "administrativo_acta")
public class AdministrativoActa implements Serializable {

     private static final long serialVersionUID = 1L;

     @Id
     @GeneratedValue(strategy = GenerationType.TABLE)
     @DocumentId
     private Integer id;

     @OneToOne
     private User user;

     @Basic
     private String nombres;

     @Basic
     private String apellidoPaterno;

     @Basic
     private String apellidoMaterno;

     public AdministrativoActa() {

     }

}
