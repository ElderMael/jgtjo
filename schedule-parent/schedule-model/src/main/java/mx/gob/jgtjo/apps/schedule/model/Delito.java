package mx.gob.jgtjo.apps.schedule.model;

import java.io.Serializable;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Version;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ReflectionToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

@Entity
@Table(name = "delitos")
public class Delito implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.TABLE)
	@Column(name = "id")
	private Integer id;

	@Basic
	@Column(name = "nombre")
	private String nombre;

	@Version
	@Column(name = "opt_lock")
	private Integer version;

	public Delito() {
	}

	public Delito(Integer id, Integer version, String nombre) {
		this.id = id;
		this.version = version;
		this.nombre = nombre;

	}

	@Override
	public boolean equals(Object obj) {

		if (obj == null) {
			return false;
		}
		if (obj == this) {
			return true;
		}
		if (!(obj instanceof Delito)) {
			return false;
		}

		Delito o = (Delito) obj;

		return new EqualsBuilder().append(this.nombre, o.nombre).isEquals();
	}

	public Integer getId() {
		return this.id;
	}

	public String getNombre() {
		return this.nombre;
	}

	public Integer getVersion() {
		return this.version;
	}

	@Override
	public int hashCode() {
		return new HashCodeBuilder(19, 115).append(this.nombre).toHashCode();
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public void setVersion(Integer version) {
		this.version = version;
	}

	@Override
	public String toString() {
		return ReflectionToStringBuilder.toString(this, ToStringStyle.SHORT_PREFIX_STYLE);
	}
}
