package mx.gob.jgtjo.apps.schedule.model;

import java.io.Serializable;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.Table;

import org.apache.commons.lang3.builder.ReflectionToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import org.hibernate.search.annotations.Field;

@Embeddable
@Table(name = "domicilios")
public class Domicilio implements Serializable {

	private static final long serialVersionUID = 1L;

	@Basic
	@Field
	@Column(name = "calle")
	private String calle;

	@Basic
	@Field
	@Column(name = "numero")
	private String numero;

	@Basic
	@Field
	@Column(name = "codigo_postal")
	private Integer codigoPostal;

	@Basic
	@Field
	@Column(name = "colonia")
	private String colonia;

	@Basic
	@Field
	@Column(name = "estado")
	private String estado;

	@Basic
	@Field
	@Column(name = "municipio")
	private String municipio;

	@Basic
	@Field
	@Column(name = "pais")
	private String pais;

	public Domicilio() {

	}

	public Domicilio(String calle, Integer codigoPostal, String colonia, String estado,
			String municipio, String pais) {
		super();
		this.calle = calle;
		this.codigoPostal = codigoPostal;
		this.colonia = colonia;
		this.estado = estado;
		this.municipio = municipio;
		this.pais = pais;
	}

	@Override
	public String toString() {
		return ReflectionToStringBuilder.toString(this, ToStringStyle.SHORT_PREFIX_STYLE);
	}

	public String getCalle() {
		return this.calle;
	}

	public Integer getCodigoPostal() {
		return this.codigoPostal;
	}

	public String getColonia() {
		return this.colonia;
	}

	public String getEstado() {
		return this.estado;
	}

	public String getMunicipio() {
		return this.municipio;
	}

	public String getPais() {
		return this.pais;
	}

	public void setCalle(String calle) {
		this.calle = calle;
	}

	public void setCodigoPostal(Integer codigoPostal) {
		this.codigoPostal = codigoPostal;
	}

	public void setColonia(String colonia) {
		this.colonia = colonia;
	}

	public void setEstado(String estado) {
		this.estado = estado;
	}

	public void setMunicipio(String municipio) {
		this.municipio = municipio;
	}

	public void setPais(String pais) {
		this.pais = pais;
	}

	public String getNumero() {
		return numero;
	}

	public void setNumero(String numero) {
		this.numero = numero;
	}

}
