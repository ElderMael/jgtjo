package mx.gob.jgtjo.apps.schedule.model;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.OrderBy;
import javax.persistence.Table;
import javax.persistence.Version;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ReflectionToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import org.hibernate.search.annotations.ContainedIn;
import org.hibernate.search.annotations.DocumentId;
import org.hibernate.search.annotations.Field;
import org.hibernate.search.annotations.Indexed;
import org.hibernate.search.annotations.IndexedEmbedded;

@Entity
@Indexed(index = "causa_penal")
@Table(name = "causas_penales")
public class CausaPenal implements Serializable {

     private static final long serialVersionUID = 1L;

     @Id
     @GeneratedValue(strategy = GenerationType.TABLE)
     @Column(name = "id")
     @DocumentId
     private Integer id;

     @Version
     @Column(name = "opt_lock")
     private Integer version;

     @Basic
     @Column(name = "anio_causa")
     @Field
     private Integer annioCausa;

     @OneToMany(mappedBy = "causaPenal")
     @ContainedIn
     @OrderBy
     private List<AudienciaOral> audienciasOrales;

     @ManyToMany(cascade = CascadeType.ALL)
     private List<DefensorPenal> defensoresPenales;

     @OneToMany(cascade = CascadeType.ALL)
     private List<DelitoConfigurado> delitosConfigurados;

     @ManyToMany(cascade = { CascadeType.ALL })
     @JoinTable(name = "causapenal_imputados")
     @IndexedEmbedded(depth = 1)
     private List<ParteMaterial> imputados;

     @ManyToMany(cascade = CascadeType.ALL)
     private List<MinisterioPublico> ministeriosPublicos;

     @Basic
     @Column(name = "numero_causa")
     @Field
     private Integer numeroCausa;

     @ManyToMany(cascade = CascadeType.ALL)
     @JoinTable(name = "causapenal_victimas")
     @IndexedEmbedded(depth = 1)
     private List<ParteMaterial> victimas;

     public CausaPenal() {
     }

     public CausaPenal(Integer id, Integer version, Integer numeroCausa,
               Integer annioCausa, List<DelitoConfigurado> delitosConfigurados,
               List<ParteMaterial> victimas, List<ParteMaterial> imputados,
               List<MinisterioPublico> ministeriosPublicos,
               List<DefensorPenal> defensoresPenales,
               List<AudienciaOral> audienciasOrales) {
          super();
          this.id = id;
          this.version = version;
          this.numeroCausa = numeroCausa;
          this.annioCausa = annioCausa;
          this.delitosConfigurados = delitosConfigurados;
          this.victimas = victimas;
          this.imputados = imputados;
          this.ministeriosPublicos = ministeriosPublicos;
          this.defensoresPenales = defensoresPenales;
          this.audienciasOrales = audienciasOrales;
     }

     @Override
     public boolean equals(Object obj) {
          if ( obj == null ) {
               return false;
          }
          if ( obj == this ) {
               return true;
          }
          if ( !(obj instanceof CausaPenal) ) {
               return false;
          }

          CausaPenal o = (CausaPenal) obj;

          return new EqualsBuilder().append(this.numeroCausa, o.numeroCausa)
                    .append(this.annioCausa, o.annioCausa).isEquals();

     }

     public Integer getAnnioCausa() {
          return this.annioCausa;
     }

     public List<AudienciaOral> getAudienciasOrales() {
          return this.audienciasOrales;
     }

     public List<DefensorPenal> getDefensoresPenales() {
          return this.defensoresPenales;
     }

     public List<DelitoConfigurado> getDelitosConfigurados() {
          return this.delitosConfigurados;
     }

     public Integer getId() {
          return this.id;
     }

     public List<ParteMaterial> getImputados() {
          return this.imputados;
     }

     public List<MinisterioPublico> getMinisteriosPublicos() {
          return this.ministeriosPublicos;
     }

     public Integer getNumeroCausa() {
          return this.numeroCausa;
     }

     public Integer getVersion() {
          return this.version;
     }

     public List<ParteMaterial> getVictimas() {
          return this.victimas;
     }

     @Override
     public int hashCode() {

          return new HashCodeBuilder(13, 33).append(this.numeroCausa)
                    .append(this.annioCausa).toHashCode();
     }

     public void setAnnioCausa(Integer annioCausa) {
          this.annioCausa = annioCausa;
     }

     public void setAudienciasOrales(List<AudienciaOral> audienciasOrales) {
          this.audienciasOrales = audienciasOrales;
     }

     public void setDefensoresPenales(List<DefensorPenal> defensoresPenales) {
          this.defensoresPenales = defensoresPenales;
     }

     public void setDelitosConfigurados(
               List<DelitoConfigurado> delitosConfigurados) {
          this.delitosConfigurados = delitosConfigurados;
     }

     public void setId(Integer id) {
          this.id = id;
     }

     public void setImputados(List<ParteMaterial> imputados) {
          this.imputados = imputados;
     }

     public void setMinisteriosPublicos(
               List<MinisterioPublico> ministeriosPublicos) {
          this.ministeriosPublicos = ministeriosPublicos;
     }

     public void setNumeroCausa(Integer numeroCausa) {
          this.numeroCausa = numeroCausa;
     }

     public void setVersion(Integer version) {
          this.version = version;
     }

     public void setVictimas(List<ParteMaterial> victimas) {
          this.victimas = victimas;
     }

     @Override
     public String toString() {
          return ReflectionToStringBuilder.toString(this,
                    ToStringStyle.SHORT_PREFIX_STYLE);
     }

}
