package mx.gob.jgtjo.apps.schedule.model;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Version;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ReflectionToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import org.hibernate.search.annotations.DateBridge;
import org.hibernate.search.annotations.DocumentId;
import org.hibernate.search.annotations.Indexed;
import org.hibernate.search.annotations.IndexedEmbedded;
import org.hibernate.search.annotations.Resolution;

@Entity
@Table(name = "audiencias_orales")
@Indexed(index = "audiencias_orales")
public class AudienciaOral implements Serializable {

     private static final long serialVersionUID = 1L;

     @ManyToOne(cascade = CascadeType.ALL)
     @JoinColumn(name = "causa_fk")
     @IndexedEmbedded(depth = 2)
     private CausaPenal causaPenal;

     @OneToOne
     private ActaAudiencia actaAudiencia;

     @ManyToMany
     private List<DefensorPenal> defensoresPenales;

     @ElementCollection
     @JoinTable(name = "audiencias_orales_eventos")
     private List<EventoAudiencia> eventos;

     @Temporal(TemporalType.TIMESTAMP)
     @Column(name = "fecha_inicio")
     @DateBridge(resolution = Resolution.HOUR)
     private Date fechaInicio;

     @Temporal(TemporalType.TIMESTAMP)
     @Column(name = "fecha_fin")
     private Date fechaFinAprox;

     @Id
     @GeneratedValue(strategy = GenerationType.TABLE)
     @Column(name = "id")
     @DocumentId
     private Integer id;

     @ManyToMany
     private List<Juez> jueces;

     @ManyToMany
     private List<MinisterioPublico> ministeriosPublicos;

     @ManyToOne
     @JoinColumn(name = "sala_fk")
     @IndexedEmbedded(depth = 1)
     private Sala sala;

     @ManyToOne
     @JoinColumn(name = "tipo_audiencia_fk")
     private TipoAudiencia tipoAudiencia;

     @Version
     @Column(name = "opt_lock")
     private Integer version;

     @Basic
     @Column(name = "numero_audiencia")
     private Integer numeroAudiencia;

     @Basic
     @Column(name = "cancelada")
     private Boolean seCancelo = false;

     public AudienciaOral() {
     }

     public AudienciaOral(Integer id, Integer version, CausaPenal causaPenal,
               List<EventoAudiencia> eventos, Date fechaInicio,
               Date fechaFinAprox, Sala sala, TipoAudiencia tipoAudiencia,
               List<Juez> jueces, List<MinisterioPublico> ministeriosPublicos,
               List<DefensorPenal> defensoresPenales) {
          super();
          this.id = id;
          this.version = version;
          this.causaPenal = causaPenal;
          this.eventos = eventos;
          this.fechaInicio = fechaInicio;
          this.sala = sala;
          this.tipoAudiencia = tipoAudiencia;
          this.jueces = jueces;
          this.ministeriosPublicos = ministeriosPublicos;
          this.defensoresPenales = defensoresPenales;
          this.fechaFinAprox = fechaFinAprox;
     }

     @Override
     public boolean equals(Object obj) {
          if ( obj == null ) {
               return false;
          }
          if ( obj == this ) {
               return true;
          }
          if ( !(obj instanceof AudienciaOral) ) {
               return false;
          }

          AudienciaOral o = (AudienciaOral) obj;

          return new EqualsBuilder().append(this.causaPenal, o.causaPenal)
                    .append(this.fechaInicio, o.fechaInicio)
                    .append(this.fechaFinAprox, o.fechaFinAprox)
                    .append(this.tipoAudiencia, o.tipoAudiencia).isEquals();
     }

     public CausaPenal getCausaPenal() {
          return this.causaPenal;
     }

     public List<DefensorPenal> getDefensoresPenales() {
          return this.defensoresPenales;
     }

     public List<EventoAudiencia> getEventos() {
          return this.eventos;
     }

     public Integer getId() {
          return this.id;
     }

     public List<Juez> getJueces() {
          return this.jueces;
     }

     public List<MinisterioPublico> getMinisteriosPublicos() {
          return this.ministeriosPublicos;
     }

     public Sala getSala() {
          return this.sala;
     }

     public TipoAudiencia getTipoAudiencia() {
          return this.tipoAudiencia;
     }

     public Integer getVersion() {
          return this.version;
     }

     @Override
     public int hashCode() {
          return new HashCodeBuilder(11, 43).append(this.causaPenal)
                    .append(this.fechaInicio).append(this.fechaFinAprox)
                    .append(this.tipoAudiencia).toHashCode();

     }

     public void setCausaPenal(CausaPenal causaPenal) {
          this.causaPenal = causaPenal;
     }

     public void setDefensoresPenales(List<DefensorPenal> defensoresPenales) {
          this.defensoresPenales = defensoresPenales;
     }

     public void setEventos(List<EventoAudiencia> eventos) {
          this.eventos = eventos;
     }

     public void setId(Integer id) {
          this.id = id;
     }

     public void setJueces(List<Juez> jueces) {
          this.jueces = jueces;
     }

     public void setMinisteriosPublicos(
               List<MinisterioPublico> ministeriosPublicos) {
          this.ministeriosPublicos = ministeriosPublicos;
     }

     public void setSala(Sala sala) {
          this.sala = sala;
     }

     public void setTipoAudiencia(TipoAudiencia tipoAudiencia) {
          this.tipoAudiencia = tipoAudiencia;
     }

     public void setVersion(Integer version) {
          this.version = version;
     }

     @Override
     public String toString() {
          return ReflectionToStringBuilder.toString(this,
                    ToStringStyle.SHORT_PREFIX_STYLE);
     }

     public Date getFechaFinAprox() {
          return fechaFinAprox;
     }

     public void setFechaFinAprox(Date fechaFinAprox) {
          this.fechaFinAprox = fechaFinAprox;
     }

     public Date getFechaInicio() {
          return fechaInicio;
     }

     public void setFechaInicio(Date fechaInicio) {
          this.fechaInicio = fechaInicio;
     }

     public Integer getNumeroAudiencia() {
          return numeroAudiencia;
     }

     public void setNumeroAudiencia(Integer numeroAudiencia) {
          this.numeroAudiencia = numeroAudiencia;
     }

     public Boolean getSeCancelo() {
          return seCancelo;
     }

     public void setSeCancelo(Boolean seCancelo) {
          this.seCancelo = seCancelo;
     }

     public ActaAudiencia getActaAudiencia() {
          return actaAudiencia;
     }

     public void setActaAudiencia(ActaAudiencia actaAudiencia) {
          this.actaAudiencia = actaAudiencia;
     }

}
