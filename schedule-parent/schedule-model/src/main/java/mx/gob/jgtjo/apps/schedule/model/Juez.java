package mx.gob.jgtjo.apps.schedule.model;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
import javax.persistence.Version;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ReflectionToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

@Entity
@Table(name = "jueces")
public class Juez implements Serializable {

	private static final long serialVersionUID = 1L;

	@Basic
	@Column(name = "apellido_paterno")
	private String apellidoMaterno;

	@Basic
	@Column(name = "apellido_materno")
	private String apellidoPaterno;

	@ManyToMany(mappedBy = "jueces")
	private List<AudienciaOral> audienciasOrales;

	@Id
	@GeneratedValue(strategy = GenerationType.TABLE)
	@Column(name = "id")
	private Integer id;

	@Basic
	@Column(name = "nombres")
	private String nombres;

	@Version
	@Column(name = "opt_lock")
	private Integer version;

	@Basic
	@Column(name = "activo")
	private Boolean activo;

	@Enumerated(EnumType.STRING)
	@Column(name = "genero")
	private Genero genero;

	public Juez() {

	}

	public Juez(Integer id, Integer version, String nombres, String apellidoPaterno,
			String apellidoMaterno, Boolean activo, List<AudienciaOral> audienciasOrales) {
		super();
		this.id = id;
		this.version = version;
		this.nombres = nombres;
		this.apellidoPaterno = apellidoPaterno;
		this.apellidoMaterno = apellidoMaterno;
		this.audienciasOrales = audienciasOrales;
		this.activo = activo;
	}

	@Override
	public boolean equals(Object obj) {

		if (obj == null) {
			return false;
		}
		if (obj == this) {
			return true;
		}
		if (!(obj instanceof Juez)) {
			return false;
		}

		Juez o = (Juez) obj;

		return new EqualsBuilder().append(this.nombres, o.nombres)
				.append(this.apellidoPaterno, o.apellidoPaterno)
				.append(this.apellidoMaterno, o.apellidoMaterno).isEquals();

	}

	public String getApellidoMaterno() {
		return this.apellidoMaterno;
	}

	public String getApellidoPaterno() {
		return this.apellidoPaterno;
	}

	public List<AudienciaOral> getAudienciasOrales() {
		return this.audienciasOrales;
	}

	public Integer getId() {
		return this.id;
	}

	public String getNombres() {
		return this.nombres;
	}

	public Integer getVersion() {
		return this.version;
	}

	@Override
	public int hashCode() {

		return new HashCodeBuilder(25, 75).append(this.nombres).append(this.apellidoPaterno)
				.append(this.apellidoMaterno).toHashCode();
	}

	public void setApellidoMaterno(String apellidoMaterno) {
		this.apellidoMaterno = apellidoMaterno;
	}

	public void setApellidoPaterno(String apellidoPaterno) {
		this.apellidoPaterno = apellidoPaterno;
	}

	public void setAudienciasOrales(List<AudienciaOral> audienciasOrales) {
		this.audienciasOrales = audienciasOrales;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public void setNombres(String nombres) {
		this.nombres = nombres;
	}

	public void setVersion(Integer version) {
		this.version = version;
	}

	@Override
	public String toString() {
		return ReflectionToStringBuilder.toString(this, ToStringStyle.SHORT_PREFIX_STYLE);
	}

	public Boolean getActivo() {
		return activo;
	}

	public void setActivo(Boolean activo) {
		this.activo = activo;
	}

	public Genero getGenero() {
		return genero;
	}

	public void setGenero(Genero genero) {
		this.genero = genero;
	}

}
