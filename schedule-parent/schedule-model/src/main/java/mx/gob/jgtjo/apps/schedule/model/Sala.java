package mx.gob.jgtjo.apps.schedule.model;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Version;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ReflectionToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

@Entity
@Table(name = "salas")
public class Sala implements Serializable {

	private static final long serialVersionUID = 1L;

	@OneToMany(mappedBy = "sala")
	private List<AudienciaOral> audienciasOrales;

	@Id
	@GeneratedValue(strategy = GenerationType.TABLE)
	@Column(name = "id")
	private Integer id;

	@Basic
	@Column(name = "locacion")
	private String locacion;

	@Basic
	@Column(name = "nombre", length = 15)
	private String nombre;

	@Version
	@Column(name = "opt_lock")
	private Integer version;

	public Sala() {

	}

	public Sala(Integer id, Integer version, String nombre, String locacion,
			List<AudienciaOral> audienciasOrales) {
		super();
		this.id = id;
		this.version = version;
		this.nombre = nombre;
		this.locacion = locacion;
		this.audienciasOrales = audienciasOrales;
	}

	@Override
	public boolean equals(Object obj) {

		if (obj == null) {
			return false;
		}
		if (obj == this) {
			return true;
		}
		if (!(obj instanceof Sala)) {
			return false;
		}

		Sala o = (Sala) obj;

		return new EqualsBuilder().append(this.nombre, o.nombre).append(this.locacion, o.locacion)
				.isEquals();
	}

	public List<AudienciaOral> getAudienciasOrales() {
		return this.audienciasOrales;
	}

	public Integer getId() {
		return this.id;
	}

	public String getLocacion() {
		return this.locacion;
	}

	public String getNombre() {
		return this.nombre;
	}

	public Integer getVersion() {
		return this.version;
	}

	@Override
	public int hashCode() {
		return new HashCodeBuilder(41, 551).append(this.nombre).append(this.locacion).toHashCode();
	}

	public void setAudienciasOrales(List<AudienciaOral> audienciasOrales) {
		this.audienciasOrales = audienciasOrales;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public void setLocacion(String locacion) {
		this.locacion = locacion;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public void setVersion(Integer version) {
		this.version = version;
	}

	@Override
	public String toString() {
		return ReflectionToStringBuilder.toString(this, ToStringStyle.SHORT_PREFIX_STYLE);
	}
}
