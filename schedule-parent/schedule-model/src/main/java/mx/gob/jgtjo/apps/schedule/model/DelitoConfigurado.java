package mx.gob.jgtjo.apps.schedule.model;

import java.io.Serializable;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Version;

import org.apache.commons.lang3.builder.ReflectionToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

@Entity
@Table(name = "delitos_configurados")
public class DelitoConfigurado implements Serializable {

     private static final long serialVersionUID = 1L;

     @Basic
     @Column(name = "es_calificado")
     private Boolean calificado;

     @Basic
     @Column(name = "es_culposo")
     private Boolean culposo;

     @Basic
     @Column(name = "es_tentativa")
     private Boolean tentativa;

     @ManyToOne
     private Delito delito;

     @Id
     @GeneratedValue(strategy = GenerationType.TABLE)
     @Column(name = "id")
     private Integer id;

     @Version
     @Column(name = "opt_lock")
     private Integer version;

     public DelitoConfigurado() {

     }

     public DelitoConfigurado(Integer id, Integer version, Delito delito,
               Boolean calificado, Boolean culposo) {
          this.id = id;
          this.version = version;
          this.delito = delito;
          this.calificado = calificado;
          this.culposo = culposo;
     }

     @Override
     public String toString() {

          return ReflectionToStringBuilder.toString(this,
                    ToStringStyle.SHORT_PREFIX_STYLE);

     }

     public Boolean getCalificado() {
          return this.calificado;
     }

     public Delito getDelito() {
          return this.delito;
     }

     public Integer getId() {
          return this.id;
     }

     public Integer getVersion() {
          return this.version;
     }

     public void setCalificado(Boolean calificado) {
          this.calificado = calificado;
     }

     public void setDelito(Delito delito) {
          this.delito = delito;
     }

     public void setId(Integer id) {
          this.id = id;
     }

     public void setVersion(Integer version) {
          this.version = version;
     }

     public Boolean getCulposo() {
          return culposo;
     }

     public void setCulposo(Boolean culposo) {
          this.culposo = culposo;
     }

     public Boolean getTentativa() {
          return tentativa;
     }

     public void setTentativa(Boolean tentativa) {
          this.tentativa = tentativa;
     }

}
