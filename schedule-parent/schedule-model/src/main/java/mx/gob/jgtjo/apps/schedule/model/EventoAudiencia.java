package mx.gob.jgtjo.apps.schedule.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ReflectionToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

@Embeddable
@Table(name = "eventos_audiencia")
public class EventoAudiencia implements Serializable {

	private static final long serialVersionUID = 1L;

	@Basic
	@Column(name = "descripcion")
	private String descripcion;

	@Basic
	@Column(name = "no_disco")
	private Integer disco;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "hora_evento")
	private Date horaEvento;

	public EventoAudiencia() {

	}

	@Override
	public boolean equals(Object obj) {

		if (obj == null) {
			return false;
		}
		if (obj == this) {
			return true;
		}
		if (!(obj instanceof EventoAudiencia)) {
			return false;
		}

		EventoAudiencia o = (EventoAudiencia) obj;

		return new EqualsBuilder().append(this.horaEvento, o.horaEvento)
				.append(this.disco, this.disco).append(this.descripcion, o.descripcion).isEquals();
	}

	public String getDescripcion() {
		return this.descripcion;
	}

	public Integer getDisco() {
		return this.disco;
	}

	public Date getHoraEvento() {
		return this.horaEvento;
	}

	@Override
	public int hashCode() {
		return new HashCodeBuilder(21, 81).append(this.horaEvento).append(this.disco)
				.append(this.descripcion).toHashCode();
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public void setDisco(Integer disco) {
		this.disco = disco;
	}

	public void setHoraEvento(Date horaEvento) {
		this.horaEvento = horaEvento;
	}

	@Override
	public String toString() {
		return ReflectionToStringBuilder.toString(this, ToStringStyle.SHORT_PREFIX_STYLE);
	}

}
