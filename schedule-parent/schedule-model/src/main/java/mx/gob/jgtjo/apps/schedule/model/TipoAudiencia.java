package mx.gob.jgtjo.apps.schedule.model;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Version;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ReflectionToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

@Entity
@Table(name = "tipos_de_audiencia")
public class TipoAudiencia implements Serializable {

     private static final long serialVersionUID = 1L;

     @Id
     @GeneratedValue(strategy = GenerationType.TABLE)
     @Column(name = "id")
     private Integer id;

     @Version
     @Column(name = "opt_lock")
     private Integer version;

     @Basic
     private Integer duracionMinutosAprox;

     @OneToMany(mappedBy = "tipoAudiencia")
     private List<AudienciaOral> audiencias;

     @Basic
     @Column(name = "descripcion", length = 255)
     private String descripcion;

     @Basic
     @Column(name = "nombre", length = 100)
     private String nombre;

     @Basic
     @Column(name = "es_orden_aprehension")
     private Boolean ordenAprehension = false;

     @Basic
     @Column(name = "es_formulacion")
     private Boolean formulacion = false;

     @Basic
     @Column(name = "es_vinculacion")
     private Boolean vinculacion = false;

     @Basic
     @Column(name = "es_salida_alterna")
     private Boolean salidaAlterna = false;

     @Basic
     @Column(name = "es_para_medidas_cautelares")
     private Boolean medidaCautelar = false;

     @Basic
     @Column(name = "es_intermedia")
     private Boolean intermedia = false;

     @Basic
     @Column(name = "es_juicio_oral")
     private Boolean juicioOral = false;

     @Basic
     @Column(name = "es_revision_de_acuerdo")
     private Boolean revisionDeAcuerdoConciliatorio = false;

     @Basic
     @Column(name = "es_revision_de_condiciones")
     private Boolean revisionCondicionesDeProcesoPrueba = false;

     @Basic
     @Column(name = "es_impugnacion_ministerial")
     private Boolean impugnacionMinisterial = false;

     @Basic
     @Column(name = "es_cateo")
     private Boolean esCateo;

     public TipoAudiencia() {

     }

     public TipoAudiencia(Integer id, Integer version, String nombre,
               String descripcion, List<AudienciaOral> audiencias) {
          super();
          this.id = id;
          this.version = version;
          this.nombre = nombre;
          this.descripcion = descripcion;
          this.audiencias = audiencias;
     }

     @Override
     public boolean equals(Object obj) {

          if ( obj == null ) {
               return false;
          }
          if ( obj == this ) {
               return true;
          }
          if ( !(obj instanceof TipoAudiencia) ) {
               return false;
          }

          TipoAudiencia o = (TipoAudiencia) obj;

          return new EqualsBuilder().append(this.nombre, o.nombre)
                    .append(this.descripcion, o.descripcion).isEquals();
     }

     public List<AudienciaOral> getAudiencias() {
          return this.audiencias;
     }

     public String getDescripcion() {
          return this.descripcion;
     }

     public Integer getId() {
          return this.id;
     }

     public String getNombre() {
          return this.nombre;
     }

     public Integer getVersion() {
          return this.version;
     }

     @Override
     public int hashCode() {
          return new HashCodeBuilder(47, 835).append(this.nombre)
                    .append(this.descripcion).toHashCode();
     }

     public void setAudiencias(List<AudienciaOral> audiencias) {
          this.audiencias = audiencias;
     }

     public void setDescripcion(String descripcion) {
          this.descripcion = descripcion;
     }

     public void setId(Integer id) {
          this.id = id;
     }

     public void setNombre(String nombre) {
          this.nombre = nombre;
     }

     public void setVersion(Integer version) {
          this.version = version;
     }

     @Override
     public String toString() {
          return ReflectionToStringBuilder.toString(this,
                    ToStringStyle.SHORT_PREFIX_STYLE);
     }

     public Boolean getFormulacion() {
          return formulacion;
     }

     public void setFormulacion(Boolean formulacion) {
          this.formulacion = formulacion;
     }

     public Boolean getVinculacion() {
          return vinculacion;
     }

     public void setVinculacion(Boolean vinculacion) {
          this.vinculacion = vinculacion;
     }

     public Boolean getMedidaCautelar() {
          return medidaCautelar;
     }

     public void setMedidaCautelar(Boolean medidaCautelar) {
          this.medidaCautelar = medidaCautelar;
     }

     public Boolean getIntermedia() {
          return intermedia;
     }

     public void setIntermedia(Boolean intermedia) {
          this.intermedia = intermedia;
     }

     public Boolean getJuicioOral() {
          return juicioOral;
     }

     public void setJuicioOral(Boolean juicioOral) {
          this.juicioOral = juicioOral;
     }

     public Boolean getOrdenAprehension() {
          return ordenAprehension;
     }

     public void setOrdenAprehension(Boolean ordenAprehension) {
          this.ordenAprehension = ordenAprehension;
     }

     public Boolean getSalidaAlterna() {
          return salidaAlterna;
     }

     public void setSalidaAlterna(Boolean salidaAlterna) {
          this.salidaAlterna = salidaAlterna;
     }

     public Boolean getRevisionDeCondiciones() {
          return revisionDeAcuerdoConciliatorio;
     }

     public void setRevisionDeCondiciones(Boolean revisionDeCondiciones) {
          this.revisionDeAcuerdoConciliatorio = revisionDeCondiciones;
     }

     public Boolean getRevisionProcesoPrueba() {
          return revisionCondicionesDeProcesoPrueba;
     }

     public void setRevisionProcesoPrueba(Boolean revisionProcesoPrueba) {
          this.revisionCondicionesDeProcesoPrueba = revisionProcesoPrueba;
     }

     public Boolean getImpugnacionMinisterial() {
          return impugnacionMinisterial;
     }

     public void setImpugnacionMinisterial(Boolean impugnacionMinisterial) {
          this.impugnacionMinisterial = impugnacionMinisterial;
     }

     public Boolean getEsCateo() {
          return esCateo;
     }

     public void setEsCateo(Boolean esCateo) {
          this.esCateo = esCateo;
     }

}
