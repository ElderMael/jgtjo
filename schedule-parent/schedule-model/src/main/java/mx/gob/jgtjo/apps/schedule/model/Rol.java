package mx.gob.jgtjo.apps.schedule.model;

import java.io.Serializable;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Version;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ReflectionToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import org.springframework.security.core.GrantedAuthority;

@Entity
@Table(name = "roles")
public class Rol implements GrantedAuthority, Serializable {

	private static final long serialVersionUID = -2143135817173517829L;

	@Basic
	@Column(name = "rol")
	private String authority;

	@Basic
	@Column(name = "descripcion")
	private String descripcion;

	@Id
	@GeneratedValue(strategy = GenerationType.TABLE)
	@Column(name = "id")
	private Integer id;

	@Version
	@Column(name = "opt_lock")
	private Integer version;

	public Rol() {

	}

	public Rol(Integer id, Integer version, String authority, String descripcion) {
		super();
		this.id = id;
		this.version = version;
		this.authority = authority;
		this.descripcion = descripcion;
	}

	@Override
	public boolean equals(Object obj) {

		if (obj == null) {
			return false;
		}
		if (obj == this) {
			return true;
		}
		if (!(obj instanceof Rol)) {
			return false;
		}

		Rol o = (Rol) obj;

		return new EqualsBuilder().append(this.authority, o.authority)
				.append(this.descripcion, o.descripcion).isEquals();
	}

	@Override
	public String getAuthority() {
		return this.authority;
	}

	public String getDescripcion() {
		return this.descripcion;
	}

	public Integer getId() {
		return this.id;
	}

	public Integer getVersion() {
		return this.version;
	}

	@Override
	public int hashCode() {
		return new HashCodeBuilder(39, 247).append(this.authority).append(this.descripcion)
				.toHashCode();
	}

	public void setAuthority(String authority) {
		this.authority = authority;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public void setVersion(Integer version) {
		this.version = version;
	}

	@Override
	public String toString() {
		return ReflectionToStringBuilder.toString(this, ToStringStyle.SHORT_PREFIX_STYLE);
	}

}
