package mx.gob.jgtjo.apps.schedule.model;

import java.io.Serializable;
import java.util.Collection;
import java.util.List;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
import javax.persistence.Version;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ReflectionToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

@Entity
@Table(name = "users")
public class User implements UserDetails, Serializable {

     private static final long serialVersionUID = 1L;

     @Id
     @GeneratedValue(strategy = GenerationType.TABLE)
     @Column(name = "id")
     private Integer id;

     @Version
     @Column(name = "opt_lock")
     private Integer version;

     @Basic
     @Column(name = "account_non_expired")
     private Boolean accountNonExpired;

     @Basic
     @Column(name = "account_non_locked")
     private Boolean accountNonLocked;

     @Basic
     @Column(name = "apellido_materno")
     private String apellidoMaterno;

     @Basic
     @Column(name = "apellido_paterno")
     private String apellidoPaterno;

     @ManyToMany(targetEntity = Rol.class)
     @Column(name = "authorities")
     private List<GrantedAuthority> authorities;

     @Basic
     @Column(name = "credentials_non_expired")
     private Boolean credentialsNonExpired;

     @Basic
     @Column(name = "email", length = 40)
     private String email;

     @Basic
     @Column(name = "enabled")
     private Boolean enabled;

     @Basic
     @Column(name = "nombres", length = 50)
     private String nombres;

     @Basic
     @Column(name = "password", length = 50)
     private String password;

     @Basic
     @Column(name = "username", length = 15)
     private String username;

     public User() {
     }

     public User(Integer id, Integer version, String username, String password,
               Boolean accountNonExpired, Boolean accountNonLocked,
               Boolean credentialsNonExpired, Boolean enabled,
               List<GrantedAuthority> authorities, String nombres,
               String apellidoPaterno, String apellidoMaterno, String email) {
          super();
          this.id = id;
          this.version = version;
          this.username = username;
          this.password = password;
          this.accountNonExpired = accountNonExpired;
          this.accountNonLocked = accountNonLocked;
          this.credentialsNonExpired = credentialsNonExpired;
          this.enabled = enabled;
          this.authorities = authorities;
          this.nombres = nombres;
          this.apellidoPaterno = apellidoPaterno;
          this.apellidoMaterno = apellidoMaterno;
          this.email = email;
     }

     public static long getSerialversionuid() {
          return serialVersionUID;
     }

     @Override
     public boolean equals(Object obj) {

          if ( obj == null ) {
               return false;
          }
          if ( obj == this ) {
               return true;
          }
          if ( !(obj instanceof User) ) {
               return false;
          }

          User o = (User) obj;

          return new EqualsBuilder().append(this.username, o.username)
                    .append(this.password, o.password).isEquals();
     }

     public Boolean getAccountNonExpired() {
          return this.accountNonExpired;
     }

     public Boolean getAccountNonLocked() {
          return this.accountNonLocked;
     }

     public String getApellidoMaterno() {
          return this.apellidoMaterno;
     }

     public String getApellidoPaterno() {
          return this.apellidoPaterno;
     }

     @Override
     public Collection<GrantedAuthority> getAuthorities() {
          return this.authorities;
     }

     public Boolean getCredentialsNonExpired() {
          return this.credentialsNonExpired;
     }

     public String getEmail() {
          return this.email;
     }

     public Boolean getEnabled() {
          return this.enabled;
     }

     public Integer getId() {
          return this.id;
     }

     public String getNombres() {
          return this.nombres;
     }

     @Override
     public String getPassword() {
          return this.password;
     }

     @Override
     public String getUsername() {
          return this.username;
     }

     public Integer getVersion() {
          return this.version;
     }

     @Override
     public int hashCode() {
          return new HashCodeBuilder(49, 555).append(this.username)
                    .append(this.password).toHashCode();
     }

     @Override
     public boolean isAccountNonExpired() {
          return this.accountNonExpired;
     }

     @Override
     public boolean isAccountNonLocked() {
          return this.accountNonLocked;
     }

     @Override
     public boolean isCredentialsNonExpired() {
          return this.credentialsNonExpired;
     }

     @Override
     public boolean isEnabled() {
          return this.enabled;
     }

     public void setAccountNonExpired(Boolean accountNonExpired) {
          this.accountNonExpired = accountNonExpired;
     }

     public void setAccountNonLocked(Boolean accountNonLocked) {
          this.accountNonLocked = accountNonLocked;
     }

     public void setApellidoMaterno(String apellidoMaterno) {
          this.apellidoMaterno = apellidoMaterno;
     }

     public void setApellidoPaterno(String apellidoPaterno) {
          this.apellidoPaterno = apellidoPaterno;
     }

     public void setAuthorities(List<GrantedAuthority> authorities) {
          this.authorities = authorities;
     }

     public void setCredentialsNonExpired(Boolean credentialsNonExpired) {
          this.credentialsNonExpired = credentialsNonExpired;
     }

     public void setEmail(String email) {
          this.email = email;
     }

     public void setEnabled(Boolean enabled) {
          this.enabled = enabled;
     }

     public void setId(Integer id) {
          this.id = id;
     }

     public void setNombres(String nombres) {
          this.nombres = nombres;
     }

     public void setPassword(String password) {
          this.password = password;
     }

     public void setUsername(String username) {
          this.username = username;
     }

     public void setVersion(Integer version) {
          this.version = version;
     }

     @Override
     public String toString() {
          return ReflectionToStringBuilder.toString(this,
                    ToStringStyle.SHORT_PREFIX_STYLE);
     }

}
