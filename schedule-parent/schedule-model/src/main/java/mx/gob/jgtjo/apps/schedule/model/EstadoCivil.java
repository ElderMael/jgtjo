package mx.gob.jgtjo.apps.schedule.model;

public enum EstadoCivil {

	CASADO,

	SOLTERO,

	UNION_LIBRE

}
