package mx.gob.jgtjo.apps.schedule.dao.hibernate;

import org.springframework.stereotype.Repository;

import mx.gob.jgtjo.apps.schedule.dao.DelitoDao;
import mx.gob.jgtjo.apps.schedule.dao.HibernateProvider;
import mx.gob.jgtjo.apps.schedule.model.Delito;

@Repository("delitoDao")
public class DelitoDaoImpl extends HibernateBaseDao<Delito, Integer> implements DelitoDao,
		HibernateProvider {

	public DelitoDaoImpl() {

	}

}
