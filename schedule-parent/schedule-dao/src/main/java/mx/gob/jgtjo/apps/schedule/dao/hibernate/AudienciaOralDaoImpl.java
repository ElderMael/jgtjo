package mx.gob.jgtjo.apps.schedule.dao.hibernate;

import org.springframework.stereotype.Repository;

import mx.gob.jgtjo.apps.schedule.dao.AudienciaOralDao;
import mx.gob.jgtjo.apps.schedule.model.AudienciaOral;

@Repository("audienciaOralDao")
public class AudienciaOralDaoImpl extends HibernateBaseDao<AudienciaOral, Integer> implements
		AudienciaOralDao {

	public AudienciaOralDaoImpl() {

	}

}
