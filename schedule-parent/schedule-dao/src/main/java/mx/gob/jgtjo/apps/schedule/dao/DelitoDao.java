package mx.gob.jgtjo.apps.schedule.dao;

import mx.gob.jgtjo.apps.schedule.model.Delito;

public interface DelitoDao extends Dao<Integer, Delito> {

}
