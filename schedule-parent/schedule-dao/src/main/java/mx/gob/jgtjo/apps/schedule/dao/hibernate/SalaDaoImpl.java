package mx.gob.jgtjo.apps.schedule.dao.hibernate;

import org.springframework.stereotype.Repository;

import mx.gob.jgtjo.apps.schedule.dao.SalaDao;
import mx.gob.jgtjo.apps.schedule.model.Sala;

@Repository("salaDao")
public class SalaDaoImpl extends HibernateBaseDao<Sala, Integer> implements SalaDao {

	public SalaDaoImpl() {
	}

}
