package mx.gob.jgtjo.apps.schedule.dao.hibernate;

import org.springframework.stereotype.Repository;

import mx.gob.jgtjo.apps.schedule.dao.CausaPenalDao;
import mx.gob.jgtjo.apps.schedule.dao.HibernateProvider;
import mx.gob.jgtjo.apps.schedule.model.CausaPenal;

@Repository("causaPenalDao")
public class CausaPenalDaoImpl extends HibernateBaseDao<CausaPenal, Integer> implements
		CausaPenalDao, HibernateProvider {

	public CausaPenalDaoImpl() {

	}

}
