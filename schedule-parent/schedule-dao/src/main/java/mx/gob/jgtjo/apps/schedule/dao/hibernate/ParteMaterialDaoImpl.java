package mx.gob.jgtjo.apps.schedule.dao.hibernate;

import org.springframework.stereotype.Repository;

import mx.gob.jgtjo.apps.schedule.dao.HibernateProvider;
import mx.gob.jgtjo.apps.schedule.dao.ParteMaterialDao;
import mx.gob.jgtjo.apps.schedule.model.ParteMaterial;

@Repository("parteMaterialDao")
public class ParteMaterialDaoImpl extends HibernateBaseDao<ParteMaterial, Integer> implements
		ParteMaterialDao, HibernateProvider {

	public ParteMaterialDaoImpl() {

	}

}
