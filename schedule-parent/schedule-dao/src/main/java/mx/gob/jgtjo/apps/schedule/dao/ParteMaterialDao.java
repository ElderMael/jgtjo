package mx.gob.jgtjo.apps.schedule.dao;

import mx.gob.jgtjo.apps.schedule.model.ParteMaterial;

public interface ParteMaterialDao extends Dao<Integer, ParteMaterial> {

}
