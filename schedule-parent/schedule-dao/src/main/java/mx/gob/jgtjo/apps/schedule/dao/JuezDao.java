package mx.gob.jgtjo.apps.schedule.dao;

import mx.gob.jgtjo.apps.schedule.model.Juez;

public interface JuezDao extends Dao<Integer, Juez> {

}
