package mx.gob.jgtjo.apps.schedule.dao;

import mx.gob.jgtjo.apps.schedule.model.CausaPenal;

public interface CausaPenalDao extends Dao<Integer, CausaPenal> {

}
