package mx.gob.jgtjo.apps.schedule.dao;

import mx.gob.jgtjo.apps.schedule.model.User;

public interface UserDao extends Dao<Integer, User> {

}
