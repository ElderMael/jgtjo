package mx.gob.jgtjo.apps.schedule.dao;

import mx.gob.jgtjo.apps.schedule.model.MinisterioPublico;

public interface MinisterioPublicoDao extends Dao<Integer, MinisterioPublico> {

}
