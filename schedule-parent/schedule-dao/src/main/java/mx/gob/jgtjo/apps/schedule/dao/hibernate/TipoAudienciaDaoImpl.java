package mx.gob.jgtjo.apps.schedule.dao.hibernate;

import org.springframework.stereotype.Repository;

import mx.gob.jgtjo.apps.schedule.dao.TipoAudienciaDao;
import mx.gob.jgtjo.apps.schedule.model.TipoAudiencia;

@Repository("tipoAudienciaDao")
public class TipoAudienciaDaoImpl extends HibernateBaseDao<TipoAudiencia, Integer> implements
		TipoAudienciaDao {

	public TipoAudienciaDaoImpl() {

	}

}
