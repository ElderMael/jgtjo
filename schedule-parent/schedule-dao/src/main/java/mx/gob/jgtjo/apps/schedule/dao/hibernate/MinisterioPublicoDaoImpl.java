package mx.gob.jgtjo.apps.schedule.dao.hibernate;

import org.springframework.stereotype.Repository;

import mx.gob.jgtjo.apps.schedule.dao.MinisterioPublicoDao;
import mx.gob.jgtjo.apps.schedule.model.MinisterioPublico;

@Repository("ministerioPublicoDao")
public class MinisterioPublicoDaoImpl extends HibernateBaseDao<MinisterioPublico, Integer>
		implements MinisterioPublicoDao {

	public MinisterioPublicoDaoImpl() {

	}

}
