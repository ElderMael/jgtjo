package mx.gob.jgtjo.apps.schedule.dao.hibernate;

import java.io.Serializable;
import java.lang.reflect.ParameterizedType;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.hibernate.classic.Session;
import org.springframework.beans.factory.annotation.Autowired;

import mx.gob.jgtjo.apps.schedule.dao.Dao;
import mx.gob.jgtjo.apps.schedule.dao.HibernateProvider;

@SuppressWarnings("unchecked")
public class HibernateBaseDao<T, Pk extends Serializable> implements Dao<Pk, T>, HibernateProvider {

	@Autowired
	private SessionFactory sessionFactory;

	private Class<T> type;

	@Override
	public List<T> createQuery(String hqlQuery) {

		return this.sessionFactory.getCurrentSession().createQuery(hqlQuery).list();
	}

	@Override
	public List<T> createQuery(String hqlQuery, String parameterName, Object parameter) {

		return this.sessionFactory.getCurrentSession().createQuery(hqlQuery)
				.setParameter(parameterName, parameter).list();

	}

	@Override
	public List<T> createQuery(String ormQuery, Map<String, Object> parameterNameAndValues) {
		Query query = this.sessionFactory.getCurrentSession().createQuery(ormQuery);

		for (Entry<String, Object> e : parameterNameAndValues.entrySet()) {
			query.setParameter(e.getKey(), e.getValue());
		}

		return query.list();
	}

	@Override
	@SuppressWarnings("hiding")
	public <T> Map<String, T> createQueryAndReturnFirstMap(String ormQuery, Class<T> requiredType) {

		return (Map<String, T>) this.sessionFactory.getCurrentSession().createQuery(ormQuery)
				.uniqueResult();
	}

	@Override
	@SuppressWarnings("hiding")
	public <T> Map<String, T> createQueryAndReturnFirstMap(String ormQuery, String parameterName,
			Object parameter, Class<T> requiredType) {

		return (Map<String, T>) this.sessionFactory.getCurrentSession().createQuery(ormQuery)
				.setParameter(parameterName, parameter).uniqueResult();
	}

	@Override
	@SuppressWarnings("hiding")
	public <T> List<Map<String, T>> createQueryAndReturnMapList(String ormQuery,
			Class<T> requiredType) {

		return this.sessionFactory.getCurrentSession().createQuery(ormQuery).list();
	}

	@Override
	public T createQueryAndReturnUniqueResult(String ormQuery,
			Map<String, Object> parameterNameAndValues) {
		Query query = this.sessionFactory.getCurrentSession().createQuery(ormQuery);

		for (Entry<String, Object> e : parameterNameAndValues.entrySet()) {
			query.setParameter(e.getKey(), e.getValue());
		}

		return (T) query.uniqueResult();
	}

	@Override
	public Integer createDeleteQuery(String ormQuery, Map<String, Object> parameterNameAndValues) {

		Query query = this.sessionFactory.getCurrentSession().createQuery(ormQuery);

		for (Entry<String, Object> e : parameterNameAndValues.entrySet()) {
			query.setParameter(e.getKey(), e.getValue());
		}

		return query.executeUpdate();
	}

	@Override
	public Integer createQueryAndReturnAffectedRows(String ormQuery,
			Map<String, Object> parameterNameAndValues) {

		Query query = this.sessionFactory.getCurrentSession().createQuery(ormQuery);

		for (Entry<String, Object> e : parameterNameAndValues.entrySet()) {
			query.setParameter(e.getKey(), e.getValue());
		}

		return (Integer) query.list().get(0);
	}

	@Override
	public T get(Pk id) {
		return (T) this.sessionFactory.getCurrentSession().get(this.getType(), id);
	}

	@Override
	public Session getCurrentSession() {
		return this.sessionFactory.getCurrentSession();
	}

	@Override
	public SessionFactory getSessionFactory() {
		return this.sessionFactory;
	}

	public Class<T> getType() {

		if (this.type == null) {

			ParameterizedType parameterizedType = (ParameterizedType) (this.getClass()
					.getGenericSuperclass());

			while (!(parameterizedType instanceof ParameterizedType)) {
				parameterizedType = (ParameterizedType) parameterizedType.getClass()
						.getGenericSuperclass();
			}

			this.type = (Class<T>) parameterizedType.getActualTypeArguments()[0];

		}

		return this.type;
	}

	@Override
	public T load(Pk id) {

		return (T) this.sessionFactory.getCurrentSession().load(this.getType(), id);
	}

	@Override
	public T merge(T persistedObject) {
		return (T) this.sessionFactory.getCurrentSession().merge(persistedObject);
	}

	@Override
	public void persist(T transientObject) {
		this.sessionFactory.getCurrentSession().persist(transientObject);
	}

	/* Getters and Setters */

	@Override
	public void refresh(T persistedObject) {
		this.sessionFactory.getCurrentSession().refresh(persistedObject);
	}

	@Override
	public Pk save(T transientObject) {

		return (Pk) this.sessionFactory.getCurrentSession().save(transientObject);
	}

	@Override
	public void saveOrUpdate(T object) {
		this.sessionFactory.getCurrentSession().saveOrUpdate(object);
	}

	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}

	@Override
	public void update(T persistedObject) {
		this.sessionFactory.getCurrentSession().update(persistedObject);
	}

}
