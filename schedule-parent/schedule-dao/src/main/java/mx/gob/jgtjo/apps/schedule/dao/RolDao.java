package mx.gob.jgtjo.apps.schedule.dao;

import mx.gob.jgtjo.apps.schedule.model.Rol;

public interface RolDao extends Dao<Integer, Rol> {

}
