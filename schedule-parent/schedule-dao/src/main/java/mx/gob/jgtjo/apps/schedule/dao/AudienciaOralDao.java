package mx.gob.jgtjo.apps.schedule.dao;

import mx.gob.jgtjo.apps.schedule.model.AudienciaOral;

public interface AudienciaOralDao extends Dao<Integer, AudienciaOral> {

}
