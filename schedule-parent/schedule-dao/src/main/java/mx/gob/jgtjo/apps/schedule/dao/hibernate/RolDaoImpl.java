package mx.gob.jgtjo.apps.schedule.dao.hibernate;

import org.springframework.stereotype.Repository;

import mx.gob.jgtjo.apps.schedule.dao.HibernateProvider;
import mx.gob.jgtjo.apps.schedule.dao.RolDao;
import mx.gob.jgtjo.apps.schedule.model.Rol;

@Repository("rolDao")
public class RolDaoImpl extends HibernateBaseDao<Rol, Integer> implements RolDao, HibernateProvider {

	public RolDaoImpl() {

	}

}
