package mx.gob.jgtjo.apps.schedule.dao;

import mx.gob.jgtjo.apps.schedule.model.TipoAudiencia;

public interface TipoAudienciaDao extends Dao<Integer, TipoAudiencia> {

}
