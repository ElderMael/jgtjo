package mx.gob.jgtjo.apps.schedule.dao.hibernate;

import org.springframework.stereotype.Repository;

import mx.gob.jgtjo.apps.schedule.dao.JuezDao;
import mx.gob.jgtjo.apps.schedule.model.Juez;

@Repository("juezDao")
public class JuezDaoImpl extends HibernateBaseDao<Juez, Integer> implements JuezDao {

	public JuezDaoImpl() {

	}

}
