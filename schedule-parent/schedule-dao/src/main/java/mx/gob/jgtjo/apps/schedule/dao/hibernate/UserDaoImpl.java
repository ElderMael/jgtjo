package mx.gob.jgtjo.apps.schedule.dao.hibernate;

import org.springframework.stereotype.Repository;

import mx.gob.jgtjo.apps.schedule.dao.HibernateProvider;
import mx.gob.jgtjo.apps.schedule.dao.UserDao;
import mx.gob.jgtjo.apps.schedule.model.User;

@Repository("userDao")
public class UserDaoImpl extends HibernateBaseDao<User, Integer> implements UserDao,
		HibernateProvider {

	public UserDaoImpl() {

	}

}
