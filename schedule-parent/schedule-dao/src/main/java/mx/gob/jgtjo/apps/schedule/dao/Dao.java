package mx.gob.jgtjo.apps.schedule.dao;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

public interface Dao<Pk extends Serializable, T> {

	public T get(Pk id);

	public T load(Pk id);

	public T merge(T persistedObject);

	public void persist(T transientObject);

	public void refresh(T persistedObject);

	public Pk save(T transientObject);

	public void update(T persistedObject);

	public void saveOrUpdate(T object);

	public Integer createDeleteQuery(String ormQuery, Map<String, Object> parameterNameAndValues);

	public List<T> createQuery(String ormQuery);

	public List<T> createQuery(String ormQuery, Map<String, Object> parameterNameAndValues);

	public List<T> createQuery(String ormQuery, String parameterName, Object parameter);

	@SuppressWarnings("hiding")
	public <T> Map<String, T> createQueryAndReturnFirstMap(String ormQuery, Class<T> requiredType);

	@SuppressWarnings("hiding")
	public <T> Map<String, T> createQueryAndReturnFirstMap(String ormQuery, String parameterName,
			Object parameter, Class<T> requiredType);

	@SuppressWarnings("hiding")
	public <T> List<Map<String, T>> createQueryAndReturnMapList(String ormQuery,
			Class<T> requiredType);

	public T createQueryAndReturnUniqueResult(String ormQuery,
			Map<String, Object> parameterNameAndValues);

	public Integer createQueryAndReturnAffectedRows(String ormQuery,
			Map<String, Object> parameterNameAndValues);

}
