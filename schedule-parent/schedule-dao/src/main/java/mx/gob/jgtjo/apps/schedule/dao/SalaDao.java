package mx.gob.jgtjo.apps.schedule.dao;

import mx.gob.jgtjo.apps.schedule.model.Sala;

public interface SalaDao extends Dao<Integer, Sala> {

}
