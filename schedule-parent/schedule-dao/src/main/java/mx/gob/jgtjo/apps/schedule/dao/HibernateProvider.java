package mx.gob.jgtjo.apps.schedule.dao;

import org.hibernate.SessionFactory;
import org.hibernate.classic.Session;

public interface HibernateProvider {

	Session getCurrentSession();

	SessionFactory getSessionFactory();

}
